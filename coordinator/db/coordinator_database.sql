-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: USERS
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `uc_allocations`
--

DROP TABLE IF EXISTS `uc_allocations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uc_allocations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` varchar(10) DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `frequency` varchar(20) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `provisioned` tinyint(1) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `img_type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=999 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uc_allocations`
--

LOCK TABLES `uc_allocations` WRITE;
/*!40000 ALTER TABLE `uc_allocations` DISABLE KEYS */;
/*!40000 ALTER TABLE `uc_allocations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uc_configuration`
--

DROP TABLE IF EXISTS `uc_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uc_configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `value` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uc_configuration`
--

LOCK TABLES `uc_configuration` WRITE;
/*!40000 ALTER TABLE `uc_configuration` DISABLE KEYS */;
INSERT INTO `uc_configuration` VALUES (1,'website_name','Trinity\'s Reservation System'),(2,'website_url','134.226.55.214/reservation/'),(3,'email','noreply@ILoveUserCake.com'),(4,'activation','true'),(5,'resend_activation_threshold','0'),(6,'language','models/languages/en.php'),(7,'template','models/site-templates/default.css');
/*!40000 ALTER TABLE `uc_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uc_pages`
--

DROP TABLE IF EXISTS `uc_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uc_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(150) NOT NULL,
  `private` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uc_pages`
--

LOCK TABLES `uc_pages` WRITE;
/*!40000 ALTER TABLE `uc_pages` DISABLE KEYS */;
INSERT INTO `uc_pages` VALUES (1,'account.php',1),(2,'activate-account.php',0),(3,'admin_configuration.php',1),(4,'admin_page.php',1),(5,'admin_pages.php',1),(6,'admin_permission.php',1),(7,'admin_permissions.php',1),(8,'admin_user.php',1),(9,'admin_users.php',1),(10,'forgot-password.php',0),(11,'index.php',0),(12,'left-nav.php',0),(13,'login.php',0),(14,'logout.php',1),(15,'register.php',0),(16,'resend-activation.php',0),(17,'user_settings.php',1),(18,'generic_functions_lib.php',0),(19,'reset_allocations.php',0),(20,'show_calendar.php',0),(21,'socket_lib.php',0),(22,'usrp_all_reservations.php',0),(23,'usrp_date_submit.php',0),(24,'usrp_delete_reservation.php',0),(25,'usrp_make_reservation.php',0),(26,'usrp_my_reservations.php',0),(27,'usrp_provision.php',0),(28,'usrp_reservation.php',0),(29,'usrp_show_status.php',0),(30,'usrps.php',0),(31,'vm_all_reservations.php',0),(32,'vm_date_submit.php',0),(33,'vm_delete_reservation.php',0),(34,'vm_my_reservations.php',0),(35,'vm_provision.php',0),(36,'vm_reservation.php',0),(37,'vm_show_status.php',0);
/*!40000 ALTER TABLE `uc_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uc_permission_page_matches`
--

DROP TABLE IF EXISTS `uc_permission_page_matches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uc_permission_page_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uc_permission_page_matches`
--

LOCK TABLES `uc_permission_page_matches` WRITE;
/*!40000 ALTER TABLE `uc_permission_page_matches` DISABLE KEYS */;
INSERT INTO `uc_permission_page_matches` VALUES (1,1,1),(2,1,14),(3,1,17),(4,2,1),(5,2,3),(6,2,4),(7,2,5),(8,2,6),(9,2,7),(10,2,8),(11,2,9),(12,2,14),(13,2,17);
/*!40000 ALTER TABLE `uc_permission_page_matches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uc_permissions`
--

DROP TABLE IF EXISTS `uc_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uc_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uc_permissions`
--

LOCK TABLES `uc_permissions` WRITE;
/*!40000 ALTER TABLE `uc_permissions` DISABLE KEYS */;
INSERT INTO `uc_permissions` VALUES (1,'New Member'),(2,'Administrator');
/*!40000 ALTER TABLE `uc_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uc_servers`
--

DROP TABLE IF EXISTS `uc_servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uc_servers` (
  `server_id` varchar(50) NOT NULL,
  `tot_cores` int(11) NOT NULL,
  `tot_memory` int(11) NOT NULL,
  PRIMARY KEY (`server_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uc_servers`
--

LOCK TABLES `uc_servers` WRITE;
/*!40000 ALTER TABLE `uc_servers` DISABLE KEYS */;
INSERT INTO `uc_servers` VALUES ('server_1',128,128),('server_2',128,128),('server_3',128,128),('server_4',128,128);
/*!40000 ALTER TABLE `uc_servers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uc_simulations`
--

DROP TABLE IF EXISTS `uc_simulations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uc_simulations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `provisioned` tinyint(1) DEFAULT NULL,
  `memory` int(11) DEFAULT NULL,
  `cores` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uc_simulations`
--

LOCK TABLES `uc_simulations` WRITE;
/*!40000 ALTER TABLE `uc_simulations` DISABLE KEYS */;
/*!40000 ALTER TABLE `uc_simulations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uc_user_permission_matches`
--

DROP TABLE IF EXISTS `uc_user_permission_matches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uc_user_permission_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uc_user_permission_matches`
--

LOCK TABLES `uc_user_permission_matches` WRITE;
/*!40000 ALTER TABLE `uc_user_permission_matches` DISABLE KEYS */;
INSERT INTO `uc_user_permission_matches` VALUES (1,1,2),(2,1,1),(8,7,1),(9,8,1),(11,10,1),(12,11,1),(13,12,1),(14,13,1),(15,14,1),(16,15,1),(17,16,1),(18,17,1),(19,18,1);
/*!40000 ALTER TABLE `uc_user_permission_matches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uc_users`
--

DROP TABLE IF EXISTS `uc_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uc_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `password` varchar(225) NOT NULL,
  `email` varchar(150) NOT NULL,
  `activation_token` varchar(225) NOT NULL,
  `last_activation_request` int(11) NOT NULL,
  `lost_password_request` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `title` varchar(150) NOT NULL,
  `sign_up_stamp` int(11) NOT NULL,
  `last_sign_in_stamp` int(11) NOT NULL,
  `institution` varchar(225) NOT NULL,
  `project` varchar(225) NOT NULL,
  `ssh_key` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uc_users`
--

LOCK TABLES `uc_users` WRITE;
/*!40000 ALTER TABLE `uc_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `uc_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uc_usrps`
--

DROP TABLE IF EXISTS `uc_usrps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uc_usrps` (
  `node_id` varchar(50) NOT NULL,
  `x_pos` int(11) NOT NULL,
  `y_pos` int(11) NOT NULL,
  PRIMARY KEY (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uc_usrps`
--

LOCK TABLES `uc_usrps` WRITE;
/*!40000 ALTER TABLE `uc_usrps` DISABLE KEYS */;
INSERT INTO `uc_usrps` VALUES ('11',280,280),('12',200,280),('13',120,280),('14',40,280),('21',280,200),('22',200,200),('23',120,200),('24',40,200),('31',280,120),('32',200,120),('33',120,120),('34',40,120),('41',280,40),('42',200,40),('43',120,40),('44',40,40);
/*!40000 ALTER TABLE `uc_usrps` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-26 14:34:41
