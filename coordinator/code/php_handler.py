#!/usr/bin/env python
'''
    Coordinator PHP Handler. Handles requests from the Webserver
    (iris-testbed.connectcentre.ie/reservation)
'''

import logging
import MySQLdb
import os
import datetime
import zabbix_ctrl

from user_resources import *

def php_handle(tcpHandler,items):
    '''
        Receive requests from the the Webserver and execute the appropriate action.

        Args:
            itens (list): Desired action with respective arguments.

        Returns:
            msg (string): Result of the operation.
    '''

    tcpHandler.logger.debug('Request from PhP')
    tcpHandler.logger.debug('Items: '+str(items))
    
    if 'update_ssh_key' in items:
        [user_id,ssh_key] = [items[1],items[2]]
        msg = update_ssh(tcpHandler,user_id,ssh_key)

    elif 'make_simulation_reservation' in items:
        [cores, memory, user_id, email, start_time, end_time, type_res, rack_id,img_type] = [items[1],items[2],items[3],items[4],items[5],items[6],items[7],items[8],items[9]]
        msg = make_reservation_simulation(tcpHandler,cores, memory, user_id, email, start_time, end_time, type_res, rack_id,img_type)

    elif (('provision' in items) and ('usrp' in item for item in items)):
        [id_res,usrp,img_type,user_id,user_key] = [items[1],items[2],items[3],items[4],items[5]]
        msg = provision_PHP(tcpHandler,id_res,usrp,img_type,user_id,user_key)

    elif ('provisionVM' in items):
        [id_res,rack_id,memory,domain_str,cores,img_type,user_id,user_key] = [items[1],items[2],items[3],items[4],items[5],items[6],items[7],items[8]]
        msg = provision_VM(tcpHandler,id_res,rack_id,memory,domain_str,cores,img_type,user_id,user_key)

    elif (('provision_x310' in items) and ('usrp' in item for item in items)):
        [id_res,usrp,img_type,user_id,user_key] = [items[1],items[2],items[3],items[4],items[5]]
        msg = provision_x310_PHP(tcpHandler,id_res,usrp,img_type,user_id,user_key)

    elif (('provision_n210beam' in items) and ('usrp' in item for item in items)):
        [id_res,usrp,img_type,user_id,user_key] = [items[1],items[2],items[3],items[4],items[5]]
        msg = provision_n210beam_PHP(tcpHandler,id_res,usrp,img_type,user_id,user_key)

    elif (('status' in items) and ('usrp' in item for item in items)):
        [usrp] = [items[1]]
        msg = status_usrp(tcpHandler,usrp)

    elif 'statusVM' in items:
        [sim_id] = [items[1]]
        msg = status_VM(tcpHandler,sim_id)

    elif (('describe' in items) and ('usrp' in item for item in items)):
        [usrp,id_res] = [items[1],items[2]]
        msg = describePHP(tcpHandler,usrp,id_res)

    elif 'describeVM' in items:
        [sim_id,domain_str] = [items[1],items[2]]
        msg = describe_VM(tcpHandler,sim_id,domain_str)

    elif (('describe_x310' in items) and ('usrp' in item for item in items)):
        [usrp,id_res] = [items[1],items[2]]
        msg = describex310PHP(tcpHandler,usrp,id_res)

    elif (('describe_n210beam' in items) and ('usrp' in item for item in items)):
        [usrp,id_res] = [items[1],items[2]]
        msg = describen210beamPHP(tcpHandler,usrp,id_res)

    elif 'delete_user' in items:
        [user_id] = [items[1]]
        msg = delete_user(tcpHandler,user_id)

    elif 'create_user' in items:
        [user_id,ssh_key,email] = [items[1],items[2],items[3]]
        msg = create_user(tcpHandler,user_id,ssh_key,email)

    elif 'update_user' in items:
        msg = 'update_user'

    elif 'make_reservation' in items:
        [usrp, user_id, email, start_time, end_time, frequency,img_type, type_res] = [items[1],items[2],items[3],items[4],items[5],items[6],items[7],items[8]]
        msg = make_reservation(tcpHandler,usrp, user_id, email, start_time, end_time, frequency, img_type, type_res)

    elif 'make_reservation_x310' in items:
        tcpHandler.logger.info("landed in make reservation x310")
        [usrp, user_id, email, start_time, end_time,img_type, type_res] = [items[1],items[2],items[3],items[4],items[5],items[6],items[7]]
        msg = make_x310_reservation(tcpHandler,usrp, user_id, email, start_time, end_time, img_type, type_res)

    elif 'make_reservation_n210beam' in items:
        [usrp, user_id, email, start_time, end_time,img_type, type_res] = [items[1],items[2],items[3],items[4],items[5],items[6],items[7]]
        msg = make_n210beam_reservation(tcpHandler,usrp, user_id, email, start_time, end_time, img_type, type_res)

    elif 'delete_allocation' in items:
        [id_res,user_id] = [items[1],items[2]]
        msg = delete_allocation(tcpHandler,id_res,user_id)

    elif 'delete_allocation_x310' in items:
        [id_res,user_id] = [items[1],items[2]]
        msg = delete_x310_allocation(tcpHandler,id_res,user_id)

    elif 'delete_simulation' in items:
        [sim_id,domain_str,user_id] = [items[1],items[2],items[3]]
        msg = delete_simulation(tcpHandler,sim_id,domain_str,user_id)

    elif 'delete_allocation_n210beam' in items:
        [id_res,user_id] = [items[1],items[2]]
        msg = delete_n210beam_allocation(tcpHandler,id_res,user_id)

    return msg

def update_ssh(tcpHandler,user_id,ssh_key):
    '''
        Updates user's SSH key on the Gateway.

        Args:
            user_id (string): Name of the user
            ssh_key (string): New ssh key for the user

        Returns:
            msg (string): Status of the request.
    '''
    user_id = user_id.replace(' ','')
    global protected_users
    if any (user_id == u for u in protected_users):
        tcpHandler.logger.warning('Cannot modify the ssh key because '+str(user_id)+' is in the list of protected users')
        msg = 'Cannot create a user because '+str(user_id)+' in the list of protected users'
        return msg
    else:
        authorized_keys = []
        if any(ssh_key in k for k in authorized_keys):
            tcpHandler.logger.info('user'+str(user_id)+' already authorized.')
        else:
            tcpHandler.logger.info('add the key to authorized_keys for '+str(user_id))
            authorized_keys.append(str(ssh_key)+'\n')

        f = open('/home/'+user_id+'/.ssh/authorized_keys', 'w')
        for keys in authorized_keys:
            f.write(keys)
        f.close()
        # Do we actually need to change the permissions ??
        tcpHandler.logger.info('Changing permissions on $HOME for user '+user_id)
        os.popen("chmod 544 /home/"+user_id+" -R")
        # We do need to change the ownership of the .ssh/ forlder
        # change authorizations (make home folder only executable
        os.popen("chown "+user_id+":"+user_id+" /home/"+user_id+"/.ssh -R") # ownership of this folder goes back to user_id
        msg = 'Everything went fine for user '+user_id
    return msg

def delete_user(tcpHandler,user_id):
    '''
        Delete user on the gateway.

        Args:
            user_id (string): Username to be deleted.

        Returns:
            msg (string): Status of the request.
    '''
    user_id = user_id.replace(' ','')
    global protected_users
    if any (user_id == u for u in protected_users):
        tcpHandler.logger.warning('Cannot delete a user because its in the list of protected users')
        msg = user_id + 'is a protected user cannot be deleted'
        return msg
    else:
        tcpHandler.logger.info('deleting user '+str(user_id))
        try:
            os.popen("pkill -u "+user_id)   # kill all active processes for user user_id
            os.popen("deluser --remove-home "+user_id)  # delete the user and the its $HOME
            tcpHandler.logger.debug('user '+user_id+' successfully removed.')
        except:
            tcpHandler.logger.error('could not remove user_id '+str(user_id))
            msg = 'something went wront deleting '+user_id
            return msg

    zabbix_ctrl.delete_zabbix_user(tcpHandler,user_id)
    msg = 'everything went fine deleting user '+user_id
    return msg

def make_reservation_simulation(tcpHandler,cores, memory, user_id, email, start_time, end_time, type_res, rack_id,img_type):
    '''
        Creates a reservation for resources on the Database.

        Args:
            cores (int): number of cores for the VM
            memory (int): Total memory for the VM
            user_id (string): Username
            email (string): Email of the user
            start_time (datetime): Start time of the reservation
            end_time (datetime): end time of the reservation
            type_res (string): Type of reservation
            rack_id (int): rack to host VM

        Returns:
            msg (string): Status of the request.
    '''

    tcpHandler.logger.info('user '+str(user_id)+' is making a reservation for '+str(cores)+' cores '+str(memory)+' memory from '+str(start_time)+' to '+str(end_time))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        tcpHandler.logger.info('INSERT INTO uc_simulations (cores,memory,user_name,email,start_time,end_time,type,provisioned,rack_id, img_type) VALUES('+str(cores)+','+str(memory)+','+str(user_id)+','+str(email)+','+str(start_time)+','+str(end_time)+','+str(type_res)+',0,'+str(rack_id)+','+str(img_type)+')')
        cur.execute("""
        INSERT INTO uc_simulations
        (cores,memory,user_name,email,start_time,end_time,type,provisioned,ip_address,rack_id,img_type)
        VALUES(%s,%s,%s,%s,%s,%s,%s,0,'XXX',%s,%s)
        """,(int(cores),int(memory),str(user_id),str(email),str(start_time),str(end_time),str(type_res),str(rack_id),str(img_type)))
        db.commit()
        tcpHandler.logger.debug('Reservation on the db success.')
        msg = 'reservation successfully created.'
    except:
        tcpHandler.logger.error('INSERT db failed')
        msg = 'reservation failed'
        tcpHandler.logger.info('coordinator replies: '+str(msg))
        return msg
    cur.close()
    db.close()
    return msg

def make_n210beam_reservation(tcpHandler,usrp, user_id, email, start_time, end_time, img_type, type_res):
    '''
        Creates a reservation for resources.

        Args:
            usrp (int): n210beam USRP number to be reserved
            user_id (string): Username
            email (string): Email of the user
            start_time (datetime): Start time of the reservation
            end_time (datetime): End time of the reservation
            img_type (string): Type of Image that will populate the VM
            type_res (string): Type of the reservation

        Returns:
            msg (string): Status of the request.
    '''

    tcpHandler.logger.info('user '+str(user_id)+' is making a reservation for '+str(usrp)+' from '+str(start_time)+' to '+str(end_time))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        db_msg=('INSERT INTO uc_n210beam_allocations '
            '(node_id,user_name,email,start_time,end_time,type,provisioned,ip_address,img_type) '
            'VALUES({!r},{!r},{!r},{!r},{!r},{!r},0,{!r},{!r})').format(usrp,user_id,email,start_time,end_time,type_res,'XXX',img_type)
        tcpHandler.logger.debug(db_msg)
        cur.execute(db_msg)
        db.commit()
        tcpHandler.logger.debug('Reservation on the db success.')
        msg = 'reservation successfully created.'
    except:
        tcpHandler.logger.error('INSERT db failed'+db_msg)
        # FIX
        msg = 'reservation unsuccessful.'
        return msg
    cur.close()
    db.close()
    return msg

def provision_PHP(tcpHandler,id_res,usrp,img_type,user_id,user_key):
    '''
        Provision a reservation with a Virtual machine and an USRP.

        Args:
            id_res (int): Reservation number
            usrp (int): Number of the USRP
            img_type (string): type of image to populate the VM
            user_id (string): Username to be deployed
            user_key (string): SSH key of the user

        Returns:
            msg (string): Status of the request
    '''
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
	db_msg='UPDATE uc_allocations SET provisioned = 1 WHERE (id = '+str(id_res)+')'
        tcpHandler.logger.debug(db_msg)
        cur.execute(db_msg)
        db.commit()
    except:
        tcpHandler.logger.error('Update db failed'+db_msg)
        msg = 'Update db failed'
        return msg
    cur.close()
    db.close()

    CBTmCommand = 'request_unit:'+usrp+','+img_type+','+user_id+','+user_key+',n210'
    msg = tcpHandler.CBTmProtocol(CBTmCommand)

    msg = str(id_res)+' provisioned'
    return msg

def provision_VM(tcpHandler,id_res,rack_id,memory,domain_str,cores,img_type,user_id,user_key):
    '''
        Provision a simulation reservation with a Virtual machine 

        Args:
            id_res (int): Reservation number
            rack_id (int): id of rack
            memory (int): amount of memory in GB
            domain_str (string): simulations, x310s, b200smini,etc
            cores (int): amount of cpus
            img_type (string): type of image to populate the VM
            user_id (string): Username to be deployed
            user_key (string): SSH key of the user

        Returns:
            msg (string): Status of the request 
    '''
    db = MySQLdb.connect(host=db_host,           # your host, usually localhost
             user=db_user,                       #
             passwd=db_passwd,    # your password
             db=db_name)           # name of the database created by userCake
    cur = db.cursor()
    try:
        db_msg='UPDATE uc_'+domain_str+'s'+' SET provisioned = 1 WHERE (id = '+str(id_res)+')'
        tcpHandler.logger.info('UPDATE uc_'+domain_str+'s'+' SET provisioned = 1 WHERE (id = '+str(id_res)+')')
        cur.execute(db_msg)
        db.commit()

        msg = str(id)+' provisioned'
    except:
        tcpHandler.logger.error('Update dbi  failed'+db_msg)
        msg = 'something when wrong'
    cur.close()
    db.close()
    
    #  tcpHandler,id_res,rack_id,memory,cores,img_type,user_id,user_key
    ##request_simulation_vm:"$random_id",rack_id(11 or 13),no. CPUs,MEMORY,php_simulation,tstuser,"$KEY
    CBTmCommand = 'request_simulation_vm:'+id_res+','+rack_id+','+memory+','+cores+','+domain_str+'_eu_'+','+img_type+','+user_id+','+user_key
    tcpHandler.logger.info(CBTmCommand)
    msg = tcpHandler.CBTmProtocol(CBTmCommand)

    return msg

def provision_x310_PHP(tcpHandler,id_res,usrp,img_type,user_id,user_key):
    '''
        Provision a reservation with a Virtual machine and an USRP.

        Args:
            id_res (int): Reservation number
            usrp (int): Number of the USRP
            img_type (string): type of image to populate the VM
            user_id (string): Username to be deployed
            user_key (string): SSH key of the user

        Returns:
            msg (string): Status of the request
    '''
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        db_msg='UPDATE uc_x310_allocations SET provisioned = 1 WHERE (id = '+str(id_res)+')'
        tcpHandler.logger.debug(db_msg)
        cur.execute(db_msg)
        db.commit()
    except:
        tcpHandler.logger.error('Update db failed'+db_msg)
        msg = 'Update db failed'
        return msg
    cur.close()
    db.close()

    CBTmCommand = 'request_unit:'+usrp+','+img_type+','+user_id+','+user_key+',x310'
    msg = tcpHandler.CBTmProtocol(CBTmCommand)

    msg = str(id_res)+' provisioned'
    return msg

def provision_n210beam_PHP(tcpHandler,id_res,usrp,img_type,user_id,user_key):
    '''
        Provision a reservation with a n210 beam USRP and VM.

        Args:
            id_res (int): Reservation number
            usrp (int): Number of the n210beam USRP
            img_type (string): type of image to populate the VM
            user_id (string): Username to be deployed
            user_key (string): SSH key of the user

        Returns:
            msg (string): Status of the request
    '''
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        db_msg='UPDATE uc_n210beam_allocations SET provisioned = 1 WHERE (id = '+str(id_res)+')'
        tcpHandler.logger.debug(db_msg)
        cur.execute(db_msg)
        db.commit()
    except:
        tcpHandler.logger.error('Update db failed'+db_msg)
        msg = 'Update db failed'
        return msg
    cur.close()
    db.close()

    CBTmCommand = 'request_unit:'+usrp+','+img_type+','+user_id+','+user_key+',n210beam'
    msg = tcpHandler.CBTmProtocol(CBTmCommand)

    msg = str(id_res)+' provisioned'
    return msg

def status_usrp(tcpHandler,usrp):
    '''
        NOT IN USE
    '''

    tcpHandler.logger.info('status called for usrp '+str(usrp))

    #If ready, it must return "geni_ready", otherwise it must return "geni_configuring"
    #CBTmCommand = 'unit_status:'+str(usrp)
    #msg = tcpHandlerCBTmProtocol(CBTmCommand)

    msg = usrp+',geni_ready'
    return msg

def status_VM(tcpHandler,sim_id):
    '''
        NOT IN USE
    '''
    tcpHandler.logger.info('status called for simulation ID '+str(sim_id))
    #If ready, it must return "geni_ready", otherwise it must return "geni_configuring"
    # Attention, there is the problem of the identifier for the simulation resources
    #CBTmCommand = 'status:'+str(sim_id)
    #msg = CBTmProtocol(CBTmCommand)

    msg = sim_id+',geni_ready'
    return msg

def describe_VM(tcpHandler,sim_id,domain_str):
    '''
        
    '''
    #CBTm: Here the IP address assigned to sim_id must be returned.
    CBTmCommand = 'sim_unit_status:'+sim_id+','+domain_str
    tcpHandler.logger.info(CBTmCommand)
    msg = tcpHandler.CBTmProtocol(CBTmCommand)
    tcpHandler.logger.info(msg)
    ip_address = msg.split(':')[1]

    tcpHandler.logger.info('described called for simulation ID '+str(sim_id))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        cur.execute('''
            UPDATE uc_simulations
            SET ip_address=%s
            WHERE (id=%s)
            ''', (str(ip_address),str(sim_id),))
        db.commit()
        tcpHandler.logger.debug('Update db success.')
    except:
        tcpHandler.logger.error('Update from db failed.')
        msg = 'update failed'
        return msg
    cur.close()
    db.close()
    msg = sim_id+','+str(ip_address)
    return msg

def describePHP(tcpHandler,usrp,id_res):
    '''
        Gets a description of the deployed VM for a given reservation.

        Args:
            usrp (int): number of the USRP designed for this reservation
            id_res (int): Reservation number

        Returns:
            msg (string): Status of the request
    '''

    #Query the CBTM for the VM's IP
    CBTmCommand = 'unit_status:'+usrp
    msg = tcpHandler.CBTmProtocol(CBTmCommand)
    tcpHandler.logger.debug(msg)
    ip_address = msg.split(':')[1]

    tcpHandler.logger.info('described called for usrp '+str(usrp))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        cur.execute('''
            UPDATE uc_allocations
            SET ip_address=%s
            WHERE (id=%s)
            ''', (str(ip_address),str(id_res),))
        db.commit()
        tcpHandler.logger.debug('Update db success.')
    except:
        tcpHandler.logger.error('Update from db failed.')
        msg = 'update failed'
        return msg
    cur.close()
    db.close()
#    if any (usrp == host for host in tcpHandler.zabbix_hosts.keys()):
#        tcpHandler.logger.info('Host '+usrp+' already exists.')
#    else:
#        zabbix_ctrl.create_zabbix_host(tcpHandler,usrp, ip_address)
    msg = usrp+','+str(ip_address)
    return msg

def describex310PHP(tcpHandler,usrp,id_res):
    '''
        Gets a description of the deployed VM for a given reservation.

        Args:
            usrp (int): number of the USRP designed for this reservation
            id_res (int): Reservation number

        Returns:
            msg (string): Status of the request
    '''

    #Query the CBTM for the VM's IP
    CBTmCommand = 'sim_unit_status:'+usrp+',basic_eu_'
    msg = tcpHandler.CBTmProtocol(CBTmCommand)
    tcpHandler.logger.debug(msg)
    ip_address = msg.split(':')[1]

    tcpHandler.logger.info('described called for usrp '+str(usrp))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        cur.execute('''
            UPDATE uc_x310_allocations
            SET ip_address=%s
            WHERE (id=%s)
            ''', (str(ip_address),str(id_res),))
        db.commit()
        tcpHandler.logger.debug('Update db success.')
    except:
        tcpHandler.logger.error('Update from db failed.')
        msg = 'update failed'
        return msg
    cur.close()
    db.close()
    if any (usrp == host for host in tcpHandler.zabbix_hosts.keys()):
        tcpHandler.logger.info('Host '+usrp+' already exists.')
    else:
        zabbix_ctrl.create_zabbix_host(tcpHandler,usrp, ip_address)
    msg = usrp+','+str(ip_address)
    return msg

def describen210beamPHP(tcpHandler,usrp,id_res):
    '''
        Gets a description of the deployed n210beam usrp for a given reservation.

        Args:
            usrp (int): number of the n210beam USRP designed for this reservation
            id_res (int): Reservation number

        Returns:
            msg (string): Status of the request
    '''

    #Query the CBTM for the VM's IP
    CBTmCommand = 'sim_unit_status:'+usrp+',basic_eu_'
    msg = tcpHandler.CBTmProtocol(CBTmCommand)
    tcpHandler.logger.debug(msg)
    ip_address = msg.split(':')[1]

    tcpHandler.logger.info('described called for n210 beam usrp '+str(usrp))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        cur.execute('''
            UPDATE uc_n210beam_allocations
            SET ip_address=%s
            WHERE (id=%s)
            ''', (str(ip_address),str(id_res),))
        db.commit()
        tcpHandler.logger.debug('Update db success.')
    except:
        tcpHandler.logger.error('Update from db failed.')
        msg = 'update failed'
        return msg
    cur.close()
    db.close()
    if any (usrp == host for host in tcpHandler.zabbix_hosts.keys()):
        tcpHandler.logger.info('Host '+usrp+' already exists.')
    else:
        zabbix_ctrl.create_zabbix_host(tcpHandler,usrp, ip_address)
    msg = usrp+','+str(ip_address)
    return msg

def create_user(tcpHandler,user_id,ssh_key,email):
    '''
        Creates user in the Gateway for use with the Provisioned VM

        Args:
            user_id (string): Username
            ssh_key (string): User's SSH key
            email (String): User's Email

        Returns:
            msg (string): Status of the request.
    '''
    user_id = user_id.replace(' ','')
    global protected_users
    if any (user_id == u for u in protected_users):
        tcpHandler.logger.warning('Cannot create a user because its in the list of protected users')
        msg = 'Cannot create a user because its in the list of protected users'
        return msg
    else:
        list_users = os.popen("ls -r /home").read().split('\n')
        if any (user_id == u for u in list_users):
            tcpHandler.logger.info('user '+str(user_id)+' already deployed')
        else:
            tcpHandler.logger.info('create user '+str(user_id))
            try:
                os.popen("useradd -m "+user_id+" -s /usr/sbin/nologin")         # create user_id with $HOME
                os.popen("adduser "+user_id+" reservation") # add user to group reservation (limit what users can do)
                os.popen("mkdir /home/"+user_id+"/.ssh")             # create ~/.ssh
                os.popen("> /home/"+user_id+"/.ssh/authorized_keys") # create the file
                tcpHandler.logger.debug('user '+str(user_id)+' successfully created.')
            except:
                tcpHandler.logger.error('Something went wrong with creation of user '+str(user_id))
                msg = 'Something went wrong with creation of user '+str(user_id)
                return msg
        #f = open('/home/'+user_id+'/.ssh/authorized_keys', 'r')
        authorized_keys = []
        if any(ssh_key in k for k in authorized_keys):
            tcpHandler.logger.debug('user'+str(user_id)+' already authorized.')
        else:
            tcpHandler.logger.debug('add the key to authorized_keys for '+str(user_id))
            authorized_keys.append(str(ssh_key)+'\n')

        f = open('/home/'+user_id+'/.ssh/authorized_keys', 'w')
        for keys in authorized_keys:
            f.write(keys)
        f.close()
        tcpHandler.logger.info('Changing permissions on $HOME for user '+user_id)
        os.popen("chmod 544 /home/"+user_id+" -R")                          # change authorizations (make home folder only executable
        os.popen("chown "+user_id+":"+user_id+" /home/"+user_id+"/.ssh -R") # ownership of this folder goes back to user_id
        try:
            zabbix_ctrl.create_zabbix_user(tcpHandler,user_id,email)
            tcpHandler.logger.info('Create user '+user_id+' in Zabbix Server.')
        except:
            tcpHandler.logger.error('User '+user_id+' creation in Zabbix Server failed.')
            msg = 'Could not create user '+user_id+' in Zabbix server'
            return msg
        msg = 'Everything went fine for user '+user_id
        return msg
        # this stuff is for .Xauthority but it doesn't work
        #os.popen("touch /home/"+user_id+"/.Xauthority")
        #os.popen("chmod 744 /home/"+user_id+"/.Xauthority")
        #os.popen("chown "+user_id+":"+user_id+" /home/"+user_id+"/.Xauthority")

def make_x310_reservation(tcpHandler,usrp, user_id, email, start_time, end_time, img_type, type_res):
    '''
        Creates a reservation for resources.

        Args:
            usrp (int): USRP number to be reserved
            user_id (string): Username
            email (string): Email of the user
            start_time (datetime): Start time of the reservation
            end_time (datetime): End time of the reservation
            img_type (string): Type of Image that will populate the VM
            type_res (string): Type of the reservation

        Returns:
            msg (string): Status of the request.
    '''

    tcpHandler.logger.info('user '+str(user_id)+' is making a reservation for '+str(usrp)+' from '+str(start_time)+' to '+str(end_time))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        db_msg=('INSERT INTO uc_x310_allocations '
            '(node_id,user_name,email,start_time,end_time,type,provisioned,ip_address,img_type) '
            'VALUES({!r},{!r},{!r},{!r},{!r},{!r},0,{!r},{!r})').format(usrp,user_id,email,start_time,end_time,type_res,'XXX',img_type)
        tcpHandler.logger.debug(db_msg)
        cur.execute(db_msg)
        db.commit()
        tcpHandler.logger.debug('Reservation on the db success.')
        msg = 'reservation successfully created.'
    except:
        tcpHandler.logger.error('INSERT db failed'+db_msg)
        # FIX
        msg = 'reservation unsuccessful.'
        return msg
    cur.close()
    db.close()
    return msg

def make_reservation(tcpHandler,usrp, user_id, email, start_time, end_time, frequency, img_type, type_res):
    '''
        Creates a reservation for resources.

        Args:
            usrp (int): USRP number to be reserved
            user_id (string): Username
            email (string): Email of the user
            start_time (datetime): Start time of the reservation
            end_time (datetime): End time of the reservation
            frequency (string): Frequency that will be used
            img_type (string): Type of Image that will populate the VM
            type_res (string): Type of the reservation

        Returns:
            msg (string): Status of the request.
    '''

    tcpHandler.logger.info('user '+str(user_id)+' is making a reservation for '+str(usrp)+' from '+str(start_time)+' to '+str(end_time)+' at '+str(frequency))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        db_msg=('INSERT INTO uc_allocations '
            '(node_id,user_name,email,start_time,end_time,type,provisioned,frequency,ip_address,img_type) '
            'VALUES({!r},{!r},{!r},{!r},{!r},{!r},0,{!r},{!r},{!r})').format(usrp,user_id,email,start_time,end_time,type_res,frequency,'XXX',img_type)
        #tcpHandler.logger.debug('INSERT INTO uc_allocations (node_id,user_name,email,start_time,end_time,type,provisioned,frequency,img_type) VALUES('+str(usrp)+','+str(user_id)+','+str(email)+','+str(start_time)+','+str(end_time)+','+str(type_res)+',0,'+str(frequency)+','+str(img_type)+')')
        tcpHandler.logger.debug(db_msg)
        cur.execute(db_msg)
        db.commit()
        tcpHandler.logger.debug('Reservation on the db success.')
        msg = 'reservation successfully created.'
    except:
        tcpHandler.logger.error('INSERT db failed'+db_msg)
        # FIX
        msg = 'reservation unsuccessful.'
        return msg
    cur.close()
    db.close()
    return msg

def delete_x310_allocation(tcpHandler,id_res,user_id):
    '''
        Delete a reservation and remove a zabbix host.

        Args:
            id_res (int): Reservation number
            user_id (string): Name of the user

        Returns:
            msg (string): Status of the request.
    '''
    now = str(datetime.datetime.utcnow())
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    tcpHandler.logger.info('User '+str(user_id)+' is deleting allocation '+str(id_res))
    try:
        selectStatment = "SELECT * FROM uc_x310_allocations WHERE (id = %s)" %str(id_res)
        cur.execute(selectStatment)
        columns = [column[0] for column in cur.description]
        results = []
        for row in cur.fetchall():
            results.append(dict(zip(columns, row)))     # results contains all the details about the reservation
    except:
        tcpHandler.logger.error('Query to db failed.')
        results = []
        msg = 'Query failed'
        return msg

    try:
        updateStatment = "UPDATE uc_x310_allocations SET end_time =\'%s\' WHERE (id = %s)" %(str(now),str(id_res))
        tcpHandler.logger.info(updateStatment)
        cur.execute(updateStatment)
        db.commit()
        tcpHandler.logger.info('Update uc_x310_allocations from db success.')
    except:
        tcpHandler.logger.error('Update uc_x310_allocations db failed.')
        msg = 'update failed'
        return msg
    cur.close()
    db.close()
    msg = 'delete success'
    return msg

def delete_allocation(tcpHandler,id_res,user_id):
    '''
        Delete a reservation and remove a zabbix host.

        Args:
            id_res (int): Reservation number
            user_id (string): Name of the user

        Returns:
            msg (string): Status of the request.
    '''
    now = str(datetime.datetime.utcnow())
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    tcpHandler.logger.info('User '+str(user_id)+' is deleting allocation '+str(id_res))
    try:
        selectStatment = "SELECT * FROM uc_allocations WHERE (id = %s)" %str(id_res)
        cur.execute(selectStatment)
        columns = [column[0] for column in cur.description]
        results = []
        for row in cur.fetchall():
            results.append(dict(zip(columns, row)))     # results contains all the details about the reservation
    except:
        tcpHandler.logger.error('Query to db failed.')
        results = []
        msg = 'Query failed'
        return msg

    try:
        updateStatment = "UPDATE uc_allocations SET end_time =\'%s\' WHERE (id = %s)" %(str(now),str(id_res))
        cur.execute(updateStatment)
        db.commit()
        tcpHandler.logger.debug('Update from db success.')
    except:
        tcpHandler.logger.error('Update from db failed.')
        msg = 'update failed'
        return msg
    cur.close()
    db.close()
    if any (results[0]['node_id'] == host for host in tcpHandler.zabbix_hosts.keys()):
        zabbix_ctrl.delete_zabbix_host(tcpHandler,results[0]['node_id'])
    else:
        tcpHandler.logger.info('Host '+results[0]['node_id']+' does not exist.')
    msg = 'delete success'
    return msg

def delete_simulation(tcpHandler,sim_id,domain_str,user_id):
    '''
        Delete a simulation reservation.

        Args:
            sim_id (int): Simulation number
            domain_str (string): virt domain name
            user_id (string): Name of the user

        Returns:
            msg (string): Status of the request.
    '''
    now = str(datetime.datetime.utcnow())
    tcpHandler.logger.info('user '+str(user_id)+' is deleting allocation '+domain_str+str(sim_id))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    tcpHandler.logger.info('User '+str(user_id)+' is deleting allocation '+str(sim_id))
    try:
        selectStatment = "SELECT * FROM uc_simulations WHERE (id = %s)"
        cur.execute(selectStatment, (str(sim_id),))
        columns = [column[0] for column in cur.description]
        results = []
        for row in cur.fetchall():
            results.append(dict(zip(columns, row)))     # results contains all the details about the reservation
    except:
        tcpHandler.logger.error('Query to db failed.')
        results = []
        msg = 'Query failed'
        return msg
    cur.close()
    cur = db.cursor()
    try:
        cur.execute("""
            UPDATE uc_simulations
            SET end_time=%s
            WHERE (id=%s)
            """, (str(now),str(sim_id)))
        db.commit()
        tcpHandler.logger.debug('Update from db success.')
    except:
        tcpHandler.logger.error('Update from db failed.')
        msg = 'update failed'
        return msg
    cur.close()
    db.close()

    ##delete vm on CBTM
    CBTmCommand = 'release_sim_unit:'+sim_id+','+domain_str
    msg = tcpHandler.CBTmProtocol(CBTmCommand)
    msg = 'delete success'
   
    return msg

def delete_n210beam_allocation(tcpHandler,id_res,user_id):
    '''
        Delete a reservation and remove a zabbix host.

        Args:
            id_res (int): Reservation number
            user_id (string): Name of the user

        Returns:
            msg (string): Status of the request.
    '''
    now = str(datetime.datetime.utcnow())
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    tcpHandler.logger.info('User '+str(user_id)+' is deleting allocation '+str(id_res))
    try:
        selectStatment = "SELECT * FROM uc_n210beam_allocations WHERE (id = %s)" %str(id_res)
        cur.execute(selectStatment)
        columns = [column[0] for column in cur.description]
        results = []
        for row in cur.fetchall():
            results.append(dict(zip(columns, row)))     # results contains all the details about the reservation
    except:
        tcpHandler.logger.error('Query to db failed.')
        results = []
        msg = 'Query failed'
        return msg

    try:
        updateStatment = "UPDATE uc_n210beam_allocations SET end_time =\'%s\' WHERE (id = %s)" %(str(now),str(id_res))
        cur.execute(updateStatment)
        db.commit()
        tcpHandler.logger.debug('Update from db success.')
    except:
        tcpHandler.logger.error('Update from db failed.')
        msg = 'update failed'
        return msg
    cur.close()
    db.close()
    msg = 'delete success'
    return msg
