#!/usr/bin/env ruby

# example script that reads ICMP load measurements from a Zabbix server
# and pushes them into an OML database

# make sure you install these two gems
require "zabbixapi"
require "oml4r"

# # Zabbix node names
nodes = [""]

# Define your own Measurement Point
class PING_MP < OML4R::MPBase
    name :PING
    param :ts, :type => :string
    param :node, :type => :string
    param :lastvalue, :type => :double
    param :lastclock, :type => :string
end


# Initialise the OML4R module for your application
oml_opts = {
    :appName => '',
    :domain => '',
    :nodeID => '',
    :collect => ''
}
zabbix_opts = {
    :url => '',
    :user => '',
    :password => ''
}

interval = 1000

nodes = OML4R::init(nodes, oml_opts) do |op|
    op.banner = "Usage: #{$0} [options] host1 host2 ...\n"
    
    op.on( '-i', '--interval SEC', "Query interval in seconds [#{interval}]" ) do |i|
        interval = i.to_i
    end
    op.on( '-s', '--service-url URL', "Zabbix service url [#{zabbix_opts[:url]}]" ) do |u|
        zabbix_opts[:url] = p
    end
    op.on( '-p', '--password PW', "Zabbix password [#{zabbix_opts[:password]}]" ) do |p|
        zabbix_opts[:password] = p
    end
    op.on( '-u', '--user USER', "Zabbix user name [#{zabbix_opts[:user]}]" ) do |u|
        zabbix_opts[:user] = u
    end
end
if nodes.empty?
    OML4R.logger.error "Missing host list"
    OML4R::close()
    exit(-1)
end

# connect to Zabbix JSON API
zbx = ZabbixApi.connect(zabbix_opts)

# catch CTRL-C
exit_requested = false
Kernel.trap( "INT" ) { exit_requested = true }

# poll Zabbix API
#while
    nodes.each{|n|
        # https://www.zabbix.com/documentation/2.0/manual/appendix/api/item/get
        puts "#{n}"
        results = zbx.query(
                            :method => "item.get",
                            :params => {
                            :output => "extend",
                            :host => "#{n}",
                            # only interested in ICMP packets
                            :search => {
                            :name => "ICMP"
                            }
                            }
                            )
                            unless results.empty?
                                last = results[0]["lastvalue"]
                                lastclock = results[2]["lastclock"]
                                #puts "Injecting values #{l1}, #{l5}, #{l15} for node #{n}"
                                # injecting measurements into OML
                                PING_MP.inject(Time.now.to_s, n, last, lastclock)
                                puts "#{lastclock}"
                                puts "lastvalue #{last}"
                                puts "------"
                                else
                                OML4R.logger.warn "Empty result usually means misspelled host address"
                            end
    }
#    sleep interval
#end

OML4R::close()
puts "Exiting"
