#!/usr/bin/env python
''' 
    User Resources Module. Module will store the username and password for the Zabbix server and the MySQL Database.
    This module will be imported by multiple modules (coordinater, AM Handler, PHP handler and cleaner), but initialized only once.
    Author: Pedro Alvarez
'''

import getpass
import MySQLdb

#Get Zabbix User name and password
zapi_user = ""
zapi_passwd = ""

#Get MySql User name and password
db_user = getpass.getuser()
db_passwd = getpass.getpass("MySQL Password: ")
db_host = "127.0.0.1"
db_name = ""

protected_users = ['']

def open_db():
    """ 
    Opens database for communication
    """
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
         user=db_user,           #
          passwd=db_passwd,    # your password
          db=db_name,)           # name of the database created by userCake
    cur = db.cursor()

    return db, cur

def close_db(db,cur):
    """ 
    Closes the database.
    """
    db.close()           # name of the database created by userCake
    cur.close()
    return


def db_execute(logger, db, cur, db_msg):
    """ 
    Function to query the Database
    """
    try:
        cur.execute(db_msg)
        db.commit()
        logger.debug('Update db success.'+db_msg)
    except:
        logger.error('Update db failed.'+db_msg)
    return
