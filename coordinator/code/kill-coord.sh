#!/bin/bash

echo "About to kill these processes:"
pgrep -u root -f coordinator.py --list-full

read -p "Do you want to proceed?" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo pkill -u root -f coordinator.py
fi

