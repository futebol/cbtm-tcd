#!/usr/bin/env python
'''
    Coordinator's Cleaner Handler.
'''

import os
import MySQLdb
import datetime
import logging
import pandas as pd
import zabbix_ctrl

from user_resources import *

def cleaner_handle(tcpHandler,items):
    ''' 
        Receive requests from the Cleaner and execute the appropriate action.

        Args:
            itens (list): Desired action.

        Returns:
            msg (string): Result of the operation.
    '''
    tcpHandler.logger.debug('Request from cleaner')
    if 'remove_expired' in items:
        msg = remove_expired(tcpHandler)

    elif 'send_reminder' in items:
        msg = send_reminder(tcpHandler)

    elif 'make_clean' in items:
        msg = make_clean(tcpHandler)

    else:
        msg = 'unknown_command'

    return msg

def send_reminder(tcpHandler):
    '''
        Sends reminders about simulations and reservations.

        Returns:
            msg (string): Result of the Operation (send or not notifications; errors)
    '''
    # finds the upcoming reservation
    now = str(datetime.datetime.utcnow())
    offset = str(datetime.datetime.utcnow() + datetime.timedelta(hours=1))
    tcpHandler.logger.debug('Checking upcoming allocations '+str(now))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        selectStatment = 'SELECT * FROM uc_allocations WHERE (start_time BETWEEN %s AND %s)'
        cur.execute(selectStatment, (str(now),str(offset),))
        columns = [column[0] for column in cur.description]
        results = []
        for row in cur.fetchall():
            results.append(dict(zip(columns, row)))     # results contains all the details about the reservation
        cur.close()
        db.close()
    except:
        tcpHandler.logger.error('Query to uc_allocations failed.')
        results = []
        msg = 'Query to uc_allocations failed'
        return msg
    if len(results)==0:
        msg = 'nothing to notify'
    else:
        msg = ''
    fromaddr = 'tcdreservation@gmail.com'
    for entry in results:
        tcpHandler.logger.info('Send reminder to '+entry['email']+ 'for usrp '+str(entry['node_id']))
        toaddrs  = entry['email']
        mail = 'Subject: Upcoming Reservation\n\nYou have an upcoming allocation ('+entry['node_id']+') starting in less than 1 hour.'
        try:
            server = smtplib.SMTP('smtp.gmail.com:587')
            server.starttls()
            server.login(fromaddr,'ctvrreservation')
            server.sendmail(fromaddr, toaddrs, mail)
            server.quit()
            msg = msg + 'email sent to '+entry['email']+ 'for node '+entry['node_id']+';'
        except:
            msg = 'failed to send reminder'
            return msg

    tcpHandler.logger.debug('Checking upcoming simulations '+str(now))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        selectStatment = 'SELECT * FROM uc_simulations WHERE (start_time BETWEEN %s AND %s)'
        cur.execute(selectStatment, (str(now),str(offset),))
        columns = [column[0] for column in cur.description]
        results = []
        for row in cur.fetchall():
            results.append(dict(zip(columns, row)))     # results contains all the details about the reservation
        cur.close()
        db.close()
    except:
        tcpHandler.logger.error('Query to uc_simulations failed.')
        cur.close()
        db.close()
        results = []
        msg = 'Query to uc_simulations failed'
        return msg
    if len(results)==0:
        msg = msg+',no simulations'
    fromaddr = 'tcdreservation@gmail.com'
    for entry in results:
        toaddrs  = entry['email']
        mail = 'Subject: Upcoming Reservation\n\nYou have an upcoming VM starting in less than 1 hour.'
        try:
            server = smtplib.SMTP('smtp.gmail.com:587')
            server.starttls()
            server.login(fromaddr,'ctvrreservation')
            server.sendmail(fromaddr, toaddrs, mail)
            server.quit()
            tcpHandler.logger.info('Reminder sent to '+entry['email']+ 'for sim_id '+str(entry['id'])+ '. Core: '+str(entry['cores'])+', Memory: '+str(entry['memory']))
            msg = msg + 'email sent to '+entry['email']+ 'for VM sim_id '+str(entry['id'])+ '. Core: '+str(entry['cores'])+', Memory: '+str(entry['memory'])
        except:
            tcpHandler.logger.error('Reminder to '+entry['email']+ 'for sim_id '+str(entry['id'])+ ' failed')
            msg = 'failed to send reminder'
            return msg

    return msg

def remove_expired(tcpHandler):
    ''' 
        Removes expired reservations and simulations. Also communicates with
        the CBTm to release expired resources.

        Returns:
            msg (string): Result of the operation.
    '''
    now = str(datetime.datetime.utcnow())
    today = str(datetime.datetime.utcnow()).split(' ')[0] +'.csv'
    
    ### simulation part
    #
    remove_expired_v2(tcpHandler,now,today,'simulations','simulation_eu_')
   
    #remove expired x310_allocations
    remove_expired_v2(tcpHandler,now,today,'x310_allocations','basic_eu_')

    remove_expired_v2(tcpHandler,now,today,'n210beam_allocations','basic_eu_')

    #tcpHandler.logger.info('Deleting expired allocations at time '+str(now))
    db, cur = open_db()
    db_msg = "SELECT * FROM uc_allocations WHERE ('%s' > end_time)" % str(now)
    try:
        db_execute(tcpHandler.logger,db,cur,db_msg)
        columns = [column[0] for column in cur.description]
        results = []
        for row in cur.fetchall():
            results.append(dict(zip(columns, row)))     # results contains all the details about the reservation
    except:
        return "Error optaining expired USRP allocations"
    finally:
        close_db(db,cur)

    # save to a .csv the deleted rows
    save_expired_to_csv(tcpHandler, results, today, 'usrp')

    for entry in results:
        if entry['provisioned'] == 0:
            tcpHandler.logger.debug('Reservation id '+str(entry['id'])+' has never been provisioned by user '+str(entry['user_name'])+' on resource '+str(entry['node_id']))
        else:
            tcpHandler.logger.warning('Reservation id '+str(entry['id'])+' has been provisioned to user '+str(entry['user_name'])+' on resource '+str(entry['node_id']+' and must be released.'))
            '''CBTm'''
            CBTmCommand = 'release_unit:'+str(entry['node_id'])
            msg = tcpHandler.CBTmProtocol(CBTmCommand)

            if any(entry['node_id'] == host for host in tcpHandler.zabbix_hosts.keys()):
                zabbix_ctrl.delete_zabbix_host(tcpHandler,entry['node_id'])


    db, cur = open_db()
    db_msg = "DELETE FROM uc_allocations WHERE ('%s' > end_time)" % str(now)
    try:
        rows_affected = 0
        db_execute(tcpHandler.logger,db,cur,db_msg)
        rows_affected = cur.rowcount
    except:
        return "Error deleting expired USRP allocations"
    finally:
        close_db(db,cur)

    if rows_affected>0:
        tcpHandler.logger.info('Deleting '+str(rows_affected)+' rows from uc_allocations.')
    msg = 'Deleting '+str(rows_affected)+' rows from uc_allocations.'

    #Delete Fed4Fire Users that do not have reservations
    db, cur = open_db()
    db_msg = 'SELECT user_name FROM uc_users'
    php_users = []
    try:
        db_execute(tcpHandler.logger,db,cur,db_msg)
        for row in cur.fetchall():
            php_users.append(row[0])
    except:
        return "Error getting PHP users"
    finally:
        close_db(db,cur)

    #Get reservation users
    
    #print php_users
    reservation_users = os.popen("grep 'reservation' /etc/group").read()[:-1].split(',')[1:]
    #print reservation_users
    f4f_users = [user for user in reservation_users if user not in php_users]
    print f4f_users
    db, cur = open_db()
    for f4f_user in f4f_users:
        db_msg = "SELECT user_name FROM uc_allocations WHERE user_name = '%s' union SELECT user_name FROM uc_simulations WHERE user_name = '%s' union SELECT user_name FROM uc_n210beam_allocations WHERE user_name = '%s' union SELECT user_name FROM uc_x310_allocations WHERE user_name = '%s'" % (f4f_user, f4f_user, f4f_user, f4f_user)
#         db_msg = "SELECT * FROM uc_allocations WHERE user_name = '%s' " % f4f_user
        db_execute(tcpHandler.logger,db,cur,db_msg)
        if cur.rowcount == 0:
            tcpHandler.logger.warning('User '+str(f4f_user)+' has no reservations. Deleting the user.')
            delete_user(tcpHandler,f4f_user)
    close_db(db,cur)
    return msg

def remove_expired_v2(tcpHandler,now,today,table_name,domain_str):
    ''' 
        Removes expired reservations and simulations. Also communicates with
        the CBTm to release expired resources.

        Returns:
            msg (string): Result of the operation.
    '''
    #tcpHandler.logger.info('Deleting expired simulation allocations at time '+str(now))
    db, cur = open_db()
    db_msg = "SELECT * FROM uc_"+table_name+" WHERE ('%s' > end_time)" % str(now)
    try:
        db_execute(tcpHandler.logger,db,cur,db_msg)
        columns = [column[0] for column in cur.description]
        results = []
        for row in cur.fetchall():
            results.append(dict(zip(columns, row)))     # results contains all the details about the reservation
    except:
        return "Error optaining expired vm "+table_name +" allocations"
    finally:
        close_db(db,cur)

    # save to a .csv the deleted rows
    save_expired_to_csv(tcpHandler, results, today, 'simulations')

    for entry in results:
        if entry['provisioned'] == 0:
            tcpHandler.logger.debug('Reservation id '+str(entry['id'])+' has never been provisioned by user '+str(entry['user_name']))
        else:
            if domain_str == "basic_eu_":
               tcpHandler.logger.info(results)
               tcpHandler.logger.warning('Reservation id '+str(entry['node_id'])+' has been provisioned to user '+str(entry['user_name'])+' and must be released.')
               '''CBTM vm_id,rack_id,domain_str'''
               CBTmCommand = 'release_sim_unit:'+str(entry['node_id'])+','+domain_str
               msg = tcpHandler.CBTmProtocol(CBTmCommand)
            else: 
               tcpHandler.logger.warning('Reservation id '+str(entry['id'])+' has been provisioned to user '+str(entry['user_name'])+' and must be released.')
               '''CBTM vm_id,rack_id,domain_str'''
               CBTmCommand = 'release_sim_unit:'+str(entry['id'])+','+domain_str
               msg = tcpHandler.CBTmProtocol(CBTmCommand)

            #if any(entry['node_id'] == host for host in tcpHandler.zabbix_hosts.keys()):
            #    zabbix_ctrl.delete_zabbix_host(tcpHandler,entry['node_id'])


    db, cur = open_db()
    db_msg = "DELETE FROM uc_"+table_name+" WHERE ('%s' > end_time)" % str(now)
    try:
        rows_affected = 0
        db_execute(tcpHandler.logger,db,cur,db_msg)
        rows_affected = cur.rowcount
    except:
        return "Error deleting expired vm simulations"
    finally:
        close_db(db,cur)

    if rows_affected>0:
        tcpHandler.logger.info('Deleting '+str(rows_affected)+' rows from uc_'+table_name)
    msg = 'Deleting '+str(rows_affected)+' rows from uc_'+table_name


def save_expired_to_csv(tcpHandler,results,today,reservation_type):
    '''
        Export expired reservations to a csv log.

        Args:
            results (list): Results from a Query to the Database, containing all
                information about the reservation
            today (string): Contains today's date.
            reservation_type (string): Reservation type (usrp or simulation)
    '''
    for entry in results:
        list_files = os.popen('ls ../log/'+str(reservation_type)+'_reservations/').read().split('\n')
        if entry['start_time'] < entry['end_time']:
            if any (today == f for f in list_files):
                df = pd.read_csv('../log/'+str(reservation_type)+'_reservations/'+today)
                df = df.append(pd.DataFrame(entry, index = [1]), ignore_index=True)
            else:
                df = pd.DataFrame(entry, index = [1])
            df.to_csv('../log/'+str(reservation_type)+'_reservations/'+today, index=False)



def delete_user(tcpHandler, user_id):
    ''' 
        Delete User on the Gateway.

        Args:
            user_id (string): Username to be deleted.

        Returns:
            msg (string): Status of the request.
    '''
    user_id = user_id.replace(' ','')
    global protected_users
    if any (user_id == u for u in protected_users):
        tcpHandler.logger.warning('Cannot delete a user because its in the list of protected users')
        msg = user_id + 'is a protected user cannot be deleted'
        return msg
    else:
        tcpHandler.logger.info('deleting user '+str(user_id))
        try:
            os.popen("pkill -u "+user_id)   # kill all active processes for user user_id
            os.popen("deluser --remove-home "+user_id)  # delete the user and the its $HOME
            tcpHandler.logger.debug('user '+user_id+' successfully removed.')
        except:
            tcpHandler.logger.error('could not remove user_id '+str(user_id))
            msg = 'something went wront deleting '+user_id
            return msg

    zabbix_ctrl.delete_zabbix_user(tcpHandler, user_id)
    msg = 'everything went fine deleting user '+user_id
    return msg

def make_clean(tcpHandler):
    tcpHandler.logger.debug("In make clean function.")
    all_usrps = ["11","12","13","14","21","22","23","24","31","32","33","34","41","42","43","44","51","52","53","54","63","64"]
    now = str(datetime.datetime.utcnow())
    db, cur = open_db()
    db_msg = "SELECT node_id FROM uc_allocations WHERE ('%s' > start_time) AND ('%s' < end_time) UNION SELECT node_id FROM uc_x310_allocations WHERE ('%s' > start_time) AND ('%s' < end_time) UNION SELECT node_id FROM uc_n210beam_allocations WHERE ('%s' > start_time) AND ('%s' < end_time)" % (str(now), str(now), str(now), str(now), str(now), str(now))
    db_execute(tcpHandler.logger,db,cur,db_msg)
    active_usrps=[]
    for node_id in cur.fetchall():
        active_usrps.append(node_id[0])
    non_active_usrps = [usrp for usrp in all_usrps if usrp not in active_usrps ]
    if active_usrps != []:
        tcpHandler.logger.debug('Talking with the CBTM to make sure that everything is clean')
        CBTmCommand = 'make_clean:'+','.join(non_active_usrps)
        tcpHandler.logger.debug(CBTmCommand)
        msg = tcpHandler.CBTmProtocol(CBTmCommand)

    return 'ok'
