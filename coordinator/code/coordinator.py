#!/usr/bin/env python
'''
    Coordinator Module. This is the central module of our Aggregate Manager.
    Author: pdifranc
    Contributors: Joao Pinheiro and Pedro Alvarez
'''

import SocketServer
import socket
import datetime
import logging
import os
import pandas as pd
import MySQLdb
import smtplib
import ssl
import argparse

from user_resources import *

import zabbix_ctrl
import am_handler
import cleaner_handler
import php_handler

from pyzabbix import ZabbixAPI

#ZABBIX_SERVER = '134.226.55.38'
ZABBIX_SERVER = 'iris-testbed.connectcentre.ie'
#ZABBIX_SERVER = '134.226.55.214'
#ZABBIX_SERVER = '192.168.122.173'
'''
    Zabbix Server IP
'''

_resources = {'13':['True','192.168.5.13'],'14':['True','192.168.5.14'],
              '21':['True','192.168.5.21'],'22':['True','192.168.5.22'],
              '23':['True','192.168.5.23'],'24':['True','192.168.5.24'],
              '31':['True','192.168.5.31'],'32':['True','192.168.5.32'],
              '33':['True','192.168.5.33'],'34':['True','192.168.5.34'],
              '41':['True','192.168.5.41'],'42':['True','192.168.5.42'],
              '51':['True','192.168.5.51'],'52':['True','192.168.5.52'],
              '53':['True','192.168.5.53'],'54':['True','192.168.5.54'],
              '63':['True','192.168.5.63'],'64':['True','192.168.5.64'],
             }

_n210_900_res = {'13':['True','192.168.5.13'],
              '22':['True','192.168.5.22'],
              '23':['True','192.168.5.23'],
              '31':['True','192.168.5.31'],
              '33':['True','192.168.5.33'],
              '41':['True','192.168.5.41'],
             }

_n210_2450_res = {'14':['True','192.168.5.14'],
              '22':['True','192.168.5.22'],
              '24':['True','192.168.5.24'],
              '32':['True','192.168.5.32'],
              '34':['True','192.168.5.34'],
              '42':['True','192.168.5.42'],
             }


_n210_beam_res = {'12':['True','192.168.5.13'],'43':['True','192.168.5.14'],
             }

_x310_res = {'11':['True','192.168.5.11'],'44':['True','192.168.5.44'],
             }


'''
    In reality these IP are fake because the IP address should come from the CBTm
'''

_zabbix_users = dict()
'''
    List of Zabbix users
'''

_zabbix_hosts = dict()
'''
    List of Zabbix Hosts
'''

zapi = []

g_log_level="info"

def get_zabbix_users(zapi_user,zapi_passwd):
    '''
        Retrives a list of users in the zabbix server.
    '''
    global zapi
    global _zabbix_users
    global _zabbix_hosts
    # retrive zabbix users

    #zapi = ZabbixAPI(url='http://'+ZABBIX_SERVER+'/zabbix/', user=zapi_user['user'],
    #    password=zapi_user['passwd'])
    zapi = ZabbixAPI('https://'+ZABBIX_SERVER+'/zabbix/')
    zapi.login(zapi_user,zapi_passwd)


    zabbix_users = zapi.user.get(output='extend')
    for zabbix_user in zabbix_users:
        _zabbix_users[zabbix_user['alias']] = zabbix_user['userid']

    zabbix_hosts = zapi.host.get(output='extend')
    for zabbix_host in zabbix_hosts:
        _zabbix_hosts[zabbix_host['host']] = zabbix_host['hostid']


class MyTCPHandler(SocketServer.BaseRequestHandler):
    """
        The RequestHandler class for our server.
        It is instantiated once per connection to the server, and must
        override the handle() method to implement communication to the
        client.

        Parameters:
            level (int): Level of the Logger
            logger (logger): Coordinator Logger
            handler (logging.FileHandler): Logger Handler
            formatter (logging.Formatter): Format of Logging
            resources (dict): List of (Fake) resources
            zabbix_users (list): List of Zabbix Users
            Zabbix_hosts (list): List of Zabbix Hosts
    """
    #def __init__(self, request, client_address, server):
        
    #level_dict={"critical":logging.CRITICAL, "error":logging.ERROR, "warning":logging.WARNING, "info":logging.INFO, "debug":logging.DEBUG, "notset":logging.NOTSET}
    #level = level_dict[server.log_level]
    """ 
    level = logging.INFO
    logging.basicConfig(level=level)
    logger = logging.getLogger('coordinator')
    # log file
    handler = logging.FileHandler('../log/log-coordinator.log')
    handler.setLevel(level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(funcName)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.info("Init")
    logger.debug("Debug init")
    """
    resources = _resources
    x310_res = _x310_res
    n210_beam_res = _n210_beam_res
    n210_2450_res = _n210_2450_res
    n210_900_res = _n210_900_res

    zabbix_users = _zabbix_users
    zabbix_hosts = _zabbix_hosts

    #zapi = ZabbixAPI(url='http://'+ZABBIX_SERVER+'/zabbix/', user=zapi_user['user'], password=zapi_user['passwd'])
    zapi = ZabbixAPI('https://'+ZABBIX_SERVER+'/zabbix/')
    zapi.login(zapi_user,zapi_passwd)

    #SocketServer.BaseRequestHandler.__init__(self, request, client_address, server)
    #SocketServer.BaseRequestHandler.setup(self)

    def handle(self):
        """
            Receives requests from clients and redirects to the appropriate
            handler.
        """
        self.logger=self.server.logger
        # self.request is the TCP socket connected to the client
        self.data = self.request.recv(1024).strip()
        who = self.data.split(':')[0]   # From who the requesti is coming from (i.e., AM, PHP, cleaner)
        self.data = self.data.split('::')[1]
        items = self.data.split(',')
        if who == 'AM':
            msg = am_handler.am_handle(self,items)

        elif who == 'php':
            msg = php_handler.php_handle(self,items)

        elif who == 'cleaner':
            msg = cleaner_handler.cleaner_handle(self,items)

        self.logger.debug('gateway replies: '+str(msg))
        self.request.sendall(msg)

    def CBTmProtocol(self,msg):
        """
            Implements communication between the Coordinator and the CBTM, using
            the CBTProtocol.

            Args:
                msg (string): Command to be send to the CBTm server.

            Returns:
                received (string): Response from the CBTm server.
        """
        # Create a socket (SOCK_STREAM means a TCP socket)
        host = '127.0.0.1' #Frontend IP
        port = 5006
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        wrapped_sock = ssl.wrap_socket(sock,certfile='../resources/cbtm_key_and_cert.pem')
        try:
            # Connect to server and send data
            wrapped_sock.connect((host, port))
            wrapped_sock.sendall(msg + "\n")
            # Receive data from the server and shut down
            received = wrapped_sock.recv(1024)
        finally:
            sock.close()
        return received

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Select Log level.')
    parser.add_argument('--log-level', action="store", type=str, choices=["critical", "error", "warning", "info", "debug", "notset"], default="info",
                    help='Select the log level of the program.')

    args = parser.parse_args()

    #level = args.log_level
    level_dict={"critical":logging.CRITICAL, "error":logging.ERROR, "warning":logging.WARNING, "info":logging.INFO, "debug":logging.DEBUG, "notset":logging.NOTSET}
    level = level_dict[args.log_level]
    logging.basicConfig(level=level)
    logger = logging.getLogger('coordinator')
    # log file
    handler = logging.FileHandler('../log/log-coordinator.log')
    handler.setLevel(level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(funcName)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.info("Init")
    logger.debug("Debug init")

    get_zabbix_users(zapi_user,zapi_passwd)
    host, port = "localhost", 9999
    server = SocketServer.TCPServer((host, port), MyTCPHandler)
    server.logger=logger
    # interrupt the program with Ctrl-C
    server.serve_forever()
