#!/usr/bin/env python
'''
    Coordinator's GCF Aggregate Manager Handler. For more information about the
    supported functions:
        https://fed4fire-testbeds.ilabt.iminds.be/asciidoc/federation-am-api.html
'''

import logging
import MySQLdb
import os
import datetime
from datetime import timedelta
import zabbix_ctrl
import threading
from user_resources import *

lock = threading.Lock()

def am_handle(tcpHandler,items):
    '''
        Receive requests from the GCF Aggregate Manager and execute the
        appropriate action.

        Args:
            itens (list): Desired action.

        Returns:
            msg (string): Result of the operation.
    '''
    tcpHandler.logger.debug('Request from AM: '+items[0])
    if 'delete' in items: #changed delete_user to delete
        [user_id] = [items[1]]
        delete_user(tcpHandler,user_id)
        msg = 'delete_user called for user_id '+str(user_id)

    elif 'listresource' in items:
        msg = listresource(tcpHandler)

    elif 'list_x310_resource' in items:
        msg = list_x310_resource(tcpHandler)

    elif 'list_n210beam_resource' in items:
        msg = list_n210beam_resource(tcpHandler)

    elif 'list_vms_resource' in items:
        msg = list_vms_resource(tcpHandler)
        
    elif 'allocate' in items:
        [usrp, user_id, email, expiration_time, img_type] = [items[1],items[2],items[3],items[4],items[5]]
        msg = allocate(tcpHandler, usrp, user_id, email, expiration_time, img_type)

    elif 'make_reservation_x310' in items:
        tcpHandler.logger.info("test here")
        [usrp, user_id, email, expiration_time, img_type] = [items[1],items[2],items[3],items[4],items[5]]
        msg = make_x310_reservation(tcpHandler, usrp, user_id, email, expiration_time, img_type)

    elif 'make_reservation_n210beam' in items:
        [usrp, user_id, email, expiration_time, img_type] = [items[1],items[2],items[3],items[4],items[5]]
        msg = make_n210beam_reservation(tcpHandler, usrp, user_id, email, expiration_time, img_type)

    elif 'allocate' in items:
        [usrp, user_id, email, expiration_time, img_type] = [items[1],items[2],items[3],items[4],items[5]]
        msg = allocate(tcpHandler, usrp, user_id, email, expiration_time, img_type)

    elif 'provision' in items:
        [usrp,img_type,user_id,user_key,email,type_res] = [items[1],items[2],items[3],items[4],items[5],items[6]]
        msg = provision_AM(tcpHandler,usrp,img_type,user_id,user_key,email,type_res)

    elif ('provisionVM' in items):
        [id_res,rack_id,memory,domain_str,cores,img_type,user_id,user_key,email] = [items[1],items[2],items[3],items[4],items[5],items[6],items[7],items[8],items[9]]
        msg = provision_VM(tcpHandler,id_res,rack_id,memory,domain_str,cores,img_type,user_id,user_key,email)

    elif (('provision_x310' in items) and ('usrp' in item for item in items)):
        [usrp,img_type,user_id,user_key,email,type_res] = [items[1],items[2],items[3],items[4],items[5],items[6]]
        msg = provision_x310_PHP(tcpHandler,usrp,img_type,user_id,user_key,email,type_res)

    elif (('provision_n210beam' in items) and ('usrp' in item for item in items)):
        [usrp,img_type,user_id,user_key,email,type_res] = [items[1],items[2],items[3],items[4],items[5],items[6]]
        msg = provision_n210beam_PHP(tcpHandler,usrp,img_type,user_id,user_key,email,type_res)

    elif 'status' in items:
        [usrp] = [items[1]]
        msg = status_usrp(tcpHandler,usrp)

    elif 'describe' in items:
        [usrp] = [items[1]]
        msg = describe(tcpHandler,usrp)

#     elif 'describeVM' in items:
#         [sim_id,rack_id,domain_str] = [items[1],items[2],items[3]]
#         msg = describe_VM(tcpHandler,sim_id,rack_id,domain_str)

    elif (('describe_x310' in items) and ('usrp' in item for item in items)):
        [usrp] = [items[1]]
        msg = describex310PHP(tcpHandler,usrp)

    elif (('describe_n210beam' in items) and ('usrp' in item for item in items)):
        [usrp] = [items[1]]
        msg = describen210beamPHP(tcpHandler,usrp)

    elif 'release' in items:
        [usrp] = [items[1]]
        msg = release(tcpHandler,usrp)

    elif 'delete_allocation_x310' in items:
        [id_res,user_id] = [items[1],items[2]]
        msg = delete_x310_allocation(tcpHandler,id_res,user_id)

    elif 'delete_simulation' in items:
        [sim_id,domain_str,user_id] = [items[1],items[2],items[3]]
        msg = delete_simulation(tcpHandler,sim_id,domain_str,user_id)

    elif 'delete_allocation_n210beam' in items:
        [id_res,user_id] = [items[1],items[2]]
        msg = delete_n210beam_allocation(tcpHandler,id_res,user_id)

    elif 'renew' in items:
        [usrp,user_id,end_time] = [items[1],items[2],items[3]]
        msg = renew(tcpHandler,usrp,user_id,end_time)

    elif 'renewVM' in items:
        [usrp,user_id,end_time] = [items[1],items[2],items[3]]
        msg = renewVM(tcpHandler,usrp,user_id,end_time)

    elif 'renewX310' in items:
        [usrp,user_id,end_time] = [items[1],items[2],items[3]]
        msg = renewX310(tcpHandler,usrp,user_id,end_time)

    elif 'renewBeam' in items:
        [usrp,user_id,end_time] = [items[1],items[2],items[3]]
        msg = renewBeam(tcpHandler,usrp,user_id,end_time)

    elif 'PerformOperationalAction'in items:
        [action,usrp] = [items[1],items[2]]
        msg = PerformOperationalAction(tcpHandler,usrp,action)

    elif 'PerformVMOperationalAction'in items:
        [vm_name,action,usrp] = [items[1],items[2],items[3]]
        msg = PerformVMOperationalAction(tcpHandler,vm_name,usrp,action)

    elif 'checkavailability' in items:
        [usrp, start_time, end_time] = [items[1], items[2], items[3]]
        msg = check_availability(usrp,start_time,end_time)

    elif 'check_n210beam_availability' in items:
        [usrp, start_time, end_time] = [items[1], items[2], items[3]]
        msg = check_n210beam_availability(usrp,start_time,end_time)

    elif 'check_x310_availability' in items:
        [usrp, start_time, end_time] = [items[1], items[2], items[3]]
        msg = check_x310_availability(usrp,start_time,end_time)

    elif 'anyavailable' in items:
        [start_time, exp_time] = [items[1],items[2]]
        msg = any_available(tcpHandler, start_time, exp_time)

    elif 'any_x310_available' in items:
        [start_time, exp_time] = [items[1],items[2]]
        msg = any_x310_available(tcpHandler, start_time, exp_time)

    elif 'any_n210beam_available' in items:
        [start_time, exp_time] = [items[1],items[2]]
        msg = any_n210beam_available(tcpHandler, start_time, exp_time)

    return msg

def create_user(tcpHandler,user_id,ssh_key,email):
    ''' 
        Creates user in the Gateway for use with the Provisioned VM

        Args:
            user_id (string): Username
            ssh_key (string): User's SSH key
            email (String): User's Email

        Returns:
            msg (string): Status of the request.
    '''
    user_id = user_id.replace(' ','')
    global protected_users
    if any (user_id == u for u in protected_users):
        tcpHandler.logger.warning('Cannot create a user because its in the list of protected users')
        msg = 'Cannot create a user because its in the list of protected users'
        return msg
    else:
        list_users = os.popen("ls -r /home").read().split('\n')
        if any (user_id == u for u in list_users):
            tcpHandler.logger.info('user '+str(user_id)+' already deployed')
        else:
            tcpHandler.logger.info('create user '+str(user_id))
            try:
                os.popen("useradd -m "+user_id+" -s /usr/sbin/nologin")         # create user_id with $HOME
                os.popen("adduser "+user_id+" reservation") # add user to group reservation (limit what users can do)
                os.popen("mkdir /home/"+user_id+"/.ssh")             # create ~/.ssh
                os.popen("> /home/"+user_id+"/.ssh/authorized_keys") # create the file
                tcpHandler.logger.debug('user '+str(user_id)+' successfully created.')
            except:
                tcpHandler.logger.error('Something went wrong with creation of user '+str(user_id))
                msg = 'Something went wrong with creation of user '+str(user_id)
                return msg
        #f = open('/home/'+user_id+'/.ssh/authorized_keys', 'r')
        authorized_keys = []
        if any(ssh_key in k for k in authorized_keys):
            tcpHandler.logger.debug('user'+str(user_id)+' already authorized.')
        else:
            tcpHandler.logger.debug('add the key to authorized_keys for '+str(user_id))
            authorized_keys.append(str(ssh_key)+'\n')

        f = open('/home/'+user_id+'/.ssh/authorized_keys', 'w')
        for keys in authorized_keys:
            f.write(keys)
        f.close()
        tcpHandler.logger.info('Changing permissions on $HOME for user '+user_id)
        os.popen("chmod 544 /home/"+user_id+" -R")                          # change authorizations (make home folder only executable
        os.popen("chown "+user_id+":"+user_id+" /home/"+user_id+"/.ssh -R") # ownership of this folder goes back to user_id
        try:
            zabbix_ctrl.create_zabbix_user(tcpHandler,user_id,email)
            tcpHandler.logger.info('Create user '+user_id+' in Zabbix Server.')
        except:
            tcpHandler.logger.error('User '+user_id+' creation in Zabbix Server failed.')
            msg = 'Could not create user '+user_id+' in Zabbix server'
            return msg
        msg = 'Everything went fine for user '+user_id
        return msg
        # this stuff is for .Xauthority but it doesn't work
        #os.popen("touch /home/"+user_id+"/.Xauthority")
        #os.popen("chmod 744 /home/"+user_id+"/.Xauthority")
        #os.popen("chown "+user_id+":"+user_id+" /home/"+user_id+"/.Xauthority")

def delete_user(tcpHandler,user_id):
    ''' 
        Delete User on the Gateway.

        Args:
            user_id (string): Username to be deleted.

        Returns:
            msg (string): Status of the request.
    '''
    user_id = user_id.replace(' ','')
    global protected_users
    if any (user_id == u for u in protected_users):
        tcpHandler.logger.warning('Cannot delete a user because its in the list of protected users')
        msg = user_id + 'is a protected user cannot be deleted'
        return msg
    else:
        tcpHandler.logger.info('deleting user '+str(user_id))
        try:
            os.popen("pkill -u "+user_id)   # kill all active processes for user user_id
            os.popen("deluser --remove-home "+user_id)  # delete the user and the its $HOME
            tcpHandler.logger.debug('user '+user_id+' successfully removed.')
        except:
            tcpHandler.logger.error('could not remove user_id '+str(user_id))
            msg = 'something went wront deleting '+user_id
            return msg

    zabbix_ctrl.delete_zabbix_user(tcpHandler, user_id)
    msg = 'everything went fine deleting user '+user_id
    return msg

def listresource(tcpHandler):
    ''' 
        List the Testbed resources querying the CBTm and Database.
        If the resource has a reservation in the database that was not provided,
        it counts as an occupied resource.
    '''
    now = datetime.datetime.utcnow()
    now=now.replace(microsecond=0)
    grid_response = tcpHandler.CBTmProtocol('grid_status:get')
    for usrp in tcpHandler.resources.keys():
        if usrp in grid_response:
            #USRPs in use
            tcpHandler.resources[usrp][0] = 'False'
        else:
            #Free USRPs
            tcpHandler.resources[usrp][0] = 'True'

    #This query the database for resources that are allocated but not provided.
    tcpHandler.logger.info('Querying database for free resources at timestamp '+str(now))
    db = MySQLdb.connect(host=db_host,
             user=db_user,
              passwd=db_passwd,
              db=db_name,)
    cur = db.cursor()
    db_msg=("SELECT node_id FROM uc_allocations WHERE '%s' BETWEEN start_time AND end_time" % str(now))
    print db_msg
    try:
        cur.execute(db_msg) #change for expiration time
        tcpHandler.logger.debug('Query to db successful.')
    except:
        tcpHandler.logger.error('Query to db failed.')
    for row in cur.fetchall():
        #if the database query has any occupied usrps from now to exp_time, if becomes unavailable
        usrp = row[0]
        tcpHandler.resources[usrp][0] = 'False'
    cur.close()
    db.close()
    msg = ""

    for k in tcpHandler.resources.keys():
        msg += k+','+tcpHandler.resources[k][0]+'\n'
    return msg

def list_n210beam_resource(tcpHandler):
    ''' 
        List the Testbed n210 beam resources querying the CBTm and Database.
        If the resource has a reservation in the database that was not provided,
        it counts as an occupied resource.
    '''
    now = datetime.datetime.utcnow()
    now=now.replace(microsecond=0)
    grid_response = tcpHandler.CBTmProtocol('grid_status:get')
    for usrp in tcpHandler.n210_beam_res.keys():
        if usrp in grid_response:
            #USRPs in use
            tcpHandler.n210_beam_res[usrp][0] = 'False'
        else:
            #Free USRPs
            tcpHandler.n210_beam_res[usrp][0] = 'True'

    #This query the database for resources that are allocated but not provided.
    tcpHandler.logger.info('Querying database for free n210 beam resources at timestamp '+str(now))
    db = MySQLdb.connect(host=db_host,
             user=db_user,
              passwd=db_passwd,
              db=db_name,)
    cur = db.cursor()
    db_msg=("SELECT node_id FROM uc_n210beam_allocations WHERE '%s' BETWEEN start_time AND end_time" % str(now))
    print db_msg
    try:
        cur.execute(db_msg) #change for expiration time
        tcpHandler.logger.debug('Query to db successful.')
    except:
        tcpHandler.logger.error('Query to db failed.')
    for row in cur.fetchall():
        #if the database query has any occupied usrps from now to exp_time, if becomes unavailable
        usrp = row[0]
        tcpHandler.n210_beam_res[usrp][0] = 'False'
    cur.close()
    db.close()
    msg = ""

    for k in tcpHandler.n210_beam_res.keys():
        msg += k+','+tcpHandler.n210_beam_res[k][0]+'\n'
    tcpHandler.logger.info("list n210 beam resource: " + msg)
    return msg

def list_vms_resource(tcpHandler):
    ''' 
        List the Testbed vm resources querying the Database.
        If the resource has a reservation in the database that was not provided,
        it counts as an occupied resource.
    '''
    with lock:
        now = datetime.datetime.utcnow()
        now=now.replace(microsecond=0)
    
        #This query the database for resources that are allocated but not provided.
        tcpHandler.logger.info('Querying database for a count of current vm resources at timestamp '+str(now))
        db = MySQLdb.connect(host=db_host,
                 user=db_user,
                  passwd=db_passwd,
                  db=db_name,)
        cur = db.cursor()
        db_msg=("SELECT count(id) FROM uc_simulations WHERE '%s' BETWEEN start_time AND end_time" % str(now))
        print db_msg
        try:
            cur.execute(db_msg) #change for expiration time
            tcpHandler.logger.debug('Query to db successful.')
        except:
            tcpHandler.logger.error('Query to db failed.')
        for row in cur.fetchall():
            #if the database query has any occupied usrps from now to exp_time, if becomes unavailable
            msg = str(70 - int(row[0]))
            
        db_msg=("SELECT Auto_increment FROM information_schema.tables WHERE table_name='uc_simulations'")
        print db_msg
        try:
            cur.execute(db_msg) #change for expiration time
            tcpHandler.logger.debug('Query to db successful.')
        except:
            tcpHandler.logger.error('Query to db failed.')
        for row in cur.fetchall():
            #if the database query has any occupied usrps from now to exp_time, if becomes unavailable
            msg = msg + ","+str(row[0])
            
        cur.close()
        db.close()
    
        tcpHandler.logger.info("list vm resources available of out 12: " + msg)
        return msg

def list_x310_resource(tcpHandler):
    ''' 
        List the Testbed x310 resources querying the CBTm and Database.
        If the resource has a reservation in the database that was not provided,
        it counts as an occupied resource.
    '''
    now = datetime.datetime.utcnow()
    now=now.replace(microsecond=0)
    grid_response = tcpHandler.CBTmProtocol('grid_status:get')
    for usrp in tcpHandler.x310_res.keys():
        if usrp in grid_response:
            #USRPs in use
            tcpHandler.x310_res[usrp][0] = 'False'
        else:
            #Free USRPs
            tcpHandler.x310_res[usrp][0] = 'True'

    #This query the database for resources that are allocated but not provided.
    tcpHandler.logger.info('Querying database for free x310 usrp resources at timestamp '+str(now))
    db = MySQLdb.connect(host=db_host,
             user=db_user,
              passwd=db_passwd,
              db=db_name,)
    cur = db.cursor()
    db_msg=("SELECT node_id FROM uc_x310_allocations WHERE '%s' BETWEEN start_time AND end_time" % str(now))
    print db_msg
    try:
        cur.execute(db_msg) #change for expiration time
        tcpHandler.logger.debug('Query to db successful.')
    except:
        tcpHandler.logger.error('Query to db failed.')
    for row in cur.fetchall():
        #if the database query has any occupied usrps from now to exp_time, if becomes unavailable
        usrp = row[0]
        tcpHandler.x310_res[usrp][0] = 'False'
    cur.close()
    db.close()
    msg = ""

    for k in tcpHandler.x310_res.keys():
        msg += k+','+tcpHandler.x310_res[k][0]+'\n'
    tcpHandler.logger.info("x310 list resource: "+msg)
    return msg



def make_x310_reservation(tcpHandler, usrp, user_id, email, expiration_time, img_type):
    '''
        Creates a reservation for resources.

        Args:
            usrp (int): USRP number to be reserved
            user_id (string): Username
            email (string): Email of the user
            expiration_time (datetime): End time of the reservation

        Returns:
            msg (string): Status of the request.
    '''
    tcpHandler.logger.info('allocate called')
    now = datetime.datetime.utcnow()
    now = now.replace(microsecond=0)
    # Query the DataBase to see if usrp is available at expiration
    db = MySQLdb.connect(host=db_host,
             user=db_user,
              passwd=db_passwd,
              db=db_name,)
    cur = db.cursor()
    tcpHandler.logger.debug('Checking if node_id '+str(usrp)+' is actually available.')
    try:
        db_msg = "SELECT node_id FROM uc_x310_allocations WHERE (node_id=%s) AND ((start_time BETWEEN '%s' AND '%s') OR ('%s' BETWEEN start_time AND end_time))" % (str(usrp),str(now),str(expiration_time),str(now))
        print db_msg
        cur.execute(db_msg)
        tcpHandler.logger.debug('Query to db success.')
    except:
        tcpHandler.logger.error('Query to db failed.')
    columns = [column for column in cur.description]
    results = []
    successful = True
    for row in cur.fetchall():
        results.append(dict(zip(columns, row)))     # results contains all the details about the reservation, if it exists. If a reservation exists, we cannot allocate the resource. 
        successful = False

    cur.close()
    cur = db.cursor()
    if successful:
        try:
            db_msg = "INSERT INTO uc_x310_allocations (node_id,user_name,email,start_time,end_time,type,provisioned,img_type,ip_address) VALUES('%s','%s','%s','%s','%s','%s','0','%s','%s','XXX')" % (str(usrp),str(user_id),str(email),str(now),str(expiration_time),'am',str(img_type))
            cur.execute(db_msg)
            db.commit()
            print db_msg
            tcpHandler.logger.debug('Reservation on the db success.')
            successful=True
            res_id = cur.lastrowid
        except:
            tcpHandler.logger.error('INSERT db failed')
            successful=False
            print db_msg
    cur.close()
    #THIS IS TO GET THE RESERVATION ID.
    cur = db.cursor()
    
    if successful:
        tcpHandler.x310_res[usrp][0] = 'False'
        msg = 'success,'+str(res_id)
    else:
        tcpHandler.x310_res[usrp][0] = 'True'
        msg = 'failed'

    return msg

def make_n210beam_reservation(tcpHandler, usrp, user_id, email, expiration_time, img_type):
    '''
        Creates a reservation for resources.

        Args:
            usrp (int): USRP number to be reserved
            user_id (string): Username
            email (string): Email of the user
            expiration_time (datetime): End time of the reservation

        Returns:
            msg (string): Status of the request.
    '''
    tcpHandler.logger.info('allocate called')
    now = datetime.datetime.utcnow()
    now = now.replace(microsecond=0)
    # Query the DataBase to see if usrp is available at expiration
    db = MySQLdb.connect(host=db_host,
             user=db_user,
              passwd=db_passwd,
              db=db_name,)
    cur = db.cursor()
    tcpHandler.logger.debug('Checking if node_id '+str(usrp)+' is actually available.')
    try:
        db_msg = "SELECT node_id FROM uc_n210beam_allocations WHERE (node_id=%s) AND ((start_time BETWEEN '%s' AND '%s') OR ('%s' BETWEEN start_time AND end_time))" % (str(usrp),str(now),str(expiration_time),str(now))
        print db_msg
        cur.execute(db_msg)
        tcpHandler.logger.debug('Query to db success.')
    except:
        tcpHandler.logger.error('Query to db failed.')
    columns = [column for column in cur.description]
    results = []
    successful = True
    for row in cur.fetchall():
        results.append(dict(zip(columns, row)))     # results contains all the details about the reservation, if it exists. If a reservation exists, we cannot allocate the resource. 
        successful = False

    cur.close()
    cur = db.cursor()
    if successful:
        try:
            db_msg = "INSERT INTO uc_n210beam_allocations (node_id,user_name,email,start_time,end_time,type,provisioned,img_type,ip_address) VALUES('%s','%s','%s','%s','%s','%s','0','%s','%s','XXX')" % (str(usrp),str(user_id),str(email),str(now),str(expiration_time),'am',str(img_type))
            cur.execute(db_msg)
            db.commit()
            print db_msg
            tcpHandler.logger.debug('Reservation on the db success.')
            successful=True
            res_id = cur.lastrowid
        except:
            tcpHandler.logger.error('INSERT db failed')
            successful=False
            print db_msg
    cur.close()
    #THIS IS TO GET THE RESERVATION ID.
    cur = db.cursor()

    if successful:
        tcpHandler.n210_beam_res[usrp][0] = 'False'
        msg = 'success,'+str(res_id)
    else:
        tcpHandler.n210_beam_res[usrp][0] = 'True'
        msg = 'failed'

    return msg

def allocate(tcpHandler, usrp, user_id, email, expiration_time, img_type):
    '''
        Creates a reservation for resources.

        Args:
            usrp (int): USRP number to be reserved
            user_id (string): Username
            email (string): Email of the user
            expiration_time (datetime): End time of the reservation

        Returns:
            msg (string): Status of the request.
    '''
    tcpHandler.logger.info('allocate called')
    now = datetime.datetime.utcnow()
    now = now.replace(microsecond=0)
    # Query the DataBase to see if usrp is available at expiration
    db = MySQLdb.connect(host=db_host,
             user=db_user,
              passwd=db_passwd,
              db=db_name,)
    cur = db.cursor()
    tcpHandler.logger.debug('Checking if node_id '+str(usrp)+' is actually available.')
    try:
        db_msg = "SELECT node_id FROM uc_allocations WHERE (node_id=%s) AND ((start_time BETWEEN '%s' AND '%s') OR ('%s' BETWEEN start_time AND end_time))" % (str(usrp),str(now),str(expiration_time),str(now))
        print db_msg
	cur.execute(db_msg)
        tcpHandler.logger.debug('Query to db success.')
    except:
        tcpHandler.logger.error('Query to db failed.')
    columns = [column for column in cur.description]
    results = []
    successful = True
    for row in cur.fetchall():
        results.append(dict(zip(columns, row)))     # results contains all the details about the reservation, if it exists. If a reservation exists, we cannot allocate the resource. 
        successful = False

    cur.close()
    cur = db.cursor()
    if successful:
        try:
            db_msg = "INSERT INTO uc_allocations (node_id,user_name,email,start_time,end_time,type,provisioned,frequency,img_type,ip_address) VALUES('%s','%s','%s','%s','%s','%s','0','%s','%s','XXX')" % (str(usrp),str(user_id),str(email),str(now),str(expiration_time),'am','-',str(img_type))
            cur.execute(db_msg)
            db.commit()
            print db_msg
            tcpHandler.logger.debug('Reservation on the db success.')
            successful=True
            res_id = cur.lastrowid
        except:
            tcpHandler.logger.error('INSERT db failed')
            successful=False
            print db_msg
    cur.close()
    #THIS IS TO GET THE RESERVATION ID.
    cur = db.cursor()
    if successful:
        tcpHandler.resources[usrp][0] = 'False'
        msg = 'success,'+str(res_id)
    else:
        tcpHandler.resources[usrp][0] = 'True'
        msg = 'failed'

    return msg

def provision_AM(tcpHandler,usrp,img_type,user_id,user_key,email,type_res):
    '''
        Provision a reservation with a Virtual machine and an USRP.

        Args:
            usrp (int): Number of the USRP
            img_type (string): type of image to populate the VM
            user_id (string): Username to be deployed
            user_key (string): SSH key of the user
            email (string): User's email
            type_res (string): Type of the reservation (AM)

        Returns:
            msg (string): Status of the request
    '''
    tcpHandler.logger.info('provision_AM called')

    create_user(tcpHandler,user_id,user_key,email)
    now = datetime.datetime.utcnow()
    now = now.replace(microsecond=0)
    now = str(now)
    
    #Getting VM image type from DB
    db, cur = open_db()

    #Get the image type from the database. Use the USRP id and the fact that the provisioning time must be between (now) must be between
    #start and end time of the reservation.
    db_msg = "SELECT img_type FROM uc_allocations WHERE node_id = '%s' and (start_time <= '%s') and ('%s' <= end_time)" % (usrp, now, now)
    tcpHandler.logger.debug("Checking for the USRP image. DB Query: "+db_msg)
    db_execute(tcpHandler.logger, db, cur, db_msg)

    for img_db in cur.fetchall():
        img_type = img_db[0]
    close_db(db,cur)
    
    #img_type="gnuradio"
    if img_type == None:
         msg = usrp+','+tcpHandler.resources[usrp][1]+',not provisioned'

    #CBTM
    tcpHandler.logger.info('Creating VM, Please wait.')
    CBTmCommand = 'request_unit:'+usrp+','+img_type+','+user_id+','+user_key+',n210'
    tcpHandler.logger.info('CBTm Command: '+CBTmCommand)
    msg = tcpHandler.CBTmProtocol(CBTmCommand)
    if 'Accepted' in msg:
        CBTmCommand = 'unit_status:'+usrp
        msg = tcpHandler.CBTmProtocol(CBTmCommand)
    else:
        tcpHandler.logger.error('VM Already Provisioned.')
        return 'Error: Already Provisioned'

    ip_vm = msg.split(':')[1]
    print "IP: ", ip_vm
    tcpHandler.logger.info('VM Provisioned.')
    ############################################################################

    #UPDATE DATABASE
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    tcpHandler.logger.info('user '+str(user_id)+' is creating a provision')
    try:
        cur.execute("""
                    UPDATE uc_allocations
                    SET provisioned = 1 , ip_address = %s
                    WHERE user_name = %s AND (email = %s) AND (type = %s)
                    """,(str(ip_vm),str(user_id),str(email),'am'))
        db.commit()
        msg = usrp+','+ip_vm+',provisioned'
        tcpHandler.logger.debug('UPDATE db successful.')
    except:
        tcpHandler.logger.error('UPDATE db failed.')
        msg = usrp+','+tcpHandler.resources[usrp][1]+',not provisioned'
        cur.close()
        db.close()
        return msg
    cur.close()
    db.close()
    print msg
    return msg

def provision_VM(tcpHandler,id_res,rack_id,memory,domain_str,cores,img_type,user_id,user_key, email):
    '''provision_VM(tcpHandler,id_res,rack_id,memory,domain_str,cores,img_type,user_id,user_key)
        Provision a reservation with a Virtual machine and an USRP.

        Args:
            id_res (string): vm reservation id
            rack_id (string): id of rack server to host vm
            memory (string): amount of vm memory to assign vm
            domain_str (string): domain string of vm
            cores (string): no. of vm cores to assign
            img_type (string): type of image to populate the VM
            user_id (string): Username to be deployed
            user_key (string): SSH key of the user
            email (string): User's email

        Returns:
            msg (string): Status of the request
    '''
    tcpHandler.logger.info('provision_AM called')

    create_user(tcpHandler,user_id,user_key,email)
    now = datetime.datetime.utcnow()
    now = now.replace(microsecond=0)
    now = str(now)

    #Getting VM image type from DB
    db, cur = open_db()

    #Get the image type from the database. Use the USRP id and the fact that the provisioning time must be between (now) must be between
    #start and end time of the reservation.
    db_msg = "SELECT img_type FROM uc_simulations WHERE id = '%s' and (start_time <= '%s') and ('%s' <= end_time)" % (id_res, now, now)
    tcpHandler.logger.debug("Checking for the vm image. DB Query: "+db_msg)
    db_execute(tcpHandler.logger, db, cur, db_msg)

    img_type=None
    for img_db in cur.fetchall():
        img_type = img_db[0]
    close_db(db,cur)

    if img_type == None:
         msg = id_res+',vm,not provisioned'

    #CBTM
    tcpHandler.logger.info('Creating VM, Please wait.')

    ##request_simulation_vm:"$random_id",rack_id(11 or 13),no. CPUs,MEMORY,php_simulation,tstuser,"$KEY
    CBTmCommand = 'request_simulation_vm:'+id_res+','+rack_id+','+memory+','+cores+','+domain_str+'_eu_,'+img_type+','+user_id+','+user_key
    tcpHandler.logger.info(CBTmCommand)
    msg = tcpHandler.CBTmProtocol(CBTmCommand)
    
    if 'Accepted' in msg:
        #CBTm: Here the IP address assigned to sim_id must be returned.
        CBTmCommand = 'sim_unit_status:'+id_res+','+'simulation_eu_'
        tcpHandler.logger.info(CBTmCommand)
        msg = tcpHandler.CBTmProtocol(CBTmCommand)
    else:
        tcpHandler.logger.error('VM Already Provisioned.')
        return 'Error: Already Provisioned'

    ip_vm = msg.split(':')[1]
    print "IP: ", ip_vm
    tcpHandler.logger.info('VM Provisioned.')
    ############################################################################

    #UPDATE DATABASE
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    tcpHandler.logger.info('user '+str(user_id)+' is creating a provision')
    try:
        cur.execute("""
                    UPDATE uc_simulations 
                    SET provisioned = 1 , ip_address = %s
                    WHERE user_name = %s AND (email = %s) AND (type = %s)
                    """,(str(ip_vm),str(user_id),str(email),'am'))
        db.commit()
        msg = +ip_vm+',provisioned'
        tcpHandler.logger.debug('UPDATE db successful.')
    except:
        tcpHandler.logger.error('UPDATE db failed.')
        msg = ',VM,'+'not provisioned'
        cur.close()
        db.close()
        return msg
    cur.close()
    db.close()
    print msg
    return msg

def provision_x310_PHP(tcpHandler,usrp,img_type,user_id,user_key,email,type_res):
    '''
        Provision a reservation with a Virtual machine and an USRP.

        Args:
            usrp (int): Number of the USRP
            img_type (string): type of image to populate the VM
            user_id (string): Username to be deployed
            user_key (string): SSH key of the user
            email (string): User's email
            type_res (string): Type of the reservation (AM)

        Returns:
            msg (string): Status of the request
    '''
    tcpHandler.logger.info('provision_AM called')
    create_user(tcpHandler,user_id,user_key,email)
    now = datetime.datetime.utcnow()
    now = now.replace(microsecond=0)
    now = str(now)

    #Getting VM image type from DB
    db, cur = open_db()

    #Get the image type from the database. Use the USRP id and the fact that the provisioning time must be between (now) must be between
    #start and end time of the reservation.
    db_msg = "SELECT img_type FROM uc_x310_allocations WHERE node_id = '%s' and (start_time <= '%s') and ('%s' <= end_time)" % (usrp, now, now)
    tcpHandler.logger.debug("Checking for the USRP image. DB Query: "+db_msg)
    db_execute(tcpHandler.logger, db, cur, db_msg)

    img_type=None
    for img_db in cur.fetchall():
        img_type = img_db[0]
    close_db(db,cur)

    #img_type="gnuradio"
    if img_type == None:
         msg = usrp+','+tcpHandler.resources[usrp][1]+',not provisioned'

    #CBTM
    tcpHandler.logger.info('Creating VM, Please wait.')
    CBTmCommand = 'request_unit:'+usrp+','+img_type+','+user_id+','+user_key+',x310'
    msg = tcpHandler.CBTmProtocol(CBTmCommand)
    tcpHandler.logger.info('CBTm Command: '+CBTmCommand)
    tcpHandler.logger.info('msg = '+msg)
    if 'Accepted' in msg:
        #Query the CBTM for the VM's IP
        CBTmCommand = 'sim_unit_status:'+usrp+',basic_eu_'
        msg = tcpHandler.CBTmProtocol(CBTmCommand)
        tcpHandler.logger.debug(msg)
    else:
        tcpHandler.logger.error('VM Already Provisioned.')
        return 'Error: Already Provisioned'

    ip_vm = msg.split(':')[1]
    print "IP: ", ip_vm
    tcpHandler.logger.info('VM Provisioned.')
    ############################################################################

    #UPDATE DATABASE
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    tcpHandler.logger.info('user '+str(user_id)+' is creating a provision')
    tcpHandler.logger.info('ip_address ='+ ip_vm + 'user_name: ' + user_id + ' email:'+ email + 'type_res: '+ type_res )
    try:
        cur.execute("""
                    UPDATE uc_x310_allocations
                    SET provisioned = 1 , ip_address = %s
                    WHERE user_name = %s AND (email = %s) AND (type = %s)
                    """,(str(ip_vm),str(user_id),str(email),'amphp'))

        db.commit()
        msg = usrp+','+ip_vm+',provisioned'
        tcpHandler.logger.info('UPDATE x310 db successful.')
    except:
        tcpHandler.logger.error('UPDATE x310 db failed.')
        msg = usrp+','+tcpHandler.x310_res[usrp][1]+',not provisioned'
        cur.close()
        db.close()
        return msg
    cur.close()
    db.close()
    print msg
    return msg

def provision_n210beam_PHP(tcpHandler,usrp,img_type,user_id,user_key,email,type_res):
    '''
        Provision a reservation with a Virtual machine and an USRP.

        Args:
            usrp (int): Number of the USRP
            img_type (string): type of image to populate the VM
            user_id (string): Username to be deployed
            user_key (string): SSH key of the user
            email (string): User's email
            type_res (string): Type of the reservation (AM)

        Returns:
            msg (string): Status of the request
    '''
    tcpHandler.logger.info('provision_AM called')

    create_user(tcpHandler,user_id,user_key,email)
    now = datetime.datetime.utcnow()
    now = now.replace(microsecond=0)
    now = str(now)

    #Getting VM image type from DB
    db, cur = open_db()

    #Get the image type from the database. Use the USRP id and the fact that the provisioning time must be between (now) must be between
    #start and end time of the reservation.
    db_msg = "SELECT img_type FROM uc_n210beam_allocations WHERE node_id = '%s' and (start_time <= '%s') and ('%s' <= end_time)" % (usrp, now, now)
    tcpHandler.logger.debug("Checking for the USRP image. DB Query: "+db_msg)
    db_execute(tcpHandler.logger, db, cur, db_msg)

    img_type=None
    for img_db in cur.fetchall():
        img_type = img_db[0]
    close_db(db,cur)

    #img_type="gnuradio"
    if img_type == None:
         msg = usrp+','+tcpHandler.resources[usrp][1]+',not provisioned'

    #CBTM
    tcpHandler.logger.info('Creating VM, Please wait.')
    CBTmCommand = 'request_unit:'+usrp+','+img_type+','+user_id+','+user_key+',n210beam'
    msg = tcpHandler.CBTmProtocol(CBTmCommand)
    tcpHandler.logger.info('CBTm Command: '+CBTmCommand)
    if 'Accepted' in msg:
        #Query the CBTM for the VM's IP
        CBTmCommand = 'sim_unit_status:'+usrp+',basic_eu_'
        msg = tcpHandler.CBTmProtocol(CBTmCommand)
    else:
        tcpHandler.logger.error('VM Already Provisioned.')
        return 'Error: Already Provisioned'

    ip_vm = msg.split(':')[1]
    print "IP: ", ip_vm
    tcpHandler.logger.info('VM Provisioned.')
    ############################################################################

    #UPDATE DATABASE
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    tcpHandler.logger.info('user '+str(user_id)+' is creating a provision')
    try:
        cur.execute("""
                    UPDATE uc_n210beam_allocations
                    SET provisioned = 1 , ip_address = %s
                    WHERE user_name = %s AND (email = %s) AND (type = %s)
                    """,(str(ip_vm),str(user_id),str(email),'am'))
        db.commit()
        msg = usrp+','+ip_vm+',provisioned'
        tcpHandler.logger.debug('UPDATE db successful.')
    except:
        tcpHandler.logger.error('UPDATE db failed.')
        msg = usrp+','+tcpHandler.n210_beam_res[usrp][1]+',not provisioned'
        cur.close()
        db.close()
        return msg
    cur.close()
    db.close()
    print msg
    return msg

def status_usrp(tcpHandler,usrp):
    ''' 
        Gets the status of a USRP.

        Args:
            usrp (int): USRP's number

        Returns:
            msg (string): "geni_ready" if the USRP is ready for use.
                Otherwise it must return "geni_configuring"
    '''
    tcpHandler.logger.info('status called for usrp '+str(usrp))
    ''' If ready, it must return "geni_ready", otherwise it must return "geni_configuring"'''
    '''CBTmCommand = 'unit_status:'+str(usrp)'''
    CBTmCommand = 'sim_unit_status:'+str(usrp)+',basic_eu_'
    msg = tcpHandler.CBTmProtocol(CBTmCommand)
    if 'free' in msg:
        msg = usrp+',geni_configuring,'+'0.0.0.0'
    elif 'wait' in msg:
        msg = usrp+',geni_configuring,'+'0.0.0.0'
    else:
        msg = usrp+',geni_ready,'+msg.split(':')[1]
    return msg

def describe(tcpHandler,usrp):
    ''' 
        Gets a description of the deployed VM for a given reservation and
        update the database with it.

        Args:
            usrp (int): number of the USRP designed for this reservation

        Returns:
            msg (string): Status of the request
    '''

    CBTmCommand = 'unit_status:'+str(usrp)
    msg = tcpHandler.CBTmProtocol(CBTmCommand)
    ip_address =  msg.split(':')[1]

    now = str(datetime.datetime.utcnow().replace(microsecond=0))
    tcpHandler.logger.info('described called for usrp '+str(usrp))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        cur.execute("""
            UPDATE uc_allocations
            SET ip_address=%s
            WHERE node_id = %s AND (%s BETWEEN start_time AND end_time)
            """, (str(usrp),str(ip_address),str(now),))
        db.commit()
        tcpHandler.logger.debug('Update db success.')
    except:
        tcpHandler.logger.error('Update from db failed.')
        msg = 'update failed'
        return msg
    cur.close()
    db.close()
    msg = usrp+','+str(ip_address)  # currently forcing to this static IP address
    return msg

# def describe_VM(tcpHandler,sim_id, domain_str):
#     '''
#         
#     '''
#     #CBTm: Here the IP address assigned to sim_id must be returned.
#     CBTmCommand = 'sim_unit_status:'+sim_id+','+domain_str
#     tcpHandler.logger.info(CBTmCommand)
#     msg = tcpHandler.CBTmProtocol(CBTmCommand)
#     tcpHandler.logger.info(msg)
# 
#     now = str(datetime.datetime.now().replace(microsecond=0))
#     tcpHandler.logger.info('described called for usrp '+str(usrp))
#     db = MySQLdb.connect(host=db_host,      # your host, usually localhost
#              user=db_user,           #
#               passwd=db_passwd,    # your password
#               db=db_name,)           # name of the database created by userCake
#     cur = db.cursor()
#     try:
#         cur.execute('''
#             UPDATE uc_simulations
#             SET ip_address=%s
#             WHERE (id=%s)
#             ''', (str(ip_address),str(sim_id),))
#         db.commit()
#         tcpHandler.logger.debug('Update db success.')
#     except:
#         tcpHandler.logger.error('Update from db failed.')
#         msg = 'update failed'
#         return msg
#     cur.close()
#     db.close()
# 
#     msg = sim_id+','+str(ip_address)
#     return msg

def describex310PHP(tcpHandler,usrp):
    '''
        Gets a description of the deployed VM for a given reservation.

        Args:
            usrp (int): number of the USRP designed for this reservation

        Returns:
            msg (string): Status of the request
    '''

    #Query the CBTM for the VM's IP
    CBTmCommand = 'sim_unit_status:'+usrp+',basic_eu_'
    msg = tcpHandler.CBTmProtocol(CBTmCommand)
    tcpHandler.logger.debug(msg)
    ip_address = msg.split(':')[1]

    tcpHandler.logger.info('described called for usrp '+str(usrp))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        cur.execute('''
            UPDATE uc_x310_allocations
            SET ip_address=%s
            WHERE (id=%s)
            ''', (str(ip_address),str(usrp),))
        db.commit()
        tcpHandler.logger.debug('Update db success.')
    except:
        tcpHandler.logger.error('Update from db failed.')
        msg = 'update failed'
        return msg
    cur.close()
    db.close()
    msg = usrp+','+str(ip_address)
    return msg

def describen210beamPHP(tcpHandler,usrp):
    '''
        Gets a description of the deployed n210beam usrp for a given reservation.

        Args:
            usrp (int): number of the n210beam USRP designed for this reservation

        Returns:
            msg (string): Status of the request
    '''

    #Query the CBTM for the VM's IP
    CBTmCommand = 'sim_unit_status:'+usrp+',basic_eu_'
    msg = tcpHandler.CBTmProtocol(CBTmCommand)
    tcpHandler.logger.debug(msg)
    ip_address = msg.split(':')[1]

    tcpHandler.logger.info('described called for n210 beam usrp '+str(usrp))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        cur.execute('''
            UPDATE uc_n210beam_allocations
            SET ip_address=%s
            WHERE (id=%s)
            ''', (str(ip_address),str(usrp),))
        db.commit()
        tcpHandler.logger.debug('Update db success.')
    except:
        tcpHandler.logger.error('Update from db failed.')
        msg = 'update failed'
        return msg
    cur.close()
    db.close()
    msg = usrp+','+str(ip_address)
    return msg

def renew(tcpHandler,usrp,user_id,end_time):
    ''' 
        Renew a reservation, extending the expiration time.

        Args:
            usrp (int): USRP's number
            user_id (string): Username
            end_time (datetime): New expiration time

        Returns:
            msg (string): Status of the request.
    '''
    now = datetime.datetime.utcnow()
    now = now.replace(microsecond=0)
    # Query the DataBase to see if usrp is available at expiration
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    tcpHandler.logger.debug('Checking if node_id '+str(usrp)+' is actually available at Expiration time.')
    try:
        cur.execute("""
        SELECT node_id
        FROM uc_allocations
        WHERE (node_id=%s) AND (user_name NOT LIKE %s) AND ((start_time BETWEEN %s AND %s) OR (%s BETWEEN start_time AND end_time))
        """, (usrp,user_id,now,end_time,now))
        tcpHandler.logger.debug('Query to db success.')
    except:
        tcpHandler.logger.error('Query to db failed.')
    successful = True if len(cur.fetchall()) == 0 else False
    cur.close()
    db.close()
    if successful:
        tcpHandler.resources[usrp][0] = 'False'   # is this necessary?
        msg = usrp+','+tcpHandler.resources[usrp][1]+','+'success'
    else:
        tcpHandler.resources[usrp][0] = 'True'    # is this necessary?
        msg = usrp+','+tcpHandler.resources[usrp][1]+','+'failed'

    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        cur.execute(""" 
            UPDATE uc_allocations
            SET end_time=%s
            WHERE (node_id=%s) and (start_time <%s) and (end_time >%s)
            """, (str(end_time),str(usrp),str(now),str(now)))
        db.commit()
        tcpHandler.logger.debug('Update db success.')
    except:
        tcpHandler.logger.error('Delete from db failed.')
        msg = 'delete failed'
        return msg
    cur.close()
    db.close()
    return msg

def renewVM(tcpHandler,usrp,user_id,end_time):
    ''' 
        Renew a reservation, extending the expiration time.

        Args:
            usrp (int): USRP's number
            user_id (string): Username
            end_time (datetime): New expiration time

        Returns:
            msg (string): Status of the request.
    '''
    now = datetime.datetime.utcnow()
    now = now.replace(microsecond=0)
    # Query the DataBase to see if usrp is available at expiration
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    tcpHandler.logger.debug('Checking if node_id '+str(usrp)+' is actually available at Expiration time.')
    try:
        cur.execute("""
        SELECT id
        FROM uc_simulations
        WHERE (id=%s) AND (user_name NOT LIKE %s) AND ((start_time BETWEEN %s AND %s) OR (%s BETWEEN start_time AND end_time))
        """, (usrp,user_id,now,end_time,now))
        tcpHandler.logger.debug('Query to db success.')
    except:
        tcpHandler.logger.error('Query to db failed.')
    successful = True if len(cur.fetchall()) == 0 else False
    cur.close()
    db.close()
    msg='success'

    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        cur.execute(""" 
            UPDATE uc_simulations
            SET end_time=%s
            WHERE (id=%s) and (start_time <%s) and (end_time >%s)
            """, (str(end_time),str(usrp),str(now),str(now)))
        db.commit()
        tcpHandler.logger.debug('Update db success.')
    except:
        tcpHandler.logger.error('Delete from db failed.')
        msg = 'delete failed'
        return msg
    cur.close()
    db.close()
    return msg

def renewX310(tcpHandler,usrp,user_id,end_time):
    ''' 
        Renew a reservation, extending the expiration time.

        Args:
            usrp (int): USRP's number
            user_id (string): Username
            end_time (datetime): New expiration time

        Returns:
            msg (string): Status of the request.
    '''
    now = datetime.datetime.utcnow()
    now = now.replace(microsecond=0)
    # Query the DataBase to see if usrp is available at expiration
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    tcpHandler.logger.debug('Checking if node_id '+str(usrp)+' is actually available at Expiration time.')
    try:
        cur.execute("""
        SELECT node_id
        FROM uc_x310_allocations
        WHERE (node_id=%s) AND (user_name NOT LIKE %s) AND ((start_time BETWEEN %s AND %s) OR (%s BETWEEN start_time AND end_time))
        """, (usrp,user_id,now,end_time,now))
        tcpHandler.logger.debug('Query to db success.')
    except:
        tcpHandler.logger.error('Query to db failed.')
    successful = True if len(cur.fetchall()) == 0 else False
    cur.close()
    db.close()
#    if successful:
#        tcpHandler.x310_res[usrp][0] = 'False'   # is this necessary?
#        msg = usrp+','+tcpHandler.x310_res[usrp][1]+','+'success'
#    else:
##        tcpHandler.resources[usrp][0] = 'True'    # is this necessary?
#        msg = usrp+','+tcpHandler.x310_res[usrp][1]+','+'failed'

    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        cur.execute("""
            UPDATE uc_x310_allocations
            SET end_time=%s
            WHERE (node_id=%s) and (start_time <%s) and (end_time >%s)
            """, (str(end_time),str(usrp),str(now),str(now)))
        db.commit()
        msg = 'updated success'
        tcpHandler.logger.debug('Update db success.')
    except:
        tcpHandler.logger.error('Delete from db failed.')
        msg = 'delete failed'
        return msg
    cur.close()
    db.close()
    return msg

def renewBeam(tcpHandler,usrp,user_id,end_time):
    ''' 
        Renew a reservation, extending the expiration time.

        Args:
            usrp (int): USRP's number
            user_id (string): Username
            end_time (datetime): New expiration time

        Returns:
            msg (string): Status of the request.
    '''
    now = datetime.datetime.utcnow()
    now = now.replace(microsecond=0)
    # Query the DataBase to see if usrp is available at expiration
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    tcpHandler.logger.debug('Checking if node_id '+str(usrp)+' is actually available at Expiration time.')
    try:
        cur.execute("""
        SELECT node_id
        FROM uc_n210beam_allocations
        WHERE (node_id=%s) AND (user_name NOT LIKE %s) AND ((start_time BETWEEN %s AND %s) OR (%s BETWEEN start_time AND end_time))
        """, (usrp,user_id,now,end_time,now))
        tcpHandler.logger.debug('Query to db success.')
    except:
        tcpHandler.logger.error('Query to db failed.')
    successful = True if len(cur.fetchall()) == 0 else False
    cur.close()
    db.close()
#    if successful:
#        tcpHandler.n210_bean_res[usrp][0] = 'False'   # is this necessary?
#        msg = usrp+','+tcpHandler.n210_bean_res[usrp][1]+','+'success'
#    else:
#        tcpHandler.n210_bean_[usrp][0] = 'True'    # is this necessary?
#        msg = usrp+','+tcpHandler.n210_bean_res[usrp][1]+','+'failed'

    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        cur.execute(""" 
            UPDATE uc_n210beam_allocations
            SET end_time=%s
            WHERE (node_id=%s) and (start_time <%s) and (end_time >%s)
            """, (str(end_time),str(usrp),str(now),str(now)))
        db.commit()
        msg = 'updated success'
        tcpHandler.logger.debug('Update db success.')
    except:
        tcpHandler.logger.error('Delete from db failed.')
        msg = 'update failed'
        return msg
    cur.close()
    db.close()
    return msg

def PerformOperationalAction(tcpHandler,usrp,action):
    ''' 
        Perform an operation as described by Standardisation spec of the
        Federation Aggregate Manager API for testbeds.

        Args:
            usrp (int): USRP's (Resource) number
            action (string): Action to be performed ("geni_start", "geni_stop"
                or "geni_restart")

        Returns:
            msg (string): Status of the request
    '''

    if action == 'geni_start':
        CBTmCommand = 'start_unit:'+str(usrp)
    elif action == 'geni_stop':
        CBTmCommand = 'shutdown_unit:'+str(usrp)
    elif action == 'geni_restart':
        CBTmCommand = 'restart_unit:'+str(usrp)

    msg = tcpHandler.CBTmProtocol(CBTmCommand)

    return msg

def PerformVMOperationalAction(tcpHandler,vm_name,res_id,action):
    ''' 
        Perform an operation as described by Standardisation spec of the
        Federation Aggregate Manager API for testbeds.

        Args:
            vm_name (String): vm domain name
            action (string): Action to be performed ("geni_start", "geni_stop"
                or "geni_restart")

        Returns:
            msg (string): Status of the request
    '''
    if action == 'geni_start':
        CBTmCommand = 'start_sim_unit:'+str(res_id+","+vm_name)
    elif action == 'geni_stop':
        CBTmCommand = 'stop_sim_unit:'+str(res_id+","+vm_name)
    elif action == 'geni_restart':
        CBTmCommand = 'restart_sim_unit:'+str(res_id+","+vm_name)

    msg = tcpHandler.CBTmProtocol(CBTmCommand)

    return msg

def release(tcpHandler,usrp):
    ''' 
        Release allocated resources (VM and USRP)

        Args:
            usrp (int): USRP's (Resource) number

        Returns:
            msg (string): Status of the request
    '''
    msg = 'release,'+usrp
    tcpHandler.logger.info('release called for usrp '+str(usrp))
    now = datetime.datetime.utcnow()
    now = now.replace(microsecond=0)
    tcpHandler.logger.debug('Modify the info (changing the end_time) on the DB on '+str(usrp))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
         user=db_user,           #
          passwd=db_passwd,    # your password
          db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        cur.execute(""" 
        UPDATE uc_allocations
        SET end_time=%s
        WHERE (node_id=%s) and (start_time <%s) and (end_time >%s)
        """, (str(now),str(usrp),str(now),str(now)))
        db.commit()
        tcpHandler.logger.debug('Update db success.')
        msg = 'success'
    except:
        tcpHandler.logger.error('Update db failed.')
        return msg
    cur.close()
    db.close()
    return msg

def delete_x310_allocation(tcpHandler,usrp,user_id):
    '''
        Delete a reservation and remove a zabbix host.

        Args:
            usrp (int): usrp id 
            user_id (string): Name of the user

        Returns:
            msg (string): Status of the request.
    '''
    msg = 'release,'+usrp
    tcpHandler.logger.info('release called for usrp '+str(usrp))
    now = datetime.datetime.utcnow()
    now = now.replace(microsecond=0)
    tcpHandler.logger.debug('Modify the info (changing the end_time) on the DB on '+str(usrp))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
         user=db_user,           #
          passwd=db_passwd,    # your password
          db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        cur.execute(""" 
        UPDATE uc_x310_allocations
        SET end_time=%s
        WHERE (node_id=%s) and (start_time <%s) and (end_time >%s)
        """, (str(now),str(usrp),str(now),str(now)))
        db.commit()
        tcpHandler.logger.info('Update db success.')
        msg = 'success'
    except:
        tcpHandler.logger.error('Update db failed.')
        return msg
    cur.close()
    db.close()
    return msg

def delete_simulation(tcpHandler,sim_id,domain_str,user_id):
    '''
        Delete a simulation reservation.

        Args:
            sim_id (int): Simulation number
            domain_str (string): virt domain name
            rack_id (string): id of hos rack server
            user_id (string): Name of the user

        Returns:
            msg (string): Status of the request.
    '''
    now = str(datetime.datetime.utcnow())
    tcpHandler.logger.info('user '+str(user_id)+' is deleting vm allocation '+domain_str+str(sim_id))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
             user=db_user,           #
              passwd=db_passwd,    # your password
              db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    tcpHandler.logger.info('User '+str(user_id)+' is deleting allocation '+str(sim_id))
    try:
        selectStatment = "SELECT * FROM uc_simulations WHERE (id = %s)"
        cur.execute(selectStatment, (str(sim_id),))
        columns = [column[0] for column in cur.description]
        results = []
        for row in cur.fetchall():
            results.append(dict(zip(columns, row)))     # results contains all the details about the reservation
    except:
        tcpHandler.logger.error('Query to db failed.')
        results = []
        msg = 'Query failed'
        return msg
    cur.close()
    cur = db.cursor()
    try:
        cur.execute("""
            UPDATE uc_simulations
            SET end_time=%s
            WHERE (id=%s)
            """, (str(now),str(sim_id)))
        db.commit()
        tcpHandler.logger.debug('Update from db success.')
    except:
        tcpHandler.logger.error('Update from db failed.')
        msg = 'update failed'
        return msg
    cur.close()
    db.close()

    ##delete vm on CBTM
    ##vm_id,rack_id,domain_str
    CBTmCommand = 'release_sim_unit:'+sim_id+','+domain_str
    msg = tcpHandler.CBTmProtocol(CBTmCommand)
        
    msg = 'delete success'
    
    return msg

def delete_n210beam_allocation(tcpHandler,usrp,user_id):
    '''
        Delete a reservation and remove a zabbix host.

        Args:
            usrp (int): usrp id 
            user_id (string): Name of the user

        Returns:
            msg (string): Status of the request.
    '''
    msg = 'release,'+usrp
    tcpHandler.logger.info('release called for usrp '+str(usrp))
    now = datetime.datetime.utcnow()
    now = now.replace(microsecond=0)
    tcpHandler.logger.debug('Modify the info (changing the end_time) on the DB on '+str(usrp))
    db = MySQLdb.connect(host=db_host,      # your host, usually localhost
         user=db_user,           #
          passwd=db_passwd,    # your password
          db=db_name,)           # name of the database created by userCake
    cur = db.cursor()
    try:
        cur.execute(""" 
        UPDATE uc_n210beam_allocations
        SET end_time=%s
        WHERE (node_id=%s) and (start_time <%s) and (end_time >%s)
        """, (str(now),str(usrp),str(now),str(now)))
        db.commit()
        tcpHandler.logger.debug('Update db success.')
        msg = 'success'
    except:
        tcpHandler.logger.error('Update db failed.')
        return msg
    cur.close()
    db.close()
    return msg

def check_availability(tcpHandler, usrp, start_time, exp_time):
    """ 
    Checks on the database the availability of a USRP between a certain start time and expiration time
    """
    db, cur = open_db()
    db_msg = "SELECT * FROM uc_allocations"
    db_execute(tcpHandler.logger, db, cur, db_msg)
    for result in cur.fetchall():
        print result
    close_db(db,cur)

def check_x310_availability(tcpHandler, usrp, start_time, exp_time):
    """ 
    Checks on the database the availability of a x310 USRP between a certain start time and expiration time
    """
    db, cur = open_db()
    db_msg = "SELECT * FROM uc_x310_allocations"
    db_execute(tcpHandler.logger, db, cur, db_msg)
    for result in cur.fetchall():
        print result
    close_db(db,cur)

def check_n210beam_availability(tcpHandler, usrp, start_time, exp_time):
    """ 
    Checks on the database the availability of a n210beam USRP between a certain start time and expiration time
    """
    db, cur = open_db()
    db_msg = "SELECT * FROM uc_n210beam_allocations"
    db_execute(tcpHandler.logger, db, cur, db_msg)
    for result in cur.fetchall():
        print result
    close_db(db,cur)

def any_available(tcpHandler, start_time, end_time):
    ''' 
        Release allocated resources (VM and USRP)

        Args:
            usrp (int): USRP's (Resource) number

        Returns:
            msg (string): Status of the request
    '''
    db, cur = open_db()

    #If the end_time of the reservation is later than the start of the observed interval, the reservation has not finished by the start of 
    #the interval.
    #If the start_time of the reservation is after the end observed interval, the reservation has not begun.
    db_msg = "SELECT node_id FROM uc_allocations WHERE (start_time <'%s') and (end_time >'%s')" % (end_time, start_time)
    tcpHandler.logger.debug("Checking for any USRP available. DB Query: "+db_msg)
    db_execute(tcpHandler.logger, db, cur, db_msg)

    resources = {"13":True,"14":True,"21":True,"22":True,"23":True,"24":True,"31":True,"32":True,"33":True,"34":True,"41":True,"42":True,"51":True,"52":True,"53":True,"54":True,"63":True,"64":True}
    for busy_usrp in cur.fetchall():
        print "Busy: ", busy_usrp
        resources[busy_usrp[0]] = False
    close_db(db,cur)
    print "Resources: ", resources
    msg=""
    for k in resources.keys():
        msg += k+','+str(resources[k])+'\n'
    msg=msg[:-1] #Remove the last \n
    tcpHandler.logger.debug(msg)
    return msg

def any_n210beam_available(tcpHandler, start_time, end_time):
    ''' 
        Release allocated n210 beam resources (VM and USRP)

        Args:
            usrp (int): n210beam USRP's (Resource) number

        Returns:
            msg (string): Status of the request
    '''
    db, cur = open_db()

    #If the end_time of the reservation is later than the start of the observed interval, the reservation has not finished by the start of 
    #the interval.
    #If the start_time of the reservation is after the end observed interval, the reservation has not begun.
    db_msg = "SELECT node_id FROM uc_n210beam_allocations WHERE (start_time <'%s') and (end_time >'%s')" % (end_time, start_time)
    tcpHandler.logger.debug("Checking for any USRP available. DB Query: "+db_msg)
    db_execute(tcpHandler.logger, db, cur, db_msg)

    resources = {"12":True,"43":True}
    for busy_usrp in cur.fetchall():
        print "Busy: ", busy_usrp
        resources[busy_usrp[0]] = False
    close_db(db,cur)
    print "Resources: ", resources
    msg=""
    for k in resources.keys():
        msg += k+','+str(resources[k])+'\n'
    msg=msg[:-1] #Remove the last \n
    tcpHandler.logger.debug(msg)
    return msg

def any_x310_available(tcpHandler, start_time, end_time):
    ''' 
        Release x310 allocated resources (VM and USRP)

        Args:
            usrp (int): x310 USRP's (Resource) number

        Returns:
            msg (string): Status of the request
    '''
    db, cur = open_db()

    #If the end_time of the reservation is later than the start of the observed interval, the reservation has not finished by the start of 
    #the interval.
    #If the start_time of the reservation is after the end observed interval, the reservation has not begun.
    db_msg = "SELECT node_id FROM uc_x310_allocations WHERE (start_time <'%s') and (end_time >'%s')" % (end_time, start_time)
    tcpHandler.logger.debug("Checking for any USRP available. DB Query: "+db_msg)
    db_execute(tcpHandler.logger, db, cur, db_msg)

    res = {"11":True,"44":True}
    for busy_usrp in cur.fetchall():
        print "Busy: ", busy_usrp
        res[busy_usrp[0]] = False
    close_db(db,cur)
    print "Resources: ", res
    msg=""
    for k in res.keys():
        msg += k+','+str(res[k])+'\n'
    msg=msg[:-1] #Remove the last \n
    tcpHandler.logger.debug(msg)
    return msg



