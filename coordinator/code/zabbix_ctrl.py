#!/usr/bin/env python
'''
    This module contains auxiliar functions for management of users and hosts in
    the Zabbix monitoring system
'''

import logging
from pyzabbix import ZabbixAPI

from user_resources import *

def create_zabbix_user(tcpHandler, user_id, user_email):
    '''
        Creates a user in the Zabbix system

        Args:
            user_id (string): Username
            user_email (string): User's email
    '''
    # zapi = ZabbixAPI(url='http://192.168.0.12/zabbix/', user='zabbix_user['user'], password=zabbix_user['passwd'])
    # retrive active users in zabbix
    users = tcpHandler.zapi.user.get(output='extend')
    if any (user_id == user['name'] for user in users):
        tcpHandler.logger.debug('user '+str(user_id)+' already exists in zabbix server')
    else:
        try:
            userid = tcpHandler.zapi.user.create({
                'alias': user_id,
                'passwd': 'userpass',
                'usrgrps': [
                    {
                        'usrgrpid': '8'
                    }
                ],
                'user_medias': [
                    {
                        'mediatypeid': '1',
                        'sendto': user_email,
                        'active': 0,
                        'severity': 63,
                        'period': '1-7,00:00-24:00'
                    }
                ]
            })
            tcpHandler.logger.info('create user '+str(user_id)+' in zabbix server')
        except:
            tcpHandler.logger.error('Error while creating user '+str(user_id)+' in zabbix server')
        tcpHandler.zabbix_users[user_id] = userid['userids'][0]

def delete_zabbix_user(tcpHandler,user_id):
    '''
        Delete a user in the Zabbix system

        Args:
            user_id (string): Username
    '''
    # zapi = ZabbixAPI(url='http://192.168.0.12/zabbix/', user='zabbix_user['user'], password=zabbix_user['passwd'])
    try:
        if '2.0' in tcpHandler.zapi.api_version():
            tcpHandler.zapi.user.delete(userid = tcpHandler.zabbix_users[user_id])
        else:
            tcpHandler.zapi.user.delete(tcpHandler.zabbix_users[user_id])
        del tcpHandler.zabbix_users[user_id]    # delete zabbix user from the list of currently active users
        tcpHandler.logger.info('Successfully deleted from Zabbix server user '+user_id)
    except:
        tcpHandler.logger.error('Error in deleting user '+user_id+' from Zabbix Server.')

def delete_zabbix_host(tcpHandler,host_name):
    '''
        Delete a host in the Zabbix system

        Args:
            host_name (string): Host name
    '''
    try:
        if '2.0' in tcpHandler.zapi.api_version():
            tcpHandler.zapi.host.delete(hostid = tcpHandler.zabbix_hosts[host_name])
        else:
            tcpHandler.zapi.host.delete(tcpHandler.zabbix_hosts[host_name])
        del tcpHandler.zabbix_hosts[host_name]
        tcpHandler.logger.info('Successfully deleted host '+host_name+' from Zabbix Server')
    except:
        tcpHandler.logger.error('Error in deleting host '+host_name+' from Zabbix Server')

def create_zabbix_host(tcpHandler,host_name,ip_address):
    '''
        Creates a Host in the Zabbix system

        Args:
            host_name (string): Name for the host
            ip_address (string): Host's IP Address
    '''
    hostid = tcpHandler.zapi.host.create({'host': host_name,
        'interfaces': [
            {
                'type': 1,
                'main': 1,
                'useip': 1,
                'ip': ip_address,
                'dns': '',
                'port': '10050'
            }
        ],
        'groups': [
            {
                'groupid': '5'
            }
            ],
        'templates': [
            {
                'templateid': '10001'
            }
            ]
        })
    tcpHandler.zabbix_hosts[host_name] = hostid['hostids'][0]
    return 1
