#!/usr/bin/env python
'''
    This is the Cleaner module. It creates Daemons that query the Coordinator to
    send Reminders about Reservations, Delete expired ones and collect logs
    about them
'''

import datetime
import os
import threading
import time
import socket
import sys

def coordinator_query(host,port,message):
    ''' 
        Communicates with the coordinator to execute the defined Methods.

        Args:
            host (string): IP of Coordinator.
            port (int): Port where the Coordinator is listening.
            message (string): Message to be transmitted to Coordinator.

        Returns:
            received (string): Result from Coordinator query.
    '''
    # Create a socket (SOCK_STREAM means a TCP socket)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        # Connect to server and send data
        sock.connect((host, port))
        sock.sendall(message + "\n")
        # Receive data from the server and shut down
        received = sock.recv(1024)
    finally:
        sock.close()

    return received

def collect_log():
    '''
        Save's dailly logs to the coordinator-logs folder
    '''

    today = str(datetime.datetime.utcnow()).split(' ')[0]
    # copy the coordinator log file
    os.popen("cp ../log/log-coordinator.log ../log/coordinator-logs/"+today)
    # create an empty coordinator log file
    log = open('../log/log-coordinator.log','w')
    log.close()
    # copy the AM log file
    os.popen("cp ../log/am.log ../log/am-logs/"+today)
    # create an empty coordinator log file
    log = open('../log/am.log','w')
    log.close()
    # Do this operation every X seconds
    t = threading.Timer(86400,collect_log) # Save a log every day (86400sec)
    t.daemon = True
    t.start()

def delete_expired_allocations():
    '''
        Query to delete expired allocations from the Database, and release allocated
        resources that expired.
    '''

    host,port = 'localhost',9999
    # command to the coordinator to remove expired allocations
    msg = coordinator_query(host,port,'cleaner::remove_expired')
    msg = coordinator_query(host,port,'cleaner::make_clean')
    # do this operation every X secods (I reccomend every 5/10 seconds)
    t = threading.Timer(5,delete_expired_allocations) # Check every X seconds, I suggest every 10 seconds
    t.daemon = True
    t.start()

def send_reminder():
    '''
        Query to send reminders about reservations and simulations.
    '''
    #print 'check upcoming allocation'
    host,port = 'localhost',9999
    # command to the coordinator to send email reminders
    msg = coordinator_query(host,port,'cleaner::send_reminder')
    #print msg
    t = threading.Timer(3600,send_reminder) # Check every hour (86400sec)
    t.daemon = True
    t.start()

def main():
    '''
        Opens a Deamon for each of the three methods:

        * collect_log()
        * delete_expired_allocations()
        * send_reminder()
    '''
    collect_log()
    delete_expired_allocations()
    send_reminder()

    while True:
        time.sleep(5)

if __name__ == '__main__':
    sys.exit(main())
