# README #

This repository contains the Aggregate Manager, the PHP and the Coordinator code that enables access to the testbed. The Aggregate Manager is the process responsible for enabling external access, by communicating with jFed. The PHP code is responsible for internal user management. The coordinator is the entity that coordinates the requests from both these entities and communicates with the CBTM on behalf of the users.  

## Dependencies ##

To install the Aggregate Manager, first install the following dependencies:

```
#!shell
sudo apt-get install python-m2crypto python-dateutil python-openssl libxmlsec1 xmlsec1 libxmlsec1-openssl libxmlsec1-dev git python-pandas python-mysqldb python-pip
```

You will also need to install Zabbix, a network monitoring server. Zabbix will depend on MySql, PHP and Apache.

### Installing MySql ###
```
#!shell
sudo apt-get update
sudo apt-get install mysql-server
```
Enter root user password, and confirm

```
#!shell
sudo mysql_secure_installation
```

### Testing MySQL ###
```
#!shell
systemctl status mysql.service
```

### MYSQL Create Database ###
```
#!mysql
mysql -u root -p
Enter password:

CREATE DATABASE USERS;
```


### MYSQL Load Database ###
in the command line run the following command

```
#!shell
mysql -u root -p USERS < coordinator_database.sql
```

Test is the database was populated by logging in and selecting tables

```
#!mysql
mysql -u root -p
Enter password:

use USERS;
show tables;
```
## Installing Python Pip ##
sudo apt install python-pip

### Installing PHP ###

First of all, you can list all of the available PHP 7.0-related packages for review:

apt-cache pkgnames | grep php7.0

Then you can install the package your to-be-deployed application requires.

For example, if you want to deploy your application based on the LAMP stack, usually, you can install the below packages after installing Apache:

sudo apt-get install -y php7.0 libapache2-mod-php7.0 php7.0-cli php7.0-common php7.0-mbstring php7.0-gd php7.0-intl php7.0-xml php7.0-mysql php7.0-mcrypt php7.0-zip

After the installation, you can confirm that with:

php -v

The output should resemble:
agg-manager:~$ php -v
PHP 7.0.18-0ubuntu0.16.04.1 (cli) ( NTS )
Copyright (c) 1997-2017 The PHP Group
Zend Engine v3.0.0, Copyright (c) 1998-2017 Zend Technologies
    with Zend OPcache v7.0.18-0ubuntu0.16.04.1, Copyright (c) 1999-2017, by Zend Technologies


### Installing Apache ###

sudo apt install apache2


### Installing Zabbix ###

The following instructions were tested successfully to install Zabbix 2.0 on Ubuntu 12.04.

```
#!shell
wget http://repo.zabbix.com/zabbix/2.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_2.0-1precise_all.deb
sudo dpkg -i zabbix-release_2.0-1precise_all.deb
sudo apt-get update
sudo apt-get install zabbix-server-mysql zabbix-frontend-php
```

After installing Zabbix, you will need to go to http://127.0.0.1/zabbix and complete the installation process.
Sometimes the PHP timezone is not set. You will need to go to edit the 'php.ini' file in '/etc/php5/apache2' to include the following line 'date.timezone = Europe/Dublin'
After this, restart apache by doing:

```
#!shell
sudo service apache2 restart
```

You should now be able to login into Zabbix.

Finally, you will need to install the Zabbix API python wrapper:

```
#!shell
pip install py-zabbix
```

After installing the python bindings, you can test if they are working properly by doing:

```
#!python
from pyzabbix import ZabbixAPI
zapi = ZabbixAPI(url='http://127.0.0.1/zabbix/', user='your_user', password='your_pass')
```

## Setting up the Coordinator ##

The coordinator is the central entity of this system. It is a TCD server
that processes all the requests from the AM and the webserver and it is in charge to 1) create/delete/update users on the gateway 2) create/delete reservations for USRPs (TCD+Fed4FIRE) and Simulations (TCD only) 3) Add hosts and users to the Zabbix monitoring system.

To get the source for the coordinator do

```
#!shell

git clone https://alvas_man@bitbucket.org/tcd_testbed/coordinator.git
```

After cloning the source code, copy the CBTM certificate to the resources folder and move the code to the '/opt/' folder

```
#!shell

scp @:~/cbtm/resources/cbtm_key.pem ./resources/
scp @:~/cbtm/resources/cbtm_cert.pem ./resources/
```


Finally, start the Coordinator as root in a tmux session.

```
#!shell

cd /opt/coord
tmux new -s coord
sudo python coordinator.py
```

Now you can also start the cleaner:

```
#!shell
cd /opt/
python cleaner.py
```
