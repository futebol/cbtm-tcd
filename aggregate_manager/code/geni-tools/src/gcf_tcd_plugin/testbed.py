# -*- coding: utf-8 -*-
#----------------------------------------------------------------------
# Copyright (c) 2015 Inria by David Margery
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and/or hardware specification (the "Work") to
# deal in the Work without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Work, and to permit persons to whom the Work
# is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Work.
#
# THE WORK IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE WORK OR THE USE OR OTHER DEALINGS
# IN THE WORK.
#----------------------------------------------------------------------
'''
    CONNECT Aggregate Manager Delegate

    This is an extension of the gcf-am and implements it's methods for better
    integration with the CBTM and Databases.

    For more information about the methods implemented here, it's arguments
    and returns, see:
        * https://fed4fire-testbeds.ilabt.iminds.be/asciidoc/federation-am-api.html
        * https://fed4fire-testbeds.ilabt.iminds.be/asciidoc/general.html
        * http://groups.geni.net/geni/wiki/GAPI_AM_API_V3/CommonConcepts

    TODO:
        All time operations must follow the RFC3339 standart.
'''

from ConfigParser import SafeConfigParser
import logging

import gcf.geni.am.am3 as am3
from gcf.sfa.trust.certificate import Certificate
from gcf.geni.util import rspec_util

from .irisvm import IrisVM #TCD

import operator
import os
import socket
import sys
import xml.etree.ElementTree as ET
import xml.etree as etree
import datetime
from datetime import timedelta

import base64
import zlib
import traceback

import time, threading
import dateutil
import collections
import random

from collections import defaultdict

g_rspec_ns={"rspecV3":"http://www.geni.net/resources/rspec/3"}

host,port = 'localhost',9999

def collect_log():
    '''
        Collects the logs and move them to a centralized folder.
    '''
    os.popen('cp /opt/f4fam/currentlog.log /opt/f4fam/logs/'+''.join(list(time.ctime().split(' ')[i] for i in [0,1,2,4] )))
    file = open('/opt/f4fam/currentlog.log','w')
    file.close()
    threading.Timer(86400,collect_log).start()

def GatewayQuery(host,port,message):
    '''
        Method to communicate with the Coordinator.

        Args:
            host (string): IP address of the Coordinator.
            port (int): Port to access the Coordinator
            message (string): request to be interpreted by the Coordinator

        Returns:
            received (string): Information from the Coordinator about the request
    '''
    # Create a socket (SOCK_STREAM means a TCP socket)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        # Connect to server and send data
        sock.connect((host, port))
        sock.sendall(message + "\n")
        # Receive data from the server and shut down
        received = sock.recv(1024)
    finally:
        sock.close()

    return received

def str_to_bool(s):
    '''
        Convert a string into boolean

        Args:
            s (string): String to be converted

        Returns:
            (Boolean): True or False
    '''
    if s == 'True':
         return True
    elif s == 'False':
         return False
    else:
         raise ValueError # evil ValueError that doesn't tell you what the wrong value was


class Slice(am3.Slice):
    """
        Override the Slice class on am3.py
    """

    def __init__(self,urn_str):
        super(Slice,self).__init__(urn_str)

class MyTestbedDelegate(am3.ReferenceAggregateManager):
    '''
        This class implements our Aggregate Manager Delegate based on the
        ReferenceAggregateManager from am3.py.
    '''
    CONFIG_LOCATIONS=["/etc/geni-tools-delegate/testbed.ini",
                     "testbed.ini"]

    def __init__(self, root_cert, urn_authority, url, **kwargs):
        self.lock = threading.Lock()
        super(MyTestbedDelegate,self).__init__(root_cert,urn_authority,url,**kwargs)
        self.logger = logging.getLogger('geni-delegate')
        self.handler = logging.FileHandler('/opt/f4fam/currentlog.log')
        self.handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.handler.setFormatter(formatter)
        self.logger.addHandler(self.handler)
        
        self.aggregate_manager_id=self.getAggregateManagerId(kwargs['certfile'])
        self._my_urn=self.aggregate_manager_id
        print 'urn_authority'+urn_authority
        urn_components=self.aggregate_manager_id.split('+')
        self.urn_authority_prefix="%s+%s"%(urn_components[0],urn_components[1])
        self.parser=SafeConfigParser()
        
        self.logger.info("Querying the coordinator for the list of resources.")
        # initialise and update the resource list from the coordinator / CBTMM
        self.updateResourceList()

        found_configs=self.parser.read(self.CONFIG_LOCATIONS)
        if len(found_configs) < 1:
            self.logger.warn('Did not find testbed configuration from %s' % self.CONFIG_LOCATIONS)
        else:
            self.logger.info("Read configuration from the following sources: %s" % found_configs)
        self.logger.info("Starting testbed aggregate manager delegate for urn %s at url %s"% (urn_authority,url))

        collect_log()

    def GetVersion(self, options):
        '''
            Returns:
                (rspec): Aggregate Manager Version Rspec
        '''
        self.logger.debug('Called GetVersion in testbed.py')
        return super(MyTestbedDelegate, self).GetVersion(options)
    
    
    def updateResourceList(self):
        ########################################################################
        ######### GATHER INFO ABOUT THE RESOURCES -> COORDINATOR QUERY #########
        global novms
        global next_vm_id
                
        n210msg = GatewayQuery(host,port,'AM::listresource')
        self.logger.debug("N210 Received: "+n210msg)
        n210resourcelist = [x.split(',') for x in n210msg.split('\n')]
        n210resourcelist.remove([''])
        
        x310msg = GatewayQuery(host,port,'AM::list_x310_resource')
        self.logger.debug("x310 Received: "+x310msg)
        x310resourcelist = [x.split(',') for x in x310msg.split('\n')]
        x310resourcelist.remove([''])

        msgbeam = GatewayQuery(host,port,'AM::list_n210beam_resource')
        self.logger.debug("n210 Beam Received: "+msgbeam)
        beamresourcelist = [x.split(',') for x in msgbeam.split('\n')]
        beamresourcelist.remove([''])
        
        [novms, next_vm_id] = GatewayQuery(host,port,'AM::list_vms_resource').split(',')
        self.logger.info("VM list received. equip_type vm-2cpu-m4g ------ movms" + str(novms) + " next_vm_id" + str(next_vm_id))

        listresource = n210resourcelist
        listresource.extend(x310resourcelist)
        listresource.extend(beamresourcelist)

        res = []
       
        listresource.sort()
        
        if len(self._agg.resources) == 0:
            for ( id, available ) in listresource:
                if (int(id) == 11 or int(id) == 44):
                    res.append( IrisVM( self._agg, "x310" , int(id), str_to_bool(available),"x310"))
                elif (int(id) == 12 or int(id) == 43):
                    res.append( IrisVM( self._agg, "n210beam" , int(id), str_to_bool(available),"n210beam"))
                else:
                    res.append( IrisVM( self._agg, "usrp-vm" , int(id), str_to_bool(available),"n210"))
                
            self.logger.info("vm-2cpu-m4g count" + novms)
            if int(novms) > 0:
                res.append( IrisVM( self._agg, "vm-2cpu-m4g" , int(1), str_to_bool("True"),"vm-2cpu-m4g"))
            else:
                res.append( IrisVM( self._agg, "vm-2cpu-m4g" , int(1), str_to_bool("False"),"vm-2cpu-m4g"))
                 
            self._agg.add_resources(res)
        
        self._agg.resources.sort(key = operator.attrgetter('id'))
        listresource.sort()
        
        for i in range(len(listresource)):
            for k in range(len(self._agg.resources)):
                if self._agg.resources[k]._usrp_id == int(listresource[i][0]):
                    self.logger.debug("agg-res " + str(self._agg.resources[k]._usrp_id) + " listresource[i][0] " + str(listresource[i][0]) + " listresource[i][1] " + str(listresource[i][1]))
                    self._agg.resources[k].available = str_to_bool(listresource[i][1])
        
        self._agg.resources.sort(key = operator.attrgetter('id'))

        for items in self._agg.resources:
            self.logger.info( "Resource equip info: "+ str(items._usrp_id) + " equip_info " + str(items.available) + " hardware type:"+items._hardware_type)
       
        
    def ListResources(self, credentials, options):
        '''
            Query the Coordinator about the availability of resources (CBTm).

            Note:
                The Aggregate manager already keeps track of the resources.

            Args:

            Returns:
                (rspec): rspec containing information about request
        '''
        self.logger.info('ListResources(%r)' % (options))
        self.expire_slivers()

        #Basic sanity checking
        if 'geni_rspec_version' not in options:
            # This is a required option, so error out with bad arguments.
            self.logger.error('No geni_rspec_version supplied to ListResources.')
            return self.errorResult(AM_API.BAD_ARGS,
                                    'Bad Arguments: option geni_rspec_version was not supplied.')
        if 'type' not in options['geni_rspec_version']:
            self.logger.error('ListResources: geni_rspec_version does not contain a type field.')
            return self.errorResult(AM_API.BAD_ARGS,
                                    'Bad Arguments: option geni_rspec_version does not have a type field.')
        if 'version' not in options['geni_rspec_version']:
            self.logger.error('ListResources: geni_rspec_version does not contain a version field.')
            return self.errorResult(AM_API.BAD_ARGS,
                                    'Bad Arguments: option geni_rspec_version does not have a version field.')

        # Look to see what RSpec version the client requested
        # Error-check that the input value is supported.
        rspec_type = options['geni_rspec_version']['type']
        if isinstance(rspec_type, str):
            rspec_type = rspec_type.lower().strip()
        rspec_version = options['geni_rspec_version']['version']
        if rspec_type != 'geni':
            self.logger.error('ListResources: Unknown RSpec type %s requested', rspec_type)
            return self.errorResult(AM_API.BAD_VERSION,
                                    'Bad Version: requested RSpec type %s is not a valid option.' % (rspec_type))
        if rspec_version != '3':
            self.logger.error('ListResources: Unknown RSpec version %s requested', rspec_version)
            return self.errorResult(AM_API.BAD_VERSION,
                                    'Bad Version: requested RSpec version %s is not a valid option.' % (rspec_version))
        self.logger.info("ListResources requested RSpec %s (%s)", rspec_type, rspec_version)

        if 'geni_slice_urn' in options:
            self.logger.error('ListResources: geni_slice_urn is no longer a supported option.')
            msg = 'Bad Arguments:'
            msg += 'option geni_slice_urn is no longer a supported option.'
            msg += ' Use "Describe" instead.'
            return self.errorResult(AM_API.BAD_ARGS, msg)

        #Check user's credentials
        privileges = ()
        self.getVerifiedCredentials(None,
                                    credentials,
                                    options,
                                    privileges)

        if credentials == []:
            return self.errorResult(am3.AM_API.BAD_ARGS,"Bad Arguments: No valid credentials provided")

        # update the resource list from the coordinator / cbtm         
        self.updateResourceList()

        all_resources = self._agg.catalog(None)
        available = 'geni_available' in options and options['geni_available']
        resource_xml = ""
        for r in all_resources:
            if available and not r.available:
                continue
            if r.type == "vm-2cpu-m4g":
                resource_xml = resource_xml + self.advert_vms(r)
            else:
                resource_xml = resource_xml + self.advert_resource(r)
        result = self.advert_header() + resource_xml + self.advert_footer()
        self.logger.debug ("---------------------print the result:--------------------- " + result)
        self.logger.debug ("------------------------------------------finish print the result -----------------------------")
        # Optionally compress the result
        if 'geni_compressed' in options and options['geni_compressed']:
            try:
                result = base64.b64encode(zlib.compress(result))
            except Exception, exc:
                self.logger.error("Error compressing and encoding resource list: %s", traceback.format_exc())
                raise Exception("Server error compressing resource list", exc)
        return self.successResult(result)

        # RETURN LIST KEPT BY THE AGGREGATE MANAGER IN RSPEC FORMAT.
        #return super(MyTestbedDelegate, self).ListResources(credentials, options)

    def Allocate(self, slice_urn, credentials, rspec, options):
        '''
            TODO:
                need to rewrite this function to first allocate a resource in
                the Database, then, if successfull, in the Aggregate manager

            Returns:
                (rspec): Rspec with information about the allocation
        '''

        privileges = (am3.ALLOCATE_PRIV,)
        creds=self.getVerifiedCredentials(slice_urn,
                                          credentials,
                                          options,
                                          privileges)

        # Grab the user_urn
        user_urn = am3.gid.GID(string=options['geni_true_caller_cert']).get_urn()
        user, email = self.get_user_data_from_creds(creds) #TODO Add multiple users to the nodes.
        self.logger.debug("Got user: "+user+" E-mail: "+email)

        self.updateResourceList()
        
        self.logger.debug("Updated resources.")

        # determine the start time as bounded by slice expiration and 'now'
        now = datetime.datetime.utcnow()
        start_time = now
        if 'geni_start_time' in options:
            # Need to parse this into datetime
            start_time_raw = options['geni_start_time']
            start_time = self._naiveUTC(dateutil.parser.parse(start_time_raw))
        start_time = max(now, start_time)
        if (start_time > self.min_expire(creds)):
            return self.errorResult(am3.AM_API.BAD_ARGS,
                                    "Can't request start time on sliver after slice expiration")

        #start_time=str(datetime.datetime.now().replace(microsecond=0))
        # determine max expiration time from credentials
        # do not create a sliver that will outlive the slice!
        expiration = self.min_expire(creds, self.max_alloc,
                                     ('geni_end_time' in options
                                      and options['geni_end_time']))

        # determine end time as min of the slice
        # and the requested time (if any)
        end_time = self.min_expire(creds,
                                   requested=('geni_end_time' in options
                                              and options['geni_end_time']))


        #exp_time=self.get_experiment_expiration(options)
        self.logger.debug("Start Time: "+str(start_time)+" End Time: "+str(end_time))

        #Get requested nodes from RSpec
        try:
            root = ET.fromstring(rspec)
        except Exception, e:
            self.logger.warn("Failed to parse XML doc: %s", e)
            return False

        #TODO Extra sanity check on the RSpec

        #Parse the RSpec for the requested nodes
        usrp_list=[]
        nodes = root.findall('rspecV3:node',g_rspec_ns)
        if nodes == None:
            return self.errorResult(am3.AM_API.BAD_ARGS,"No node requested in the RSpec.")

        #Check the availability of the nodes between start_time and exp_time
        usrps_status = self.get_any_available_node(str(start_time),str(end_time))
        x310_status = self.get_any_x310_node(str(start_time),str(end_time))
        n210beam_status = self.get_any_n210beam_node(str(start_time),str(end_time))
        vm_status = self.get_any_vm_node()
        listvm = []
        
        for node in nodes:
            self.lock.acquire()
            self.logger.info('Got the lock for node----------: ' + str(node))
            try:
                self.logger.debug("Parsing Requested Node's RSpec")
    
                #Check Sliver Type Field
                sliver_el = node.find("rspecV3:sliver_type",g_rspec_ns)
                if sliver_el == None:
                    return self.errorResult(am3.AM_API.BAD_ARGS,"No sliver type requested in the RSpec.")
                if sliver_el.get("name") not in ["usrp-vm","vm-2cpu-m4g"]:
                    self.logger.info("sliver_el.get(name) : " + sliver_el.get("name"))
                    return self.errorResult(am3.AM_API.BAD_ARGS,"Sliver type unsupported. Supported sliver types are: 'usrp-vm' and 'vm-2cpu-m4g'.")
    
                #Check Image Type Field
                img_el = None
                if sliver_el != None:
                    img_el = sliver_el.find("rspecV3:disk_image",g_rspec_ns)
                
                if img_el == None:
                    img_type = "gnuradio"
                elif img_el.get("name") not in ["urn:publicid:IDN+iris-testbed.connectcentre.ie+image+ndnrepo","urn:publicid:IDN+iris-testbed.connectcentre.ie+image+ovs", "urn:publicid:IDN+iris-testbed.connectcentre.ie+image+docker-img_hello-world_hello-world_lxc-udp", "urn:publicid:IDN+iris-testbed.connectcentre.ie+image+docker-img_lxc-udp", "urn:publicid:IDN+iris-testbed.connectcentre.ie+image+docker-img_hello-world", "urn:publicid:IDN+iris-testbed.connectcentre.ie+image+oai-cn", "urn:publicid:IDN+iris-testbed.connectcentre.ie+image+docker-criu","urn:publicid:IDN+iris-testbed.connectcentre.ie+image+aggmgr_cbtm_coor", "urn:publicid:IDN+iris-testbed.connectcentre.ie+image+plain", "urn:publicid:IDN+iris-testbed.connectcentre.ie+image+iris", "urn:publicid:IDN+iris-testbed.connectcentre.ie+image+iris-radar", "urn:publicid:IDN+iris-testbed.connectcentre.ie+image+gnuradio", "urn:publicid:IDN+iris-testbed.connectcentre.ie+image+plain-14-04", "urn:publicid:IDN+iris-testbed.connectcentre.ie+image+fosphor", "urn:publicid:IDN+iris-testbed.connectcentre.ie+image+srslte", "urn:publicid:IDN+iris-testbed.connectcentre.ie+image+wishful"]:
                    return self.errorResult(am3.AM_API.BAD_ARGS,"Image Type unsupported. Supported image types are: 'urn:publicid:IDN+iris-testbed.connectcentre.ie+image+plain', 'urn:publicid:IDN+iris-testbed.connectcentre.ie+image+iris', 'urn:publicid:IDN+iris-testbed.connectcentre.ie+image+iris-radar','urn:publicid:IDN+iris-testbed.connectcentre.ie+image+gnuradio', 'urn:publicid:IDN+iris-testbed.connectcentre.ie+image+plain-14-04', 'urn:publicid:IDN+iris-testbed.connectcentre.ie+image+fosphor', 'urn:publicid:IDN+iris-testbed.connectcentre.ie+image+srslte', 'urn:publicid:IDN+iris-testbed.connectcentre.ie+image+wishful', 'urn:publicid:IDN+iris-testbed.connectcentre.ie+image+ovs', 'urn:publicid:IDN+iris-testbed.connectcentre.ie+image+ndnrepo', 'urn:publicid:IDN+iris-testbed.connectcentre.ie+image+docker-img_hello-world_hello-world_lxc-udp', 'urn:publicid:IDN+iris-testbed.connectcentre.ie+image+docker-img_lxc-udp', 'urn:publicid:IDN+iris-testbed.connectcentre.ie+image+docker-img_hello-world', 'urn:publicid:IDN+iris-testbed.connectcentre.ie+image+oai-cn', 'urn:publicid:IDN+iris-testbed.connectcentre.ie+image+docker-criu','urn:publicid:IDN+iris-testbed.connectcentre.ie+image+aggmgr_cbtm_coor'.")
                else:
                    img_type = img_el.get("name").split('+')[-1] #
    
                #Get client Id
                client_id = node.get('client_id')
                if client_id == None:
                    return self.errorResult(am3.AM_API.BAD_ARGS,"No client Id specified in the RSpec.")
                
                self.logger.info("client_id: "+client_id)
                
                #Check Availability of USRPs
                comp_id = node.get('component_id')
                
                sliver_hw = node.find("rspecV3:hardware_type",g_rspec_ns)
                
                equtype = None
                if (sliver_hw != None):
                    equtype = sliver_hw.get("name")
                    self.logger.info("equtype from sliver_hardware.get (: "+equtype)         
                
                if sliver_el.get("name") == "vm-2cpu-m4g":
                    equtype = "vm-2cpu-m4g"
                
                usrp=None
                usrp_id = 1
                
                if comp_id == None:
                    #This node is unbounded
                    self.logger.info("Received unbounded request. Checking for availability")
                    if equtype == None or equtype == "n210":
                        for key, available in usrps_status.iteritems():
                            if available == True:
                                usrp=key
                                usrps_status[key] = False #Set it as false to not request multiple time the same usrp
                                usrp_id = key
                                equtype="usrp-vm"
                                self.logger.info("Assigning a vm with ID: " +usrp)
                                break
                            
                        if usrp == None:
                            self.logger.warn("Not enough resources to satisfy unbounded request.")
                            return self.errorResult(am3.AM_API.TOO_BIG,"Not enough n210 USRP resources to satisfy unbound request at the selected times.")     
                        
                    elif equtype == "x310":
                        for key, available in x310_status.iteritems():
                            if available == True:
                                usrp=key
                                usrps_status[key] = False #Set it as false to not request multiple time the same usrp
                                usrp_id = key
                                equtype="x310"
                                self.logger.info("Assigning a x310 vm with ID: " +usrp)
                                break
                    
                    elif equtype == "n210beam":
                        for key, available in n210beam_status.iteritems():
                            if available == True:
                                usrp=key
                                usrps_status[key] = False #Set it as false to not request multiple time the same usrp
                                usrp_id = key
                                equtype="n210beam"
                                self.logger.info("Assigning a n210beam vm with ID: " +usrp)
                                break
                            
                    elif equtype == "vmhost-type1-vm" or sliver_el.get("name") == "vm-2cpu-m4g":
                        if novms > 0:
                            usrp_id = 1
                            usrp = 1
                            equtype="vm-2cpu-m4g"
                            self.logger.info("Assigning a vm-2cpu-m4g vm with ID: " +str(usrp))
                            
                else:
                    self.logger.info("comp_id: "+comp_id)
                    usrp = comp_id.split('+')[3]
                    if usrp not in ["11", "12", "13", "14", "21", "22", "23", "24","31", "32", "33", "34", "41", "42", "43", "44", "51", "52", "53", "54", "63", "64", "vmhost1"]:
                        self.logger.warn("Non-existing USRP requested.")
                        return self.errorResult(am3.AM_API.BAD_ARGS,"Non-existing USRP requested.")
                    
                    usrpAvailable = False
                        
                    for key, available in usrps_status.iteritems():
                        if key == usrp:
                            usrp_id = usrp
                            usrpAvailable = available
                            equtype="usrp-vm"
                            self.logger.info("n210 equtype: "+equtype)
                        
                    for key, available in x310_status.iteritems():
                        if key == usrp:
                            usrp_id = usrp
                            usrpAvailable = available
                            equtype="x310"
                            self.logger.info("x310 equtype: "+equtype)
                        
                    for key, available in n210beam_status.iteritems():
                        if key == usrp:
                            usrp_id = usrp
                            usrpAvailable = available
                            equtype="n210beam"
                            self.logger.info("n210beam equtype: "+equtype)
    
                    for key, available in vm_status.iteritems():
                        if usrp == "vmhost1":
                            if key == 1 and novms > 0:
                                usrp_id = 1
                                usrpAvailable = True
                                equtype="vm-2cpu-m4g"
                                self.logger.info("Assign a vm-2cpu-m4g vm ID= " +str(usrp))
                        
                    if usrpAvailable == False:
                        return self.errorResult(am3.AM_API.TOO_BIG,"Selected USRP is in use. USRP Id: "+usrp)
                
            finally:
                usrp_list.append((usrp_id,img_type,client_id, equtype))
                self.logger.info("adding vm list usrp_list: "+str(usrp_list))
                self.logger.info('Released the lock')
                self.lock.release()
                
            
        #Resources are available, create slice.
        #Experimental Code
        if slice_urn in self._slices:
            #Slice already exist. We do not support this yet.
            template = "Slice %s already exists. Create a new slice."
            self.logger.error(template % (slice_urn))
            return self.errorResult(am3.AM_API.ALREADY_EXISTS,
                                    template % (slice_urn))
        else:
            newslice = Slice(slice_urn)
        
        for usrp_info in usrp_list:
            self.lock.acquire()
            self.logger.info('Got the lock for rspec: ' + str(usrp_list))
            try:
                if usrp_info[3] == "usrp-vm":
                    for resource in self._agg.resources:
                        if (usrp_info[0] == str(resource._usrp_id)):
                            resource.external_id = usrp_info[2]
                            resource.available = False
                            self.logger.info( "Resource usrp-vm: "+ str(resource._usrp_id) + " usrp_info " + usrp_info[0])
                            sliver = newslice.add_resource(resource)
                            sliver.setExpiration(expiration)
                            sliver.setStartTime(start_time)
                            sliver.setEndTime(end_time)
                            sliver.setAllocationState(am3.STATE_GENI_ALLOCATED)
                            gtw_msg = 'AM::allocate'+','+usrp_info[0]+','+user+','+email+','+str(end_time)+','+usrp_info[1]
                            msg = GatewayQuery(host,port,gtw_msg)
                            if 'success' not in msg:
                                return self.errorResult(am3.AM_API.REFUSED,"Could not allocate the n210 resource")
                    
                elif usrp_info[3] == "x310":
                    for resource in self._agg.resources:
                        if (usrp_info[0] == str(resource._usrp_id)):
                            resource.external_id = usrp_info[2]
                            resource.available = False
                            self.logger.info( "Resource x310: "+ str(resource._usrp_id) + " usrp_info " + usrp_info[0])
                            sliver = newslice.add_resource(resource)
                            sliver.setExpiration(expiration)
                            sliver.setStartTime(start_time)
                            sliver.setEndTime(end_time)
                            sliver.setAllocationState(am3.STATE_GENI_ALLOCATED)
                            gtw_msg = 'php::make_reservation_x310'+','+usrp_info[0]+','+user+','+email+','+str(start_time)+','+str(end_time)+','+usrp_info[1] +',am'
                            msg = GatewayQuery(host,port,gtw_msg + gtw_msg)
                            if 'success' not in msg:
                                return self.errorResult(am3.AM_API.REFUSED,"Could not allocate the X310 resource")
                    
                    
                elif usrp_info[3] == "n210beam":
                    for resource in self._agg.resources:
                        if (usrp_info[0] == str(resource._usrp_id)):
                            resource.external_id = usrp_info[2]
                            resource.available = False
                            self.logger.info( "Resource beam: "+ str(resource._usrp_id) + " usrp_info " + usrp_info[0])
                            sliver = newslice.add_resource(resource)
                            sliver.setExpiration(expiration)
                            sliver.setStartTime(start_time)
                            sliver.setEndTime(end_time)
                            sliver.setAllocationState(am3.STATE_GENI_ALLOCATED)
    
                            gtw_msg = 'php::make_reservation_n210beam'+','+usrp_info[0]+','+user+','+email+','+str(start_time)+','+str(end_time)+','+usrp_info[1] +',am'
                            msg = GatewayQuery(host,port,gtw_msg)
                            if 'success' not in msg:
                                return self.errorResult(am3.AM_API.REFUSED,"Could not allocate the n210 beam resource")    
    
                # vm-2cpu-m4g case
                elif usrp_info[3] == "vm-2cpu-m4g":
                    for resource in self._agg.resources:
                        if (str(usrp_info[0]) == str(resource._usrp_id)):
                            vmres = IrisVM( self._agg, "vm-2cpu-m4g" , int(1), str_to_bool("True"),"vm-2cpu-m4g")
                            vmres.external_id = usrp_info[2]
                            [no, vm_id] = GatewayQuery(host,port,'AM::list_vms_resource').split(',')
                            vmres.vmid = vm_id
                            self.logger.info("how many loops? vm_id: "+str(vm_id) + " usrp_info[2]" + str(usrp_info[2]) + " resource.id" + str(vmres.id))
                            if (int(no) > 1):
                                resource.available = True
                            sliver = newslice.add_resource(vmres)
                            sliver.setExpiration(expiration)
                            sliver.setStartTime(start_time)
                            sliver.setEndTime(end_time)
                            sliver.setAllocationState(am3.STATE_GENI_ALLOCATED)
                        
                            self.logger.info("usrp_info client_id: "+str(usrp_info))
                            gtw_msg = 'php::make_simulation_reservation'+',2,4,'+user+','+email+','+str(start_time)+','+str(end_time)+',am,' + '13,'+usrp_info[1]
                            msg = GatewayQuery(host,port,gtw_msg)
                            if 'success' not in msg:
                                return self.errorResult(am3.AM_API.REFUSED,"Could not allocate a vm resource")    
       
            finally:
                self.logger.info('Released a resource lock')
                self.lock.release()
                    
        self._agg.allocate(slice_urn, newslice.resources())
        self._agg.allocate(user_urn, newslice.resources())
        self._slices[slice_urn] = newslice
        
        manifest = self.manifest_rspec(slice_urn)
        result = dict(geni_rspec=manifest,
                      geni_slivers=[s.status() for s in newslice.slivers()])

        success_result = self.successResult(result)
        self.logger.debug("Allocate Result: "+str(success_result))
        
        return success_result
            #End of experimental code

    def Provision(self, urns, credentials, options):
        '''
            Returns:
                (rspec): Rspec with information about the provisioning.
        '''
        self.logger.info("provision urns = "+ str(urns)) 
        self.logger.info('Called Provision in testbed.py')
        the_slice, slivers = self.decode_urns(urns)
        privileges = (am3.PROVISION_PRIV,)
        creds = self.getVerifiedCredentials(the_slice.urn,
                                            credentials,
                                            options,
                                            privileges)

        #Experimental
        self.logger.info('Provision(%r)' % (urns))
        self.expire_slivers()

        the_slice, slivers = self.decode_urns(urns)

        #Basic sanity check
        #creds = self.getVerifiedCredentials(the_slice.urn, credentials, options, privileges)
        if 'geni_rspec_version' not in options:
            # This is a required option, so error out with bad arguments.
            self.logger.error('No geni_rspec_version supplied to Provision.')
            return self.errorResult(am3.AM_API.BAD_ARGS,
                                    'Bad Arguments: option geni_rspec_version was not supplied.')
        if 'type' not in options['geni_rspec_version']:
            self.logger.error('Provision: geni_rspec_version does not contain a type field.')
            return self.errorResult(am3.AM_API.BAD_ARGS,
                                    'Bad Arguments: option geni_rspec_version does not have a type field.')
        if 'version' not in options['geni_rspec_version']:
            self.logger.error('Provision: geni_rspec_version does not contain a version field.')
            return self.errorResult(am3.AM_API.BAD_ARGS,
                                    'Bad Arguments: option geni_rspec_version does not have a version field.')

        # Look to see what RSpec version the client requested
        # Error-check that the input value is supported.
        rspec_type = options['geni_rspec_version']['type']
        if isinstance(rspec_type, str):
            rspec_type = rspec_type.lower().strip()
        rspec_version = options['geni_rspec_version']['version']
        if rspec_type != 'geni':
            self.logger.error('Provision: Unknown RSpec type %s requested', rspec_type)
            return self.errorResult(am3.AM_API.BAD_VERSION,
                                    'Bad Version: requested RSpec type %s is not a valid option.' % (rspec_type))
        if rspec_version != '3':
            self.logger.error('Provision: Unknown RSpec version %s requested', rspec_version)
            return self.errorResult(am3.AM_API.BAD_VERSION,
                                    'Bad Version: requested RSpec version %s is not a valid option.' % (rspec_version))
        self.logger.info("Provision requested RSpec %s (%s)", rspec_type, rspec_version)

        #Get user info from credentials
        try:
            userdata_raw = creds[0].get_gid_caller().get_data()
            userdata = userdata_raw.split(',')
            email = userdata[1].split('email:')[1]
        except:
            if not email:
                email = ''
                self.logger.debug('Cannot get email from Credentials. Using \
                                                                    blank email')

        if 'geni_users' not in options:
            return self.errorResult(am3.AM_API.BAD_ARGS,"Please specify User and SSH Key to deploy.")
        try:
            user_id = options['geni_users'][0]['urn'].split('user+')[1]
            ssh_key = options['geni_users'][0]['keys'][0]
        except:
            self.logger.debug('Cannot get user/ssh_key from Options.')
            return self.errorResult(am3.AM_API.BAD_ARGS,"Please specify User and SSH Key to deploy.")

        # Only provision slivers that are in the scheduled time frame
        #TODO Check if slivers are schelude correctly?
        max_expiration = self.min_expire(creds, self.max_lease,
                                     ('geni_end_time' in options
                                      and options['geni_end_time']))

        for sliver in slivers:
            # Extend the lease and set to PROVISIONED
            expiration = min(sliver.endTime(), max_expiration)
            sliver.setEndTime(expiration)
            sliver.setExpiration(expiration)
            sliver.setAllocationState(am3.STATE_GENI_PROVISIONED)
            sliver.setOperationalState(am3.OPSTATE_GENI_NOT_READY)
        

   
        img_type = "plain"
        for s in range(len(slivers)):
                if str(slivers[s].resource().type) == "usrp-vm":
                    query_msg = 'AM::provision,'+str(slivers[s].resource()._usrp_id)+','+img_type+','+str(user_id)+','+str(ssh_key)+','+str(email)+',am'
#                     self.logger.info( "UUID: "+ slivers[s].resource().uuid)
                    msg = GatewayQuery(host,port,query_msg)
                    self.logger.info( "UUID msg response: " + msg + " query_msg: "+query_msg)
                    if 'Already Provisioned' in msg:
                        return self.errorResult(am3.AM_API.ALREADYEXISTS,"The Resource is already in use (already provisioned).")
                    try:
                        slivers[s].resource().ipaddress = msg.split(',')[1]
                    except:
                        slivers[s].resource().ipaddress = '0.0.0.0'
                        
                    slivers[s].resource().user_urn = options['geni_users'][0]['urn'][0]
                    slivers[s].resource().user_id = user_id    
                    self.logger.debug(str(slivers[s].resource()._usrp_id)+' private IP assigned '+str(slivers[s].resource().ipaddress)+' in Provision')

                if str(slivers[s].resource().type) == "x310":
                    query_msg = 'AM::provision_x310,'+str(slivers[s].resource()._usrp_id)+','+img_type+','+str(user_id)+','+str(ssh_key)+','+str(email)+',am'
                    #print "UUID: ", slivers[s].resource().uuid
                    msg = GatewayQuery(host,port,query_msg)
                    if 'Already Provisioned' in msg:
                        return self.errorResult(am3.AM_API.ALREADYEXISTS,"The Resource is already in use (already provisioned).")
                    try:
                        slivers[s].resource().ipaddress = msg.split(',')[1]
                    except:
                        slivers[s].resource().ipaddress = '0.0.0.0'
                        
                    slivers[s].resource().user_urn = options['geni_users'][0]['urn'][0]
                    slivers[s].resource().user_id = user_id    
                    self.logger.debug(str(slivers[s].resource()._usrp_id)+' private IP assigned '+str(slivers[s].resource().ipaddress)+' in Provision')

                if str(slivers[s].resource().type) == "n210beam":
                    query_msg = 'AM::provision_n210beam,'+str(slivers[s].resource()._usrp_id)+','+img_type+','+str(user_id)+','+str(ssh_key)+','+str(email)+',am'
                    #print "UUID: ", slivers[s].resource().uuid
                    msg = GatewayQuery(host,port,query_msg)
                    if 'Already Provisioned' in msg:
                        return self.errorResult(am3.AM_API.ALREADYEXISTS,"The Resource is already in use (already provisioned).")
                    try:
                        slivers[s].resource().ipaddress = msg.split(',')[1]
                    except:
                        slivers[s].resource().ipaddress = '0.0.0.0'
                        
                    slivers[s].resource().user_urn = options['geni_users'][0]['urn'][0]
                    slivers[s].resource().user_id = user_id    
                    self.logger.debug(str(slivers[s].resource()._usrp_id)+' private IP assigned '+str(slivers[s].resource().ipaddress)+' in Provision')

                if str(slivers[s].resource().type) == "vm-2cpu-m4g":
                    serverlist = ['13', '15', '17', '19']
                    host_rack_id = random.choice(serverlist)
                    self.logger.info(str(host_rack_id))
                    query_msg = 'AM::provisionVM,'+str(slivers[s].resource().vmid)+','+host_rack_id+',4,simulation,1,'+img_type+','+str(user_id)+','+str(ssh_key)+','+str(email)
                    self.logger.debug("AM::provisionVM id= ", str(slivers[s].resource().vmid))
                    msg = GatewayQuery(host,port,query_msg)
                    if 'Already Provisioned' in msg:
                        return self.errorResult(am3.AM_API.ALREADYEXISTS,"The Resource is already in use (already provisioned).")
                    try:
                        slivers[s].resource().ipaddress = msg.split(',')[1]
                    except:
                        slivers[s].resource().ipaddress = '0.0.0.0'
                        
                    slivers[s].resource().user_urn = options['geni_users'][0]['urn'][0]
                    slivers[s].resource().user_id = user_id
                    self.logger.debug(str(slivers[s].resource()._usrp_id)+' private IP assigned '+str(slivers[s].resource().ipaddress)+' in Provision')

        result = dict(geni_rspec=self.manifest_rspec(the_slice.urn),
                      geni_slivers=[s.status() for s in slivers])

        return self.successResult(result)

    def PerformOperationalAction(self, urns, credentials, action, options):
        '''
            Returns:
                (rspec): Rspec with information regarding the Operational Action
        '''
        self.logger.info('Called PerformOperationalAction in testbed.py')
        the_slice, slivers = self.decode_urns(urns)
        privileges = (am3.PERFORM_ACTION_PRIV,)
        creds = self.getVerifiedCredentials(the_slice.urn,
                                            credentials,
                                            options,
                                            privileges)

        # A place to store errors on a per-sliver basis.
        # {sliverURN --> "error", sliverURN --> "error", etc.}
        astates = []
        ostates = []
        if action == 'geni_start':
            astates = ['geni_provisioned']
            ostates = ['geni_notready']
        elif action == 'geni_restart':
            astates = ['geni_provisioned']
            ostates = ['geni_ready']
        elif action == 'geni_stop':
            astates = ['geni_provisioned']
            ostates = ['geni_ready']
        else:
            msg = "Unsupported: action %s is not supported" % (action)
            raise ApiErrorException(am3.AM_API.UNSUPPORTED, msg)

        # Handle best effort. Look ahead to see if the operation
        # can be done. If the client did not specify best effort and
        # any resources are in the wrong state, stop and return an error.
        # But if the client specified best effort, trundle on and
        # do the best you can do.

        errors = collections.defaultdict(str)
        for sliver in slivers:
            # ensure that the slivers are provisioned
            if (sliver.allocationState() not in astates
                or sliver.operationalState() not in ostates):
                msg = "%d: Sliver %s is not in the right state for action %s."
                msg = msg % (am3.AM_API.UNSUPPORTED, sliver.urn(), action)
                errors[sliver.urn()] = msg
        best_effort = False
        if 'geni_best_effort' in options:
            best_effort = bool(options['geni_best_effort'])
        if not best_effort and errors:
            raise ApiErrorException(am3.AM_API.UNSUPPORTED,
                                    "\n".join(errors.values()))

        #result = super(MyTestbedDelegate, self).PerformOperationalAction(urns, credentials, action, options)
        #result ['value']['geni_operational_status'] = 'geni_ready'
	#print result

        for s in range(len(slivers)):
            self.logger.info('sliver ,'+str(slivers[s].resource()._usrp_id)+' in PerformOperationalAction')
            print slivers[s]._operational_state
            #self.logger.debug(str(slivers[s]._operational_state)+' forced to geni_configuring')

            if action=='geni_stop':
                slivers[s].setOperationalState('geni_notready')
            elif action=='geni_start':
                slivers[s].setOperationalState('geni_configuring')
            elif action=='geni_restart':
                slivers[s].setOperationalState('geni_configuring')
            else:
                return self.errorResult(am3.AM_API.UNSUPPORTED,"Operation Unsupported.")
            print slivers[s]._operational_state
            
            if slivers[s].resource()._usrp_id == 1:
                query_msg = 'AM::PerformVMOperationalAction,simulation_eu_,'+action+','+str(slivers[s].resource().vmid)+',am'
            else:
                query_msg = 'AM::PerformVMOperationalAction,basic_eu_,'+action+','+str(slivers[s].resource()._usrp_id)+',am'
            
            print "Query operational msg",query_msg
            msg = GatewayQuery(host,port,query_msg)

        result = super(MyTestbedDelegate, self).successResult([s.status(errors[s.urn()]) for s in slivers])
        print result
        return result

    def Status(self, urns, credentials, options):
        '''
            Returns:
                (rspec): Rspec with the Status of the Slivers
        '''
        self.logger.info('Called Status in testbed.py')
        the_slice, slivers = self.decode_urns(urns)
        privileges = (am3.SLIVERSTATUSPRIV,)
        creds=self.getVerifiedCredentials(the_slice.urn,
                                          credentials,
                                          options,
                                          privileges)

        userdata = creds[0].get_gid_caller().get_data()
        if (len(userdata.split(',')) == 2):  
            [user_urn, URN] = userdata.split(',')
        else:
            [user_urn, email, URN] = userdata.split(',')
        
        user_id = user_urn.split('user+')[1]

        #Here we have to query the CBTm to see the status of VM relative to each sliver
        for s in range(len(slivers)):
            if slivers[s].allocationState()== 'geni_provisioned':
                msg = ""
                if slivers[s].resource()._usrp_id == 1:
                    msg = GatewayQuery(host,port,'php::describeVM,'+str(slivers[s].resource().vmid)+',simulation_eu_')
                    msg = self.vmstatus(msg, slivers[s].resource().vmid)
                else:
                    msg = GatewayQuery(host,port,'AM::status,'+str(slivers[s].resource()._usrp_id))
                    
                slivers[s].setOperationalState(msg.split(',')[1])
                slivers[s].resource().ipaddress = msg.split(',')[2]
                slivers[s].resource().user_urn = user_urn
                slivers[s].resource().user_id = user_id

        result = super(MyTestbedDelegate, self).Status(urns, credentials, options)
        return result
    
    def vmstatus(self,msg, usrp):
        if 'free' in msg:
            msg = str(usrp)+',geni_configuring,'+'0.0.0.0'
        elif 'wait' in msg:
            msg = str(usrp)+',geni_configuring,'+'0.0.0.0'
        else:
            self.logger.info(str(usrp)+',geni_ready,'+msg.split(',')[1])
            msg = str(usrp)+',geni_ready,'+msg.split(',')[1]
        return msg
        

    def Delete(self, urns, credentials, options):
        '''
            Delete a Sliver and free it's resources
            Returns:
                (rspec): Rspec with details about the delete operation
        '''
        self.logger.info('Called delete in testbed.py')
        the_slice, slivers = self.decode_urns(urns)
        privileges = (am3.DELETESLIVERPRIV,)
        creds=self.getVerifiedCredentials(the_slice.urn,
                                          credentials,
                                          options,
                                          privileges)


        # Grab the user_urn
        user, email = self.get_user_data_from_creds(creds) #TODO Add multiple users to the nodes.
        self.logger.info("Got user: "+user+" E-mail: "+email)
        list_user_id = []
        
        for s in range(len(slivers)):

            print("print sliver type " + str(slivers[s].resource().type))
#             if the_urn.getType() != 'slice':
            if str(slivers[s].resource()._usrp_id) in ["13", "14", "21", "22", "23", "24","31", "32", "33", "34", "41", "42", "43", "44", "51", "52", "53", "54", "63", "64"]:
                self.logger.info('Deleting resource '+str(slivers[s].resource()._usrp_id))
                msg = GatewayQuery(host,port,'AM::release,'+str(slivers[s].resource()._usrp_id))
                list_user_id.append(slivers[s].resource().user_id)
            
            if str(slivers[s].resource()._usrp_id) in ["11", "44"]:
                self.logger.info('Deleting x310 resource '+str(slivers[s].resource()._usrp_id))
                msg = GatewayQuery(host,port,'AM::delete_allocation_x310,'+str(slivers[s].resource()._usrp_id)+','+user)
                list_user_id.append(slivers[s].resource().user_id)
        
            if str(slivers[s].resource()._usrp_id) in ["12", "43"]:
                self.logger.info('Deleting n210 beam resource '+str(slivers[s].resource()._usrp_id))
                msg = GatewayQuery(host,port,'AM::delete_allocation_n210beam,'+str(slivers[s].resource()._usrp_id)+','+user)
                list_user_id.append(slivers[s].resource().user_id)

            if str(slivers[s].resource().type) == "vm-2cpu-m4g":
                self.logger.info('Deleting vm-2cpu-m4g resource '+str(slivers[s].resource()._usrp_id))
                msg = GatewayQuery(host,port,'AM::delete_simulation,'+str(slivers[s].resource().vmid)+',simulation_eu_,'+user)
                list_user_id.append(slivers[s].resource().user_id)

        list_user_id = list(set(list_user_id))
        # We have de-authorize (delete the public key) to access the testbed ? Perhaps not...
        # The coordinator should take care of this, not the AM ...
        """
        for user_id in list_user_id:
            # here communicate with CBTm to check if user_id has other experiments active
            if any(user_id==u for u in os.popen("cut -d: -f1 /etc/passwd").read().split('\n')):
                try:
                    self.logger.info('user '+str(user_id)+' can be deleted')
                    os.popen("deluser --remove-home "+user_id)
                except:
                    self.logger.debug('ERROR: something went wrong while deleting '+str(user_id))
        """

        result = super(MyTestbedDelegate, self).Delete(urns, credentials, options)
        return result

    def Describe(self, urns, credentials, options):
        '''
            Returns:
                (rspec): Aggregate Manager Version Rspec
        '''
        self.logger.info('Called Describe in testbed.py')
        the_slice, slivers = self.decode_urns(urns)
        privileges = (am3.SLIVERSTATUSPRIV,)
        creds= self.getVerifiedCredentials(the_slice.urn,
                                           credentials,
                                           options,
                                           privileges)

        for s in range(len(slivers)):
            if ((slivers[s].allocationState()== 'geni_provisioned') and (slivers[s].operationalState()== 'geni_ready')):
                msg = ""
                if str(slivers[s].resource().type) == "usrp-vm":
                    msg = GatewayQuery(host,port,'AM::describe,'+str(slivers[s].resource()._usrp_id))
                
                if str(slivers[s].resource().type) == "x310":
                    msg = GatewayQuery(host,port,'AM::describe_x310,'+str(slivers[s].resource()._usrp_id))
                                    
                if str(slivers[s].resource().type) == "n210beam":
                    msg = GatewayQuery(host,port,'AM::describe_n210beam,'+str(slivers[s].resource()._usrp_id))
                    
                if str(slivers[s].resource().type) == "vm-2cpu-m4g":
                    print("print sliver type describe " + str(slivers[s].resource().vmid))
                    msg = GatewayQuery(host,port,'php::describeVM,'+str(slivers[s].resource().vmid)+',simulation_eu_')
                
        result = super(MyTestbedDelegate, self).Describe(urns, credentials, options)
        return result

    def Renew(self, urns, credentials, expiration_time, options):
        '''
            Renew sliver expiration.
            TODO:
                only update database if the status of the slice is 'geni_allocated'
            TODO:
                update the database also.
            Returns:
                (rspec): Rspec with information about the renew operation
        '''
        self.logger.info('Called Renew in testbed.py')
        the_slice, slivers = self.decode_urns(urns)
        privileges = (am3.RENEWSLIVERPRIV,)
        creds = self.getVerifiedCredentials(the_slice.urn,
                                            credentials,
                                            options,
                                            privileges)

        exp_time = self._naiveUTC(dateutil.parser.parse(expiration_time))
        try:
            userdata = creds[0].get_gid_caller().get_data()
            if (len(userdata.split(',')) == 2):
                [userURN, URN] = userdata.split(',')
                user = userURN.split('user+')[1]
            else:
                [userURN, email, URN] = userdata.split(',')
                user = userURN.split('user+')[1]
        except:
                return self.errorResult(am3.AM_API.BAD_ARGS,"Bad Arguments: could not extract user from credentials.")

        for s in range(len(slivers)):
            msg = ""
            print("print sliver type describe " + str(slivers[s].resource().type))
            if str(slivers[s].resource().type) == "usrp-vm":
                msg = GatewayQuery(host,port,'AM::renew,'+str(slivers[s].resource()._usrp_id)+','+user+','+ str(exp_time))
            if str(slivers[s].resource().type) == "x310":
                msg = GatewayQuery(host,port,'AM::renewX310,'+str(slivers[s].resource()._usrp_id)+','+user+','+ str(exp_time))
            if str(slivers[s].resource().type) == "n210beam":
                msg = GatewayQuery(host,port,'AM::renewBeam,'+str(slivers[s].resource()._usrp_id)+','+user+','+ str(exp_time))
            if str(slivers[s].resource().type) == "vm-2cpu-m4g":
                msg = GatewayQuery(host,port,'AM::renewVM,'+str(slivers[s].resource().vmid)+','+user+','+ str(exp_time))
                
            if 'success' not in msg:
                return self.errorResult(am3.AM_API.REFUSED,"Operation Refused: Could not Renew the resources.")

        result = super(MyTestbedDelegate, self).Renew(urns, credentials, expiration_time, options)
        return result

    def Shutdown(self, slice_urn, credentials, options):
        '''
            Emergency shutdown for Slices. Used only by ADM
            Returns:
                (rspec): Rspec with information about the shutdown.
        '''
        self.logger.info('Called Shutdown in testbed.py')
        privileges = (am3.SHUTDOWNSLIVERPRIV,)
        self.getVerifiedCredentials(slice_urn,
                                    credentials,
                                    options,
                                    privileges)
        return self.successResult(True)

    def decode_urns(self,urns,**kwargs):
        """
            Several methods need to map URNs to slivers and/or deduce
            a slice based on the slivers specified.

            When called from AMMethodContext, kwargs will have 2 keys
            (credentials and options), with the same values as the credentials
            and options parameters of the AMv3 API entry points. This can be
            useful for delegates derived from the ReferenceAggregateManager,
            but is not used in this reference implementation.

            Returns a slice and a list of slivers.
        """
        # All delegate methods implementing AMv3 API are called in a context
        # (of class AMMethodContext that is created with a call to decode_urns
        # when urns are passed as arguments to the call.
        #
        # When a slice is found from the urns, and slice_urn is not part of the arguments
        # to the call, this sets slice_urn as part of the args to be used
        # by the authorizer when taking decisions, with a call to getURN on the first item
        # returned by decode_urns
            
        return super(MyTestbedDelegate, self).decode_urns(urns, **kwargs)

    def _naiveUTC(self, dt):
        return super(MyTestbedDelegate, self)._naiveUTC(dt)

    def getAggregateManagerId(self, certfile=None):
        '''
            Method to get the ID of the Aggregate Manager.

            Args:
                certfile (file): File that contains the certificate of the AM

            Returns:
                (String): Aggregate Manager ID
        '''
        if not hasattr(self, 'aggregate_manager_id'):
            print certfile
            cert=Certificate(filename=certfile)
            altSubject=cert.get_extension('subjectAltName')
            altSubjects=altSubject.split(', ')
            publicids=[s for s in altSubjects if 'publicid' in s]
            print publicids
            if len(publicids) < 1:
                raise Exception("Could not get aggregate manager id from subjectAltName as no altName has the string publicid")
            self.aggregate_manager_id=publicids[0][4:]
            self.logger.info("Will run am with %s as component_manager_id"%self.aggregate_manager_id)
        return self.aggregate_manager_id

    def errorResult(self, code, output, am_code=None):
        return super(MyTestbedDelegate, self).errorResult(code, output, am_code)

    def get_user_data_from_creds(self, creds):
        # GET USER DATA FROM CREDENTIALS

        try:
            userdata = creds[0].get_gid_caller().get_data()
            if (len(userdata.split(',')) == 2):
                [userURN, URN] = userdata.split(',')
                user = userURN.split('user+')[1]
                email = ''
            else:
                [userURN, email, URN] = userdata.split(',')
                user = userURN.split('user+')[1]
                email = email.split('email:')[1]
        except:
            if not user:
                user = 'user'
            if not email:
                email = ''
            self.logger.debug('Cannot get userdata from Credentials using \
                                           username = user and blank email')

        return (user, email)

    def get_experiment_expiration(self, options):
        #Get Experiment Expiration Time
        if 'geni_end_time' in options:
            # Need to parse this into datetime
            exp_time_raw = options['geni_end_time']
            exp_time = str(self._naiveUTC(dateutil.parser.parse(exp_time_raw)))
        else:
            exp_time = str(self._naiveUTC(datetime.datetime.utcnow()+timedelta(1)))
        #need to get the exp_time and start time from rspec.
        #but first need to implement conversion between rfc3339 to normal datetime format.
        #see: https://fed4fire-testbeds.ilabt.iminds.be/asciidoc/federation-am-api.html#CommonOptionEndTime
        #and: https://fed4fire-testbeds.ilabt.iminds.be/asciidoc/federation-am-api.html#StringDateTimeRfc3339
        return exp_time

    def get_any_available_node(self, start_time, exp_time):
        """
        Get the first USRP available between start_time and end_time.
        """
        msg = GatewayQuery(host,port,'AM::anyavailable,'+start_time+','+exp_time)
        self.logger.info("Got the following message from the coordinator" + " start+end time:" + start_time+","+exp_time)
        self.logger.info(msg)
        usrp_dict={}
        #print msg.split("\n")
        for usrp_info in msg.split("\n"):
            #print "In for loop\n", usrp_info
            [usrp_id, available] = usrp_info.split(",")
            if available == "True":
                available = True
            else:
                available = False
            usrp_dict[usrp_id]=available
        self.logger.info("Available USRPs:")
        self.logger.debug(str(usrp_dict))
        return usrp_dict

    def get_any_n210beam_node(self, start_time, exp_time):
        """
        Get the first USRP available between start_time and end_time.
        """
        msg = GatewayQuery(host,port,'AM::any_n210beam_available,'+start_time+','+exp_time)
        self.logger.info("Got the following message from the coordinator about n210beam nodes")
        self.logger.info(msg)
        usrp_dict={}
        #print msg.split("\n")
        for usrp_info in msg.split("\n"):
            #print "In for loop\n", usrp_info
            [usrp_id, available] = usrp_info.split(",")
            if available == "True":
                available = True
            else:
                available = False
            usrp_dict[usrp_id]=available
        self.logger.info("Available n210beam USRPs:")
        self.logger.info(str(usrp_dict))
        return usrp_dict
    
    def get_any_vm_node(self):
        """
        Get the first USRP available between start_time and end_time.
        """
        usrp_dict={}

        if (novms > 0):
            usrp_dict[1]=True
        else:
            usrp_dict[1]=False
        self.logger.info("Available vms:")
        self.logger.info(str(usrp_dict))
        return usrp_dict
    
    def get_any_x310_node(self, start_time, exp_time):
        """
        Get the first USRP available between start_time and end_time.
        """
        msg = GatewayQuery(host,port,'AM::any_x310_available,'+start_time+','+exp_time)
        self.logger.info("Got the following message from the coordinator about x310 nodes")
        self.logger.info(msg)
        usrp_dict={}
        #print msg.split("\n")
        for usrp_info in msg.split("\n"):
            #print "In for loop\n", usrp_info
            [usrp_id, available] = usrp_info.split(",")
            if available == "True":
                available = True
            else:
                available = False
            usrp_dict[usrp_id]=available
        self.logger.info("Available x310 USRPs:")
        self.logger.info(str(usrp_dict))
        return usrp_dict
    
    def advert_header(self):
        schema_locs = ["http://www.geni.net/resources/rspec/3",
                       "http://www.geni.net/resources/rspec/3/ad.xsd",
                       "http://www.geni.net/resources/rspec/ext/opstate/1",
                       "http://www.geni.net/resources/rspec/ext/opstate/1/ad.xsd"]
        header = '''<?xml version="1.0" encoding="UTF-8"?>
        <rspec xmlns="http://www.geni.net/resources/rspec/3"
            xmlns:emulab="http://www.protogeni.net/resources/rspec/ext/emulab/1"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="%s"
            type="advertisement">
        <!-- Operational states for usrp nodes -->
        <rspec_opstate xmlns="http://www.geni.net/resources/rspec/ext/opstate/1"
            aggregate_manager_id="%s"
            start="geni_notready">
        <hardware_type name="n210"/>
        <sliver_type name="usrp-vm"/>
        <state name="geni_notready">
        <description>USRP VM takes a while to become ready.</description>
        <action name="geni_start" next="geni_ready">
            <description>Transition the node to a ready state.</description>
        </action>
        </state>
        <state name="geni_ready">
            <description>The VM node is up and ready to use.</description>
            <action name="geni_restart" next="geni_ready">
                <description>Reboot the node</description>
            </action>
            <action name="geni_stop" next="geni_notready">
                <description>Power down or stop the node.</description>
            </action>
        </state>
        </rspec_opstate>
        '''
        return header % (' '.join(schema_locs), self._my_urn)

    def advert_resource(self, resource):
        tmpl = '''  <node component_manager_id="%s"
        component_name="%s"
        component_id="%s"
        exclusive="%s">
        <available now="%s"/>
        <hardware_type name="%s"/>
        <sliver_type name="usrp-vm">
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+plain"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+gnuradio"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+iris"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+plain-14-04"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+fosphor"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+srslte"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+wishful"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+iris-radar"/>
        </sliver_type>
        </node>
        '''
        resource_id = str(resource._usrp_id)
        hardware_type = str(resource._hardware_type)
        resource_name = str(resource._hardware_type)+"-"+str(resource._usrp_id)
        resource_exclusive = str(True).lower()
        resource_type = resource.type
        resource_available = str(resource.available).lower()
        #resource_urn = resource.urn(self._urn_authority)
        resource_urn = self._my_urn.replace("authority+am","")+"node+"+resource_id
        self.logger.info("resource_name= "+resource_name + " hardware_type: "+ hardware_type+" resource_exclusive " + resource_exclusive + " resource_available: " 
                         +resource_available + " resource_urn: " +resource_urn  + "  self._my_urn:" + self._my_urn )
        return tmpl % (self._my_urn,
                       resource_name,
                       resource_urn,
                       resource_exclusive,
                       resource_available,
                       hardware_type)

    def advert_vms(self, resource):
        tmpl = '''  <node component_manager_id="%s"
        component_name="%s"
        component_id="%s"
        exclusive="%s">
        <available now="%s"/>
        <hardware_type name="vmhost-type1-vm">
            <emulab:node_type type_slots="%s"/>
        </hardware_type>
        <sliver_type name="%s">
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+plain"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+plain-14-04"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+aggmgr_cbtm_coor"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+docker-criu"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+oai-cn"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+docker-img_hello-world"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+docker-img_lxc-udp"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+docker-img_hello-world_hello-world_lxc-udp"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+ovs"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+ndnrepo"/>
            <disk_image name="urn:publicid:IDN+iris-testbed.connectcentre.ie+image+wishful"/>
        </sliver_type>
        </node>
        '''
        vms_avaliable = novms
        resource_name = "vmhost"+str(resource._usrp_id)
        resource_exclusive = str(False).lower()
        resource_type = resource.type
        resource_available = str(resource.available).lower()
        resource_urn = self._my_urn.replace("authority+am","")+"node+"+resource_name
        self.logger.info("VM RESOURCE ADVERT  -----------  resource_name= "+resource_name + " vms_avaliable: "+ vms_avaliable+" resource_exclusive " + resource_exclusive + " resource_available: " 
                         +resource_available + " resource_urn: " +resource_urn  + "  self._my_urn:" + self._my_urn + " resource_type: "+resource_type)
        return tmpl % (self._my_urn,
                       resource_name,
                       resource_urn,
                       resource_exclusive,
                       resource_available,
                       vms_avaliable,
                       resource_type)

    def advert_footer(self):
        return '</rspec>'
