#!/bin/bash

echo "About to kill these processes:"
pgrep -u root -f gcf-am.py --list-full

read -p "Do you want to proceed?" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo pkill -u root -f gcf-am.py
fi

