Design Documentation
========================

The aim of the Aggregate Manager is to expose the Iris testbed, allowing
researchers to use it's resources to perform experiments. There are two ways of
accessing the resources from this testbed:

    1) jFed-experimenter (http://jfed.iminds.be/), mainly for Fed4Fire participants
    2) Internal Webserver, for Trinity researchers.


Aggregate Manager's Overall structure
#####################################

.. _am-overall:

.. figure:: _imgs/am_overall.svg

   Aggregate Manager Overall Structure


Coordinator
###########
This is the central module of the Aggregate Manager. It is responsible for:

    1) create/delete/update users on the gateway
    2) create/delete reservations for USRPs (TCD+Fed4FIRE) and Simulations (TCD only)
    3) Add hosts and users to the Zabbix monitoring system.

It is composed of smaller modules, called handlers. Each handler manages requests
from specific clients (Aggregate Manager, Webserver or Cleaner)

Also, the Coordinator communicates with the CBTm, based on the requests from the
clients.


Handlers
********
am_handler.py
-------------
Handles requests from the GCF Aggregate Manager.

php_handler.py
--------------
Handles requests from the Webserver (http://iris-testbed.connectcentre.ie/reservation/)

cleaner_handler.py
------------------
Handles requests from the Cleaner


Cleaner
#######
The Cleaner is responsible for sending reminders, removing expired
reservations from the Database and deleting their respective VM's, if already
deployed.


Aggregate Manager (GCF/Geni-Tools)
##################################
This is the actual Aggregate Manager. It was based on the GCF Aggregate Manager
API, now called Geni-Tools (https://github.com/GENI-NSF/geni-tools) and on the
bootstrap-geni-am (https://github.com/dmargery/bootstrap-geni-am/wiki), that is
an extension of the gcf-am.

This communicates with the jFed Experimenter (http://jfed.iminds.be/), a
framework for Federated Testbeds.

.. _am-close:

.. figure:: _imgs/am_close.svg

   Aggregate Manager


Aggregate Manager's Clearinghouse (?)
#####################################

.. warning::
    Should we talk about this?
