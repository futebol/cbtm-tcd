Experiment Tutorial
===================

In this section, we provide a basic tutorial on how run a simple experiment on the IRIS testbed. To run this tutorial you will need jFed installed,
of at least version 5.6. To install jFed, proceed to http://jfed.iminds.be/

Experiment Description
**********************

In this experiment, you will reserve two virtual machines, one for transmission and another for reception. Two stages for this tutorial will be
considered, one where a simple sinusoid wave is transmitted and observed over the air, and another where a OFDM signal is transmitted using GNURadio software.

Sinusoid Transmission
+++++++++++++++++++++

To reserve two virtual machines, open jFed and go to the RSpec editor. In the RSpec editor, include the RSpec found bellow and then press run.
This RSpec will request two nodes, 'tx_node' and 'rx_node'.

::

    <?xml version='1.0'?>
        <rspec xmlns="http://www.geni.net/resources/rspec/3" type="request" generated_by="jFed RSpec Editor" generated="2016-11-10T15:37:22.884Z" xmlns:emulab="http://www.protogeni.net/resources/rspec/ext/emulab/1" xmlns:jfedBonfire="http://jfed.iminds.be/rspec/ext/jfed-bonfire/1" xmlns:delay="http://www.protogeni.net/resources/rspec/ext/delay/1" xmlns:jfed-command="http://jfed.iminds.be/rspec/ext/jfed-command/1" xmlns:client="http://www.protogeni.net/resources/rspec/ext/client/1" xmlns:jfed-ssh-keys="http://jfed.iminds.be/rspec/ext/jfed-ssh-keys/1" xmlns:jfed="http://jfed.iminds.be/rspec/ext/jfed/1" xmlns:sharedvlan="http://www.protogeni.net/resources/rspec/ext/shared-vlan/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.geni.net/resources/rspec/3 http://www.geni.net/resources/rspec/3/request.xsd ">
        <node client_id="tx_node" exclusive="true" component_manager_id="urn:publicid:IDN+FAVORITE:FAVORITE+authority+am">
            <sliver_type name="usrp-vm"/>
            <location xmlns="http://jfed.iminds.be/rspec/ext/jfed/1" x="410.0" y="185.0"/>
            <img_type name="plain"/>
        </node>
        <node client_id="rx_node" exclusive="true" component_manager_id="urn:publicid:IDN+FAVORITE:FAVORITE+authority+am">
            <sliver_type name="usrp-vm"/>
            <location xmlns="http://jfed.iminds.be/rspec/ext/jfed/1" x="10.0" y="185.00000000000003"/>
            <img_type name="plain"/>
        </node>
        <jfed-command:experimentBarrierSegment orderNumber="0" tag="Barrier segment 0"/>
    </rspec>


After pressing run, select the project you belong to, the slice name you want to create and the duration of the reservation. Since this is only an
example reservation, a 2 hour reservation should suffice. Then, wait while the virtual machines are created and configured. This may take some time, 
since a copy of a template image needs to be done for you.

You will see the nodes you reserved in jFed turn from turn from pink, to yellow and finally to green.
Once the nodes turn green, the nodes are ready for experimentation. Double click on them to ssh into your virtual machine.

.. warning
    The disk images will not be persistent and will be deleted at the end of the reservation. Make sure you save the result of your experiments
    before your reservation expires.

The virtual machines will have the Universal Hardware Driver installed required to interact with the USRPs. To transmit a sinusoid we will use the 
UHD examples that come installed in the machine.

On the 'tx_node', run the following code::
    
    cd /usr/lib/uhd/examples
    ./tx_waveforms --freq 2550000000 --rate 200000 --gain 25
    
On the 'rx_node', run the following code::
    
    cd /usr/lib/uhd/examples
    ./rx_ascii_art_dft --freq 2550000000 --rate 2000000 --ref-lvl -50

You should be able to see the ascii reprsentation of the transmitted waveform spectrum,
as illustrated below:

::

                                                                                                               .
  -55                                                                                                          |
                                                                                                               |
                                                                                                               |
                                                                                                               |
                                                                                                               |
  -60                                                                                                          |
                                                                                                               |
                                                                                                               |
                                                                                                               |
                                                                                                               |
  -65                                                                                                          |
                                                                                                               |
                                                                                                               |
                                                                                                               |
                                                                                                               |
  -70                                                                                                          |
                                                                                                               |
                                                                                                              ||
                                                                                                              ||
                                                                                                              ||
  -75                                                                                                         ||
                                                                                                              ||
                                                                                                              ||
                                                                                                              ||
                                                                                                              ||
  -80                                                                                                         ||
                                                                                                              ||
                                                                                                              |||
                                                                                                              |||
                                                                                                              |||
  -85                                                                                                         |||
                                                                                                              |||
                                                                                                              |||
                                                                                                              |||
                                                                                                              |||
  -90                                                                                                         |||
                                                                                                              |||
                                                                                                              |||
                                                                                                              |||
                                                                                                              |||
  -95                                                                                                         |||
                                                                                                              |||
                                                                                                           .  |||
                                                                                                           |  |||
                                                                                                           |  ||| |
  -100                                                                                 :                 | | !||| ||                 :
                                     .                                                 |                 | | |||| ||                 |                                                    |
                                     |                                    |            |                 |:| ||||.||  !              |                  .:  |              :              |
                               | .   |   :       !:               . |    !|           ||..|        !     |||!|||||||.:|  .|    :|   !|     .           !|| ||              |  .: .        |.
                               | |  ||  .|       ||      .      : | |    ||!  .  .    ||||| !      | :.  ||||||||||||||  |||   ||   ||    :|     !   .:||| ||              |  || |  .|   |||
  -105             .       :   |!|  ||  || ||    ||     !|      | | |    ||| :|  |    ||||| |      | || !||||||||||||||  |||   ||   ||    ||    :|   ||||| ||  :           |  || |!!||   |||    .               .
                   |       |   |||  ||  || ||   !||  |! ||     .|!| |    ||| ||  |    |||||!|| !   | || |||||||||||||||  |||!  || |:||:   ||    ||   ||||| ||  |        :! |  || |||||   |||    |!       .     :|
                   |     ! |:  |||  ||. || ||  ||||  || ||.   !||||||  . ||| ||! |.   |||||||| |   |:|| |||||||||||||||  ||||  || |||||  .||    ||   ||||| ||| |  :     || |  || |||||   |||    ||    : :|     ||
                !  |     | || ||||  |||||| ||!.||||  || |||   ||||||| !| ||| ||| ||| !|||||||| | ! ||||!||||||||||||||| :||||  || |||||| |||!  |||   ||||||||| |  |:    ||!| :|| |||||:  |||    ||   || ||.    ||
                |. | !|  | || ||||  |||||| ||||||||  || |||   ||||||| |||||| ||| ||| ||||||||| | | |||||||||||||||||||| |||||  || |||||| ||||: |||:! ||||||||| |  ||    |||| |||.||||||  |||    ||   ||.|||..| ||
  -110      :   || | ||  | || ||||  |||||| ||||||||  || |||   |||||||!|||||| ||| ||| ||||||||| | | |||||||||||||||||||| |||||  || |||||| ||||| ||||| ||||||||| | !|| :  |||||||||||||||  |||   !||   ||||||||| ||
   dBfs                 2549.2MHz            2549.4MHz           2549.6MHz            2549.8MHz            2550MHz            2550.2MHz            2550.4MHz           2550.6MHz            2550.8MHz             

Once you are done, close your terminals and terminate your reservation by pressing the 'Terminate' button.

OFDM Transmission
+++++++++++++++++

Different software is available for the virtual machines, in the form of different disk images. Experimenters can select different image types
by changing the img_type field in the RSpec.

The following are valid field for the 'img_type' field:

* *plain*: This image comes only with the UHD driver installed. This is ideal for experienced experimeters that wish to develop their applications without resorting to any particular SDR framework.
* *iris*: This image will come with the UHD driver plus the IRIS SDR Framework installed. This is ideal for experimeters that wish to use the IRIS SDR Framework to develop the radio applications. 
* *gnuradio*: This image will come with the GNURadio SDR Framework installed. This is ideal for experimenters that whish to use the GNURadio SDR framework

In this section we will show how to use these different types image types by changing the 'img_type' field in the RSPec. We will use a GNURadio image to transmit an OFDM waveform using GNURadio examples.

Include the following RSpec in the RSpec editor and press run::

    <?xml version='1.0'?>
    <rspec xmlns="http://www.geni.net/resources/rspec/3" type="request" generated_by="jFed RSpec Editor" generated="2016-11-12T13:31:18.181Z" xmlns:emulab="http://www.protogeni.net/resources/rspec/ext/emulab/1" xmlns:jfedBonfire="http://jfed.iminds.be/rspec/ext/jfed-bonfire/1" xmlns:delay="http://www.protogeni.net/resources/rspec/ext/delay/1" xmlns:jfed-command="http://jfed.iminds.be/rspec/ext/jfed-command/1" xmlns:client="http://www.protogeni.net/resources/rspec/ext/client/1" xmlns:jfed-ssh-keys="http://jfed.iminds.be/rspec/ext/jfed-ssh-keys/1" xmlns:jfed="http://jfed.iminds.be/rspec/ext/jfed/1" xmlns:sharedvlan="http://www.protogeni.net/resources/rspec/ext/shared-vlan/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.geni.net/resources/rspec/3 http://www.geni.net/resources/rspec/3/request.xsd ">
        <node client_id="tx_node" exclusive="true" component_manager_id="urn:publicid:IDN+FAVORITE:FAVORITE+authority+am">
            <sliver_type name="usrp-vm"/>
            <location xmlns="http://jfed.iminds.be/rspec/ext/jfed/1" x="410.0" y="185.0"/>
            <img_type name="gnuradio"/>
        </node>
        <node client_id="rx_node" exclusive="true" component_manager_id="urn:publicid:IDN+FAVORITE:FAVORITE+authority+am">
            <sliver_type name="usrp-vm"/>
            <location xmlns="http://jfed.iminds.be/rspec/ext/jfed/1" x="10.0" y="185.00000000000003"/>
            <img_type name="gnuradio"/>
       </node>
       <node client_id="rx_dft" exclusive="true" component_manager_id="urn:publicid:IDN+FAVORITE:FAVORITE+authority+am">
            <sliver_type name="usrp-vm"/>
            <location xmlns="http://jfed.iminds.be/rspec/ext/jfed/1" x="200.0" y="185.00000000000003"/>
            <img_type name="plain"/>
       </node>
       <jfed-command:experimentBarrierSegment orderNumber="0" tag="Barrier segment 0"/>
    </rspec>

Again, insert your project, slice name and reservation time. Wait for the machines turn green and ssh into them.
This RSpec will request 3 nodes: an tx_node, an rx_node and an rx_dft node.

Start the receiver in the rx_node by doing::

    cd /usr/local/share/gnuradio/examples/digital/ofdm
    ./benchmark_rx.py --rx-freq=2.55G --bandwidth 2M

On the 'tx_node' run the following code to start the transmitter::

    cd /usr/local/share/gnuradio/examples/digital/ofdm
    ./benchmark_tx.py --tx-freq 2.55G --bandwidth 2M --tx-gain 15


Finally, start the UHD ASCII DFT example to see the spectrum of the transmitted signal::

    cd /usr/lib/uhd/examples
    ./rx_ascii_art_dft --freq 2550000000 --rate 4000000 --ref-lvl -45 --gain 20

You should see something like the spectrum shown below.

::

  -70

                                                                                                               :
                                                                                                               |   .
                                                                                                               |  :|
  -75                                                                                         .    !    |    ! |  ||
                                                                                             || !  |   ||   :| |  ||            !
                                                                                            ||| |!.| ! ||   || |  || !  .: .   !|
                                                                                            ||| |||| | ||   || |  || |  || |   ||
                                                                                            ||| ||||.| || . || | !|| |  || |   ||
  -80                                                                                       |||!|||||| ||.| ||:| ||| |  || | | ||
                                                                                            |||||||||| ||||:|||| ||| | :|| |.|!||
                                                                                            ||||||||||!||||||||| ||| | ||| ||||||
                                                                                           .||||||||||||||||||||:||||||||||||||||!
                                                                                           |||||||||||||||||||||||||||||||||||||||
  -85                                                                                      |||||||||||||||||||||||||||||||||||||||
                                                                                           |||||||||||||||||||||||||||||||||||||||
                                                                                           |||||||||||||||||||||||||||||||||||||||
                                                                                           |||||||||||||||||||||||||||||||||||||||
                                                                                           |||||||||||||||||||||||||||||||||||||||
  -90                                                                                      |||||||||||||||||||||||||||||||||||||||
                                                                                           |||||||||||||||||||||||||||||||||||||||
                                                                                           ||||||||||||||||||||||||||||||||||||||| :
                                                                                           ||||||||||||||||||||||||||||||||||||||| |
                                                                                           ||||||||||||||||||||||||||||||||||||||| |
  -95                                                                                      |||||||||||||||||||||||||||||||||||||||!|
                                                                                           |||||||||||||||||||||||||||||||||||||||||
                                                                                           |||||||||||||||||||||||||||||||||||||||||
                                                                     .                     |||||||||||||||||||||||||||||||||||||||||
                                                                 !   |:           :   |.   |||||||||||||||||||||||||||||||||||||||||         |
  -100                                                           |   ||  :       ||   ||!  |||||||||||||||||||||||||||||||||||||||||         | .:         .
                                                                 |   ||  |! .    |||  ||| !|||||||||||||||||||||||||||||||||||||||||         | ||    !    |
                                                                 |   || !||||    |||  ||| ||||||||||||||||||||||||||||||||||||||||||         | ||    |    |     !                |
                                                  .          .   |   ||:|||||    |||. |||!||||||||||||||||||||||||||||||||||||||||||.        |!||  . |    | ||  |                |
                                       :  :       |          |   |   |||||||| .  |||| |||||||||||||||||||||||||||||||||||||||||||||||  |     |||||:| |    | || .| .              |:
  -105                                 |! |     . |      |   |  !| ! ||||||||:|. |||| |||||||||||||||||||||||||||||||||||||||||||||||! |     ||||||| |    |||| ||!|        :     ||
   dBfs                       2548.5MHz                 2549MHz                  2549.5MHz                 2550MHz                  2550.5MHz                 2551MHz                  2551.5MHz

Bounded Rspec
+++++++++++++

The previous RSpecs were unbounded, meaning the experimenter will get whichever USRP as free at the moment. Sometimes, a experimenter might require
a bounded RSpec, where the desired USRP is explicitly mentioned. To do this it is necessary to add the 'node_id' field to the RSpec.

This is illustrated in the RSpec example below, where two IRIS nodes are requested, node 11 and 44::

    <?xml version='1.0'?>
    <rspec xmlns="http://www.geni.net/resources/rspec/3" type="request" generated_by="jFed RSpec Editor" generated="2016-11-12T13:31:18.181Z" xmlns:emulab="http://www.protogeni.net/resources/rspec/ext/emulab/1" xmlns:jfedBonfire="http://jfed.iminds.be/rspec/ext/jfed-bonfire/1" xmlns:delay="http://www.protogeni.net/resources/rspec/ext/delay/1" xmlns:jfed-command="http://jfed.iminds.be/rspec/ext/jfed-command/1" xmlns:client="http://www.protogeni.net/resources/rspec/ext/client/1" xmlns:jfed-ssh-keys="http://jfed.iminds.be/rspec/ext/jfed-ssh-keys/1" xmlns:jfed="http://jfed.iminds.be/rspec/ext/jfed/1" xmlns:sharedvlan="http://www.protogeni.net/resources/rspec/ext/shared-vlan/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.geni.net/resources/rspec/3 http://www.geni.net/resources/rspec/3/request.xsd ">
        <node client_id="tx_node" exclusive="true" component_id="urn:publicid:IDN+FAVORITE+usrpvm+21" component_manager_id="urn:publicid:IDN+FAVORITE:FAVORITE+authority+am">
            <sliver_type name="usrp-vm"/>
            <location xmlns="http://jfed.iminds.be/rspec/ext/jfed/1" x="410.0" y="185.0"/>
            <img_type name="gnuradio"/>
        </node>
        <node client_id="rx_node" exclusive="true" component_id="urn:publicid:IDN+FAVORITE+usrpvm+22" component_manager_id="urn:publicid:IDN+FAVORITE:FAVORITE+authority+am">
            <sliver_type name="usrp-vm"/>
            <location xmlns="http://jfed.iminds.be/rspec/ext/jfed/1" x="10.0" y="185.0"/>
            <img_type name="gnuradio"/>
        </node>
        <jfed-command:experimentBarrierSegment orderNumber="0" tag="Barrier segment 0"/>
    </rspec>

