Usage Documentation
===================

In this section, we provide some instructions on how to get the Coordinator,
Aggregate Manager and Cleaner up and running.
This includes how to install dependencies, how to start the servers and how to
make this documentation.

Dependencies
############
.. warning::
    This documentation is based on Ubuntu 14.04 LTS.

To install the required dependencies do the following::

    sudo apt-get install python-m2crypto python-dateutil python-openssl libxmlsec1 xmlsec1 libxmlsec1-openssl libxmlsec1-dev git python-pandas python-mysqldb python-pip

The coordinator also needs the instalation of the Zabbix server. For that,
follow the instructions on:

https://www.zabbix.com/documentation/3.0/manual/installation/install_from_packages

Zabbix API python wrapper::

    sudo pip install py-zabbix

Make sure that the Zabbix Server is running and has a '' user/alias with the password used in the coordinator code.

.. note::

    If there is a need to reset the  password, it can be done over the
    MySQL console::

        mysql -uroot zabbix -p

        mysql> update zabbix.users set passwd=md5('<NEW-PASWORD>') where alias='';

After setting up the Zabbix Server, its time to setup the Database that will acoomodate all reservations::

    mysql -uroot -p<password>

    #On MySQL shell
    mysql> create database USERS character set utf8 collate utf8_bin;
    mysql> grant all privileges on USERS.* to users@localhost identified by 'userpass';
    mysql> quit;

    #Back to Linux shell
    mysql -u root USERS -p < USERS.sql #This file is located at the resources folder.

Configuration
#############

.. warning::

    STILL NEED TO SEPARATE THE GCF (GENI-TOOLS) CODE FROM OURS!
    MAIN FILES MODIFIED:

    * geni-tools/src/gcf/geni/am/am3.py
    * geni-tools/src/gcf-am.py
    * genit-tools/src/gcf/geni/am/resource.py


Copy the reference code, the wall2 certificate and AM self signed certificate in /opt::

    cd /opt

.. note:

    Need to import a file that is not on git (user_resources.py), since it
    contains usernames and passwords, and place it at the code folder.

    Also need to add keyAndCert.pem file to the resources folder (CBTM Certificates).

IMPORTANT: Some files contains passwords to access the databases.
It is very important to restrict access to the /opt/ folder for users that
belong to the group "reservation". You can do this by doing the following::

    sudo addgroup reservation
    sudo setfacl -R -m g:reservation:--- /opt//

.. note::
    New certificates are stored in /opt//code/geni-tools-delegate and I don't won't
    to commit them. There might be the risk to override them. I have a local copy on my PC.

.. note::
    Certificates for the AM and CH can be generated using the gen-certs.py code
    on code/geni-tools/src



Environmental variables
***********************

Add the following two lines in /etc/environment (it will make your default $PATH
and $PYTHONPATH the correct one to work with the AM)::

    PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt//code/geni-tools/src:/opt//code/geni-tools/src/gcf"

    PYTHONPATH="/opt//code/geni-tools/src/gcf_tcd_plugin"


Starting the Services
#####################

Coordinator (coordinator.py)
****************************
The coordinator is the central entity of this system. It is a TCD server
that processes all the requests from the AM and the webserver and it is in charge to:

    1) create/delete/update users on the gateway
    2) create/delete reservations for USRPs (TCD+Fed4FIRE) and Simulations (TCD only)
    3) Add hosts and users to the Zabbix monitoring system.

To start it (as root)::

    cd /opt/
    sudo python coordinator.py


Aggregate Manager (gcf-am.py)
*****************************
To start the Aggregate Manager do the following::

    cd /opt//code/geni-tools
    sudo python gcf-am.py -V 3 -c /opt//code/geni-tools-delegate/gcf_config

.. note::
    If need to start the clearinghouse (gcf-ch.py)::

        sudo python gcf-ch.py -c /opt//code/geni-tools-delegate/gcf_config

Now you can also start the cleaner::

    cd /opt/
    sudo python cleaner.py

Making the Documentation
########################

To make this documentation, do the following::

    cd AGGREGATE_MANAGER_DIR/docs
    sphinx-apidoc -f -H "API Documentation" -o ./_autodocs/ ../code/
    make html
