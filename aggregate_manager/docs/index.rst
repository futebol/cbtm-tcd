.. Aggregate Manager documentation master file, created by
   sphinx-quickstart on Thu Jul 14 21:51:25 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Aggregate Manager's documentation!
=============================================

.. toctree::
   :maxdepth: 2

   design_doc
   usage_doc
   experiment_doc
   _autodocs/modules
