# README #

This repository contains the Aggregate Manager, the PHP and the Coordinator code that enables access to the testbed. The Aggregate Manager is the process responsible for enabling external access, by communicating with jFed. The PHP code is responsible for internal user management. The coordinator is the entity that coordinates the requests from both these entities and communicates with the CBTM on behalf of the users.  

## Dependencies ##

To install the Aggregate Manager, first install the following dependencies:

```
#!shell
sudo apt-get install python-m2crypto python-dateutil python-openssl libxmlsec1 xmlsec1 libxmlsec1-openssl libxmlsec1-dev git python-pandas python-mysqldb python-pip
```

You will also need to install Zabbix, a network monitoring server. Zabbix will depend on MySql, PHP and Apache.

### Creating Aggregate Manager Certificates for Testbed ###
edit the certificate config file with site specific information

vi geni-tools-delegate/gcf_config

copy the file to /home/<user>/.gcf/gcf_config

cp /home/<user>/aggregate_manager/code/geni-tools-delegate/gcf_config /home/<user>/.gcf/gcf_config

then run the following to create the necessary ceritificates

sudo python ~/code/geni-tools/src/gen-certs.py

you should get a message similar to the following: 
Created CH cert/keys in aggregate_manager/code/geni-tools-delegate/certs/ch-cert.pem, aggregate_manager/code/geni-tools-delegate/certs/ch-key.pem, and in aggregate_manager/code/geni-tools-delegate/certs/trusted_roots/ch-cert.pem
Created AM cert/keys in aggregate_manager/code/geni-tools-delegate/certs/am-cert.pem and aggregate_manager/code/geni-tools-delegate/certs/am-key.pem
Created Experimenter alice certificate in aggregate_manager/code/geni-tools-delegate/certs/alice-cert.pem
Created Experimenter alice key in aggregate_manager/code/geni-tools-delegate/certs/alice-key.pem

### Start Aggregate Manager ###
$ ./run-am.sh 

### Start Aggregate Manager - Possible errors ###
The following error is due to the coordinator process not being available

INFO:cred-verifier:Will accept credentials signed by any of 3 root certs found in aggregate_manager/code/geni-tools-delegate/certs/trusted_roots: ['aggregate_manager/code/geni-tools-delegate/certs/trusted_roots/wall2.pem', 'aggregate_manager/code/geni-tools-delegate/certs/trusted_roots/ch-cert.pem', 'aggregate_manager/code/geni-tools-delegate/certs/trusted_roots/ca.pem']
INFO:cred-verifier:Will accept credentials signed by any of 3 root certs found in aggregate_manager/code/geni-tools-delegate/certs/trusted_roots: ['aggregate_manager/code/geni-tools-delegate/certs/trusted_roots/wall2.pem', 'aggregate_manager/code/geni-tools-delegate/certs/trusted_roots/ch-cert.pem', 'aggregate_manager/code/geni-tools-delegate/certs/trusted_roots/ca.pem']
INFO:gcf.am3:Running gcf AM v3 code version 2.10
aggregate_manager/code/geni-tools-delegate/certs/am-cert.pem
['URI:urn:publicid:IDN+FAVORITE+authority+am']
INFO:geni-delegate:Will run am with urn:publicid:IDN+FAVORITE+authority+am as component_manager_id
urn_authorityFAVORITE
INFO:geni-delegate:Querying the coordinator for the list of resources.
INFO:geni-delegate:queryCoordinator.....
Traceback (most recent call last):
  File "./gcf-am.py", line 217, in <module>
    sys.exit(main())
  File "./gcf-am.py", line 168, in main
    **vars(opts)
  File "aggregate_manager/code/geni-tools/src/gcf/geni/auth/util.py", line 55, in getInstanceFromClassname
    object_instance = class_instance(*argv,**kwargs)
  File "aggregate_manager/code/geni-tools/src/gcf_tcd_plugin/testbed.py", line 156, in __init__
    self.updateResourceList()
  File "aggregate_manager/code/geni-tools/src/gcf_tcd_plugin/testbed.py", line 182, in updateResourceList
    n210msg = GatewayQuery(host,port,'AM::listresource')
  File "aggregate_manager/code/geni-tools/src/gcf_tcd_plugin/testbed.py", line 95, in GatewayQuery
    sock.connect((host, port))
  File "/usr/lib/python2.7/socket.py", line 228, in meth
    return getattr(self._sock,name)(*args)
socket.error: [Errno 111] Connection refused
### Installing PHP ###
TODO
### Installing Apache ###
TODO

### Installing Zabbix ###

The following instructions were tested successfully to install Zabbix 2.0 on Ubuntu 12.04.

```
#!shell
wget http://repo.zabbix.com/zabbix/2.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_2.0-1precise_all.deb
sudo dpkg -i zabbix-release_2.0-1precise_all.deb
sudo apt-get update
sudo apt-get install zabbix-server-mysql zabbix-frontend-php
```

After installing Zabbix, you will need to go to http://127.0.0.1/zabbix and complete the installation process.
Sometimes the PHP timezone is not set. You will need to go to edit the 'php.ini' file in '/etc/php5/apache2' to include the following line 'date.timezone = Europe/Dublin'
After this, restart apache by doing:

```
#!shell
sudo service apache2 restart
```

You should now be able to login into Zabbix.

Finally, you will need to install the Zabbix API python wrapper:

```
#!shell
pip install py-zabbix
```

After installing the python bindings, you can test if they are working properly by doing:

```
#!python
from pyzabbix import ZabbixAPI
zapi = ZabbixAPI(url='http://127.0.0.1/zabbix/', user='your_user', password='your_pass')
```

## Installing the Aggregate Manager ##

Copy the reference code, the wall2 certificate and AM self signed certificate in /opt


```
#!shell
cd /opt
git clone https://your_user@bitbucket.org/tcd_testbed/aggregate_manager.git
git checkout devel
```


IMPORTANT: some files contains passwords to access the databases. It is very important to restrict access to the /opt/f4fam folder for users that belong to the group "reservation". You can do this by doing the following:


```
#!shell

sudo addgroup reservation
sudo setfacl -R -m g:reservation:--- /opt/f4fam/
```



* Environmental variables

Add the following two lines in /etc/environment (it will make your default $PATH and $PYTHONPATH the correct one to work with the AM):


```
#!shell
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/opt/f4fam/code/geni-tools/src:/opt/f4fam/code/geni-tools/src/gcf"

PYTHONPATH="/opt/f4fam/code/geni-tools/src/gcf_tcd_plugin"
```
* Coordinator (coordinator.py):

The coordinator is the central entity of this system. It is a TCD server
that processes all the requests from the AM and the webserver and it is in charge to 1) create/delete/update users on the gateway 2) create/delete reservations for USRPs (TCD+Fed4FIRE) and Simulations (TCD only) 3) Add hosts and users to the Zabbix monitoring system.

to start the Coordinator (as root)

```
#!shell

cd /opt/f4fam
python coordinator.py
```

* How to start the AM
(the new certificates are stored in /opt/geni-tools-delegate and I don't won't to commit them. There might be the risk to override them. I have a local copy on my PC.)

```
#!shell

cd /opt/f4fam/geni-tools
gcf-am.py -V 3 -c /opt/geni-tools-delegate/gcf_config

```

Now you can also start the cleaner:

```
#!shell
cd /opt/f4fam
python cleaner.py
```

## Testing the Aggregate Manager with jFed ##

TODO
