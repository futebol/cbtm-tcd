# CBTM's README #

This repository contains the files for getting the CBTM up and running. The CBTM
is the entitie responsible for receiving commands from a socket and creating/deleting/monitoring virtual machines. Setting up the CBTM requires first intalling libvirt and then starting the CBTM server.

You can get the code for the CBTM by doing

```
#!shell
git clone https://<your_user>@bitbucket.org/<your_project>/cbtm.git
```

To install and run the CBTM, check the instructions in the documentation.

To make the documentation, run:

To make this documentation, do the following

```
#!shell
cd CBTM_DIR/docs
sphinx-apidoc -f -H "API Documentation" -o ./_autodocs/ ../code/
make html
```

The documentation will be located in ./docs/_build/html/ and you can open it with a normal web-browser.


