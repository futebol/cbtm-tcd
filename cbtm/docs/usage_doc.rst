Usage Documentation
===================

In this section, we provide some instructions on how to get the CBTM up and running.
This includes how to install dependencies, how to start the CBTM server and how to make this documentation.

Setting up the CBTM requires first installing libvirt and then starting the CBTM server.

.. warning::

        Please keep the code for your experiments separate from the CBMT code! This will make the maintenance of both the CBTM and your experiment easier.



Installing libvirt
******************

To setup the CBTM you will need to first install libvirt on both the machine that will run the CBTM server and on the hosts that create the virtual machines.
These instructions are for Ubuntu 16.04. For older versions of Ubuntu, you will have to install from sorce.

Dependencies
############

To install the required dependencies do the following::

    sudo apt-get install gcc make pkg-config libxml2-dev libgnutls-dev libdevmapper-dev libcurl4-gnutls-dev python-dev libpciaccess-dev libxen-dev libnl-dev libyajl-dev uuid-dev qemu-kvm bridge-utils dnsmasq qemu-system

Installing
##########

On the host machines and the machine that will run the CBTM do the following to install libvirt::

    sudo apt-get install libvirt-bin libvirt-dev libvirt0

Install libvirt's python bindings
*********************************

After libvirt is properly installed, you will need to install the libvirt python bindings on the CBTM machine, *not* on the host machines.
The host machines, will not be running python code, so they do not need this.

::

  sudo apt-get install python-libvirt


Installing virt-manager, virt-install and virt-viewer
*****************************************************

On the CBTM machine, once the python bindings are installed, you can install the virt-manager, virt-install and virt-viewer utils.
The virt-manager is a graphical interface that is useful to monitor and manage virtual machines, while virt-install is useful to create the
master images for the virtual machines.

Dependencies
############

To install the virt-manager and the other helpful virt-tools you first need to install the following dependencies::

    sudo apt-get install intltool libtool libltdl-dev libglib2.0-dev libgtk2.0-bin libffi-dev gtk-doc-tools gobject-introspection libgirepository1.0-dev libgtk-3-dev python-gi-dev gir1.2-gtk-3.0 python-libxml2 gir1.2-libosinfo-1.0 python-ipaddr libcanberra-gtk3-module gir1.2-vte-2.90 gir1.2-spice-client-gtk-3.0 gir1.2-gtk-vnc-2.0

You will also need to install some X dependencies to use the graphical interface of the virt manager::

    sudo apt-get install fontconfig-config fonts-dejavu-core libfontconfig1 libice6 libsm6 libxaw7 libxcursor1 libxfixes3 libxft2 libxkbfile1 libxmu6 libxpm4 libxrender1 libxt6 x11-common xbitmaps

To install the virt-manager do::

    sudo apt-get install virt-manager

Preparing the Virtual Machines
******************************

For the CBTM to work properly, it is necessary to prepare the images for the virtual machines, before starting up the CBTM.
This includes creating the images, creating a storage pool at the hosts and copying the  images to this storage pool. When creating a new
virtual machine, the CBTM will clone the an image from one of the the master images.
The user's virtual machine will run on this copy of one of the master images.

At the moment, there are three master images, a plain image, a GNU Radio image and an IRIS image. Below are instructions on how to create these.

Creating a Basic Image
######################

To create the master images, first download the ISO of the desired operating system. For Ubuntu 16.04 server edition, do::

  mkdir master-imgs
  cd master-imgs
  wget http://releases.ubuntu.com/16.04/ubuntu-16.04-server-amd64.iso

Then create the raw image::

  qemu-img create -f raw master-plain.img 20g

Then, use virt-install to install Ubuntu 16.04 onto the image.

::

  sudo virt-install --connect qemu:///system --name img-creator --vcpus 8 --memory 8000 --network none --disk path=master-plain.img --virt-type kvm --graphics vnc --network network=default --cdrom ubuntu-16.04-server-amd64.iso

Go to through the installation process. Give the machine the "experiment-unit" hostname and create a user named "cbtmadmin" and give it an appropriate password.
Make sure you install OpenSSH server alongside with the basic system utilities.

You will also need to install python in the master image to be able to configure the virtual machine using  ansible.

::

  sudo apt-get install python python-apt

After finishing the instalation, copy the CBTM's public key to the new image, by doing::

  virsh domifaddr img-creator
  ssh-copy-id cbtmadmin@<vm-ip>

Finally, to make management easier and allow users to sudo, make passwordless sudo available in the VM::
Using 'visudo' add the following lines::

  #Allow all users passwordless sudo
  ALL            ALL = (ALL) NOPASSWD: ALL

Configuring the Basic Images
############################

Before the images are usable, you will need to configure them with the appropriate software. We will do this using ansible, a tool for automatic IT configuration.
Firstly, install ansible at the frontend and test if it can ping and sudo the new virtual machine::

  sudo apt-get install ansible
  ansible all -i "<vm-ip>", -m ping -u cbtmadmin --sudo

Each image will have an script to detect the MAC address of the interface to the USRP and assign the IP in the following format '10.0.<usrp-id>.1'.
This will be done at start up, so copy the 'change_usrp_ip.sh' script to the '/etc/' folder of the virtual machine and edit the '/etc/rc.local' file to run this script.

This can be done with ansible by doing::

  cd ~/cbtm/utils
  ansible-playbook -i "<vm-ip>", ip-bootscript.yml

After this install the UHD drivers from the Ettus repositories. This can be done by doing::

  ansible-playbook -i "<vm-ip>", uhd-install.yml
  
After this install the OML files::

  ansible-playbook -i "<vm-ip>", install-oml.yml

After this install the zabbix files::

  ansible-playbook -i "<vm-ip>", install-zabbix-agent.yml
  
After this install the omf6 files::

  ansible-playbook -i "<vm-ip>", install-omf6-rc.yml
  
Shutdown your VM, the plain image is configured.

::

  virsh shutdown img-creator

Configuring GNURadio image
##########################

After creating the first plain image, you can clone the original image to make the master image for and GNURadio::

  cd ~/master-imgs/
  virt-clone -o img-creator --name img-creator-gnuradio --file master-gnuradio.img
  virsh start img-creator-gnuradio
  virsh domifaddr img-creator-gnuradio

To install GNURadio do::

  cd ~/cbtm/utils
  ansible-playbook -i "<gnuradio-vm-ip>", gnuradio-install.yml

Don't forget to shutdown the VM after installing GNURadio::

    virsh shutdown img-creator-gnuradio

Configuring IRIS image
######################

After creating the first plain image, you can clone the original image to make the master image for and IRIS::

  cd ~/master-imgs/
  virt-clone -o img-creator --name img-creator-iris --file master-iris.img
  virsh start img-creator-iris
  virsh domifaddr img-creator-iris

To install IRIS do::

  cd ~/cbtm/utils
  ansible-playbook -i "<iris-vm-ip>", iris-install.yml

Don't forget to shutdown the VM after installing IRIS::

  virsh shutdown img-creator-iris

Copy the images to the hosts
############################

Firstly, create a storage pool in each rack. To do this, first make sure that the hosts are properly configured in ansible.
Add the following lines to '/etc/ansible/hosts'::

  [racks]
  <host_ip_1>
  ...
  <host_ip_N>

After this, use the following command to create the pool in the rack::

  ansible-playbook create-storage-pools.yml

Finally, copy the images to the hosts by doing::

  ansible-playbook synchronize-master-plain.yml
  ansible-playbook synchronize-master-gnuradio.yml
  ansible-playbook synchronize-master-iris.yml

Starting a DHCP server
######################

The virtual machines will connect to a DHCP server to get the IP of their outbound interfaces, eth0.

If the DHCP server is not already up and running in the frontend, install the DHCP server by ssh-ing into the frontend machine and doing::

    sudo apt-get install isc-dhcp-server

After installing the server, change the following in the /etc/dhcp/dhcpd.conf to configure the leases.
IPs up to 192.168.5.50 are reserved for physical machines, the rest can be used by virtual machines. You can chose a lease time that is reasonable.

::

    subnet 192.168.5.0 netmask 255.255.255.0 {
    range 192.168.5.50 192.168.5.254;
    option subnet-mask 255.255.255.0;
    option routers 192.168.5.1;
    option domain-name-servers 192.168.5.1;
    default-lease-time 2592000;
    max-lease-time 2592000;
    }

To make sure the DHCP server only looks to the private interface, i.e. that only the hosts and the virtual machines see the DHCP server,
you can change the file /etc/default/isc-dhcp-server to include the following::

    INTERFACES="bond0"

.. warning

    Make sure that is looking only to the interface directed only at the cloud. You can interfere with other nodes in the network if you set the DHCP server incorrectly.

Creating SSL Certificates to use with CBTM
******************************************

The CBTM is protected by SSL certificate authentication. To generate the certificates for usage with teh CBTM server, do the following: ::

    openssl genrsa -out key.pem 2048
    openssl req -new -sha256 -key key.pem -out csr.csr
    openssl req -x509 -sha256 -days 365 -key key.pem -in csr.csr -out cert.pem

After generating the certificate and the key, you must combine the two into one file by doing: ::

    cat key.pem>>KeyAndCert.pem
    cat cert.pem>>KeyAndCert.pem

With this done, copy the 3 files (key.pem, cert.pem and KeyAndCert.pem) to the ``resources`` folder.
Those files are needed to estabilish connections with the CBTM server, for security reasons.
Every client needs to have those files for authentication.

To start the CBTM server
************************

You can get the code for the CBTM by doing::

    git clone https://your_user@bitbucket.org/tcd_testbed/cbtm.git

To get the CBMT up and running, do::

    cd cbtm/code
    ./cbtm.py

Test the CBTM server with the CBTM Client
*****************************************

To test if the CBTM server is working properly, you can start the CBTM client, which was built for testing and debugging purposes. You can start the CBTM client by doing:
::

    cd cbtm/code
    ./cbtm_client.py

After starting the client, you can send the CBTM server commands and look at the response.
Some examples include request unit (ru), grid status (gs), unit status (us) and release (rl), as illustrated below.
::

    ru 22
    gs
    us 22
    rl 22
    gs

Making the Documentation
************************

To make this documentation, do the following::

    cd CBTM_DIR/docs
    sphinx-apidoc -f -H "API Documentation" -o ./_autodocs/ ../code/
    make html

Coding Style
************

We follow mostly the Google Python coding style. The following naming conventions should be used::

    module_name, package_name, ClassName, method_name, ExceptionName, function_name, GLOBAL_CONSTANT_NAME, global_var_name, instance_var_name, function_parameter_name, local_var_name.

Regarding docstrings (TODO)
