Design Documentation
========================

The aim of the CBTM is to provide, instantiate, monitor and delete, Experimentation Units -- *id est* Virtual Machines (VMs) plus Universal Software Radio Peripherals (USRPs) -- to users.
The users can then access the VMs and run their Software Defined Radio (SDR) experiments.

CBTM's Overall structure
************************

The CBTM's structure is illustrated in Figure :ref:`cbtm-overall`. There, we can see three main logical blocks: the CBTM Server, VM Control and Logging.
The CBTM server is a TCP server that listens to messages defined in the Cloud Based Testbed Protocol (CBTP).

.. _cbtm-overall:

.. figure:: _imgs/cbtm_overall.svg

   CBTM overall structure

CBTM's Server
*************

The server is a TCP server developed with the twisted server module.

CBTP: CBTM's Protocol
*********************

The CBTM's protocol was designed to estabilish communication between the CBTM Server and a Client, provinding full functionality over the CBTM's features.

The structure for commands is the following:
::

    COMMAND:ARG1,ARG2,ARG3,...

The following commands are supported:

* request_unit

    Request an Experimental Unit (Virtual Machine) attached to an USRP.

    Usage:
        * request_unit:USRP_Id,Image type,Username,SSH Key

    Args:
        * USRP Id: Grid number of the USRP

        * Image Type: Clean, GnuRadio or Iris

        * Username

        * SSH Key

    Return:
        * Request Status: Accepted or Denied

* grid_status

    Request the status of the USRP grid.

    Args:

    Return:
        * Usrp's in use

* release_unit

    Release a USRP and delete the respective Experimental Unit

    Args:
        * USRP Id: Grid number of the USRP

    Return:
        * Request Response: ok

* unit_status

    Request the status of one USRP.

    Args:
        * USRP Id: Grid number of the USRP

    Return:
        * Unit Status: Free, Wait, Unavailable or the IP of the VM

* shutdown_unit

    Shutdown an Experimental unit

    Args:
        * USRP Id: Grid number of the USRP

    Return:
        * Request Response: ok

* start_unit

    Start an Experimental unit

    Args:
        * USRP Id: Grid number of the USRP

    Return:
        * Request Response: ok

* restart_unit

    Restart an Experimental unit

    Args:
        * USRP Id: Grid number of the USRP

    Return:
        * Request Response: ok

CBTM's Image and Template Control
*********************************

The Template Control module (template_ctrl.py) is responsible for managing the templates of Virtual Machines.
Those are XML files with specifications about the VM's, such as Number of CPU cores, amout of RAM, path to VM image and Rack Interface connected to USRP.

The Image Control (image_ctrl.py) module is responsible for managing the images of the VM's (delete and clone).
The CBTM assumes that there are base images, created manually or using tools (ubuntu-vm-builder or virt-install for example), ready for use.
The images (both base and for use) are stored in Storage Pools managed by the Libvirt Python API.

The storage pools are created using virt-manager GUI.

Our pools are located in each Rack of the Testbed, at ``\home\nodeuser\forge-pool`` and contain 3 base images:
    * Clean
    * Gnuradio
    * Iris

After clonning, the images used by experimenters are not persistent, which means that after the experiments are done, with the release of the Resources,
the images are also deleted.

CBTM's Virtual Machines Control
*******************************

Combined with Image and Template control, this is responsible for creation and management of the Virtual Machines on our Testbed Racks.

This module, wich alredy is a combination of tree other modules (resource_ctrl, virt and cbtm), combines information about the Resources (USRP's), Hypervisors (Racks)
and VM's and provide management using the Libvirt 1.3.4 Python API.
