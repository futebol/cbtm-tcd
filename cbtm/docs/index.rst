.. CBTM documentation master file, created by
   sphinx-quickstart on Wed May  4 10:44:20 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CBTM's documentation!
================================

.. toctree::
   :maxdepth: 2

   design_doc
   usage_doc
   _autodocs/modules
