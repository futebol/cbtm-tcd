#!/usr/bin/env python
''' 
CBTM's resource control module.

This module provides basic information about resources (USRP's).
'''
import threading
import time
import virt

''' 
rack11 or rack13
''' 
SIM_VM_MAPPING = { 
    11: ('rackA11'),
    13: ('rackA13'),
    15: ('rackA15'),
    17: ('rackA17'),
    19: ('rackA19'),
    21: ('rackA21'),
    23: ('rackA23'),
}

N210BEAM_IF_MAPPING = {
    12: ('rackA21', 'em2'),
    43: ('rackA23', 'em4'),
}

X310_IF_MAPPING = {
    11: ('rackA13', 'em2'),
    44: ('rackA15', 'em4'),
}

USRP_IF_MAPPING = {
    13: ('rackA21', 'em3'),
    14: ('rackA21', 'em4'),
    21: ('rackA19', 'em2'),
    22: ('rackA19', 'em3'),
    23: ('rackA19', 'em4'),
    24: ('rackA15', 'em2'),
    31: ('rackA17', 'em2'),
    32: ('rackA17', 'em3'),
    33: ('rackA17', 'em4'),
    34: ('rackA15', 'em3'),
    41: ('rackA23', 'em2'),
    42: ('rackA23', 'em3'),
    51: ('rackA13', 'em3'),
    52: ('rackA13', 'em4'),
    53: ('rackA11', 'em2'),
    54: ('rackA11', 'em3'),
    63: ('rackA35', 'em4'),
    64: ('rackA11','em4'),
}
'''
Dictionary that defines the mapping from the USRP Id (11, 34, etc.) to the correct
rack and interface.
'''

USRP_IF_MAC_MAPPING = {
    11: ('b0:83:fe:da:91:2a'),
    12: ('c8:1f:66:dc:31:14'),
    13: ('c8:1f:66:dc:31:15'),
    14: ('c8:1f:66:dc:31:16'),
    21: ('f8:bc:12:34:f4:b9'),
    22: ('f8:bc:12:34:f4:ba'),
    23: ('f8:bc:12:34:f4:bb'),
    24: ('b0:83:fe:da:92:32'),
    31: ('c8:1f:66:ef:59:58'),
    32: ('c8:1f:66:ef:59:59'),
    33: ('c8:1f:66:ef:59:5a'),
    34: ('b0:83:fe:da:92:33'),
    41: ('c8:1f:66:da:b7:fd'),
    42: ('c8:1f:66:da:b7:fe'),
    43: ('c8:1f:66:da:b7:ff'),
    44: ('b0:83:fe:da:92:34'),
    51: ('b0:83:fe:da:91:2b'),
    52: ('b0:83:fe:da:91:2c'),
    53: ('14:18:77:5d:6e:76'),
    54: ('14:18:77:5d:6e:77'),
    63: ('c8:1f:66:dc:2b:66'),
    64: ('14:18:77:5d:6e:78'),
}
''' 
Dictionary that defines the mapping from the USRP Id (11, 34, etc.) to the correct
MAC address of the Rack's interface.
'''

USRP_VM_NAME_MAPPING = {
#    11: ('basic_eu_11'),
#    12: ('basic_eu_12'),
    13: ('basic_eu_13'),
    14: ('basic_eu_14'),
    21: ('basic_eu_21'),
    22: ('basic_eu_22'),
    23: ('basic_eu_23'),
    24: ('basic_eu_24'),
    31: ('basic_eu_31'),
    32: ('basic_eu_32'),
    33: ('basic_eu_33'),
    34: ('basic_eu_34'),
    41: ('basic_eu_41'),
    42: ('basic_eu_42'),
    51: ('basic_eu_51'),
    52: ('basic_eu_52'),
    53: ('basic_eu_53'),
    54: ('basic_eu_54'),
    63: ('basic_eu_63'),
    64: ('basic_eu_64'),
#    43: ('basic_eu_43'),
#    44: ('basic_eu_44'),
}
''' 
Dictionary that defines the mapping from the USRP Id (11, 34, etc.) to the correct
Virtual Machine name on the Rack.
'''

def lookup_n210beam_if(usrp_num):
    ''' 
    Get (host, interface) tupple from the USRP Id

    Args:
        usrp_id (int): USRP Id
    Returns:
        (host, iter) (string): Host Name, Host Interface
    '''
    return N210BEAM_IF_MAPPING[int(usrp_num)]

def lookup_x310_if(usrp_num):
    ''' 
    Get (host, interface) tupple from the USRP Id

    Args:
        usrp_id (int): USRP Id
    Returns:
        (host, iter) (string): Host Name, Host Interface
    '''
    return X310_IF_MAPPING[int(usrp_num)]

def lookup_host_if(usrp_num):
    ''' 
    Get (host, interface) tupple from the USRP Id

    Args:
        usrp_id (int): USRP Id
    Returns:
        (host, iter) (string): Host Name, Host Interface
    '''
    return USRP_IF_MAPPING[int(usrp_num)]

def lookup_x310_mapping(usrp_num):
    ''' 
    Get (host, cores, memory) 

    Args:
        cores_memory (int): Num of cores and memory
    Returns:
        (host, cores, memory) (string): Host Name, Host Interface
    '''
    return X310_IF_MAPPING[int(usrp_num)]

def lookup_n210beam_mapping(usrp_num):
    ''' 
    Get (host, interface) tupple from the USRP Id

    Args:
        usrp_id (int): USRP Id
    Returns:
        (host, iter) (string): Host Name, Host Interface
    '''
    return N210BEAM_IF_MAPPING[int(usrp_num)]

def lookup_vm_sim_mapping(resources):
    ''' 
    Get (host, interface) tupple from the USRP Id

    Args:
        usrp_id (int): USRP Id
    Returns:
        (host, iter) (string): Host Name, Host Interface
    '''
    return SIM_VM_MAPPING[int(resources)]

def lookup_usrp_if_mac(usrp_num):
    ''' 
    Get the mac address of the interface connected to the USRP from it's Id.

    Args:
        usrp_id (int): USRP Id
    Returns:
        mac_addr (string): Mac Address of the interface
    '''
    return USRP_IF_MAC_MAPPING[int(usrp_num)]

def lookup_usrp_id(host, inter):
    ''' 
    Get USRP Id from the (host, interface) tupple.

    Args:
        host (string): Host Name
        inter (string): Host Interface
    Returns:
        usrp_id (int): USRP Id
    '''
    for key, value in USRP_IF_MAPPING.items():
        if host == value[0] and inter == value[1]:
            return str(key)

def list_vms_deployed(conn_list, domain_name):
    ''' 
    list the vms deployed

    Args:
        conn_list (ConnInfo): Dictionary of Libvirt connections to the Racks
    Returns:
        host: return the name of the rack server hosting the vm domain name
    '''
    for host, conn in conn_list.items():
        vm_names=conn.get_all_vms()
        if vm_names == []:
            continue #No VMs defined in this rack. Move to next Rack
        for vm in vm_names:
            #Check if VM is the same as requested domain, otherwise skip it
            if domain_name in vm:
                print("domain_name "+domain_name + " matches vm_name: " + vm + " return host"+ host)
                return host

    return "null"

def check_grid_resources(conn_list):
    ''' 
    Check the availability of the resources (USRP's)

    Args:
        conn_list (ConnInfo): Dictionary of Libvirt connections to the Racks
    Returns:
        usrps (list): list of USRP's in use and the IP of the respective VM. If
        the VM is turned off, the ip returned is 0.0.0.0
    '''
    usrps = UsrpList()
    for host, conn in conn_list.items():

        vm_names=conn.get_all_vms()
        if vm_names == []:
            continue #No VMs defined in this rack. Move to next Rack

        for vm in vm_names:
            #Check if VM is an USRP VM, otherwise skip it
            if "basic_eu_" not in vm:
                continue
            #For each VM, get interfaces IPs
            (ext_ip,usrp_if) = conn.get_vm_if_info(vm)

            #get USRP id from the (rack, interface) tupple
            usrp_id = vm[9:]
            usrps.use[usrp_id] = True
            usrps.ips[usrp_id] = ext_ip
            try:
                dom = conn.conn.lookupByName(vm)
                if dom.isActive():
                    usrps.booted[usrp_id] = True
            except:
                raise


    #Return list with USRPs.
    return usrps

def check_usrp_status(conn_list,usrp_num):
    ''' 
    Checks the status of the USRP: defined, booted and IP.
    '''
    (host, iface) = lookup_host_if(usrp_num)
    try:
        dom = conn_list[host].conn.lookupByName("basic_eu_"+str(usrp_num))
        use = True
    except:
        #self.log.warning("Cloud not get find the USRP "+str(usrp_num))
        use = False
        booted = False
        ip = '0.0.0.0'

    if use:
        booted = dom.isActive()
        if booted:
            ip, usrp_if = conn_list[host].get_vm_if_info("basic_eu_"+str(usrp_num))
        else:
            ip = '0.0.0.0'

    return use, booted, ip

def get_vm(conn_list, usrp_num):
    ''' 
    Get VM name from an USRP Id, if the USRP is attached to a VM.

    Args:
        conn_list (ConnInfo): Dictionary of Libvirt connections to the Racks
        usrp_num (int): USRP Id
    Returns:
        vm_name (string): if such VM exists, -1 if the VM is not found
    '''
    (host, iface) = lookup_host_if(usrp_num)

    conn = conn_list[host]

    vm_names=conn.get_all_vms()
    result = -1
    if vm_names != []:
        for vm in vm_names:
            (ext_ip,usrp_if) = conn.get_vm_if_info(vm)
            if usrp_if == iface:
                result = vm
    else:
        result = -1

    return result

def get_vol(conn_list, usrp_num):
    ''' 
    Get Volume name from an USRP Id, if the volume exists.

    Args:
        conn_list (ConnInfo): Dictionary of Libvirt connections to the Racks
        usrp_num (int): USRP Id
    Returns:
        vol_name (string): Volume name if volume exists, -1 if it doesn't.
    '''
    (host, iface) = lookup_host_if(usrp_num)
    conn = conn_list[host]
    img_name = USRP_VM_NAME_MAPPING[int(usrp_num)]+'.qcow2'

    vol_names=conn.get_all_vols()
    if img_name in vol_names:
    	return img_name
    else:
        return -1



class UsrpList(object):
    ''' 
    Class that holds a list of USRP parameters. This class

    Attributes:
        use (dict): Dictionary that holds an Bool for each USRP.
            These bools are used to check if an USRP is in use or not.
        ips (dict): A dictionary class that holds the VM's IPs
            of each USRP.
    '''
    def __init__(self):
        ''' 
        Intialization of the usrp_list class. This includes creating a dictionary
        for the USRPs in use, and a locked dictionary to hold the IPs.
        '''
        usrps = [str(key) for key in USRP_IF_MAPPING.keys()]

        self.use = {}

        for key in usrps:
            self.use[key] = False

        self.booted = {}
        for key in usrps:
            self.booted[key] = False

        self.ips = {}
        for key in usrps:
            self.ips[key] = '0.0.0.0'


    def usrp_print(self):
        ''' 
        Print's the information about the USRPs to standard output.
        Useful for debug.
        '''
        usrp_str = 'USRP ID  DEFINED  BOOT    IP'
        for usrp in self.use.keys():
            usrp_str=usrp_str+'\n    '+usrp+'    '+str(self.use[usrp])+'    '+str(self.booted[usrp])+'    '+self.ips[usrp]
        return usrp_str


if __name__ == '__main__':
    conn_list = virt.open_connections()
    usrps = check_grid_resources(conn_list)
    print usrps.usrp_print()
