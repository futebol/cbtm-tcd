#!/usr/bin/env python
''' 
    This module is responsible for handling  client requests,
    translating them to the CBTP format.
'''

import resource_ctrl
import threading
import Queue
import logging
import random
import virt

class Handler(object):
    def __init__(self, conn_list, maker,maker_sim, destroyer, vmsimdestroyer, starter, stopper, restarter, cleaner,start_vm_unit,restart_vm_unit,stop_vm_unit):
        """ 
        Init of the handling class
        Args: TODO
        """
        self._log = logging.getLogger("cbtm_logger")
        self._log.debug("Initializing Hanlder")

        self._conn_list=conn_list

        self._make_usrp_vm = maker
        self._make_simulation_vm = maker_sim
        self._destroyer = destroyer
        self._vm_sim_destroyer = vmsimdestroyer
        self._start_vm = starter
        self._stop_vm = stopper
        self._restart_vm = restarter
        self._make_clean = cleaner
        self._start_sim_unit = start_vm_unit
        self._restart_sim_unit = restart_vm_unit
        self._stop_sim_unit = stop_vm_unit
        self._make_threads = {}
        self._break_flags = {}
        self._del_threads = {}

        self.accepted_commands = ['request_unit',
                                'request_simulation_vm',
                                'grid_status',
                                'release_unit',
                                'release_sim_unit',
                                'unit_status',
                                'sim_unit_status',
                                'start_sim_unit',
                                'stop_sim_unit',
                                'restart_sim_unit',
                                'shutdown_unit',
                                'start_unit',
                                'restart_unit',
                                'make_clean']

    def handle(self, command, args):
        #self.update()
        self._log.debug("Received command: "+command)
        msg = 'Bad command: "%s"'%command
        if command in self.accepted_commands:
            if len(args) > 0:
                try:
                    msg = getattr(self, command)(*args)
                except TypeError:
                    pass

        return msg

    def request_unit(self, args):
        self._log.info("Unit Requested: "+args)
        try:
            usrp_num,img_type,user,ssh_key,usrp_type = args.split(',')
        except:
            msg = 'Please specify image type, username and ssh public key.'
            return msg
        host = ""
        usrp_if = ""
        
        if usrp_type == 'x310':
            host, usrp_if =resource_ctrl.lookup_x310_if(usrp_num)
        if usrp_type == 'n210beam':
            print usrp_num
            host, usrp_if =resource_ctrl.lookup_n210beam_if(usrp_num)
        if usrp_type == 'n210':
            host, usrp_if =resource_ctrl.lookup_host_if(usrp_num)

        try:
            domain = self._conn_list[host].lookupByName("basic_eu_"+usrp_num)
            deny = True
        except:
            deny = False

        if deny:
            msg = 'denied'
        else:
            msg = 'Accepted'
            f = threading.Event()
            t = threading.Thread(target=self._make_usrp_vm, args=(usrp_num,img_type,host,usrp_if,
                                                               user, ssh_key,f))
            t.name = 'make_usrp_'+str(usrp_num)+'_'+t.name.lower()
            t.start()

            self._make_threads[usrp_num] = t
            self._break_flags[usrp_num] = f


        return 'request_status:'+msg

    def request_simulation_vm(self, args):
        self._log.info("Unit Requested: "+args)
        try:
            vm_id,rack_id,memory,cores,img_title, img_type,user,ssh_key = args.split(',')
        except:
            msg = 'Please specify a vm_id, rack_id, memory in GB, no. of cores, image type, username and ssh public key.'
            return msg

        host = resource_ctrl.lookup_vm_sim_mapping(rack_id)
        try:
            domain = self._conn_list[host].lookupByName(img_title+vm_id)
            deny = True
        except:
            deny = False

        if deny:
            msg = 'denied'
        else:
            
            msg = 'Accepted'
            f = threading.Event()
            t = threading.Thread(target=self._make_simulation_vm, args=(rack_id,vm_id, img_title, cores,memory,img_type,
                                                               user, ssh_key,f))
            t.name = 'make_'+img_title+str(vm_id)+'_'+t.name.lower()
            t.start()
            self._make_threads[vm_id] = t
            self._break_flags[vm_id] = f


        return 'request_status:'+msg
    
    def grid_status(self, args):
        self._log.debug("Grid Status: "+args)
        usrps=resource_ctrl.check_grid_resources(self._conn_list)
        engaged = []
        for key in usrps.use.keys():
            if usrps.use[key]:
                engaged.append(key)
        engaged.sort()
        engaged = [str(k) for k in engaged]
        return 'grid_status:\n\tIn use:'+','.join(engaged)

    def release_unit(self, usrp_num):
        """ 
        Deletes the VM
        """
        use, boot, vm_ip = resource_ctrl.check_usrp_status(self._conn_list,usrp_num)
        """ 
        try:
            self._break_flags[usrp_num].set()
        except KeyError:
            #This usrp has not been requested yet (in this session)
            pass
        """
        if use:
            t = threading.Thread(target=self._destroyer, args=(usrp_num,))
            t.name = 'destroy_usrp_'+str(usrp_num)+t.name.lower()
            t.start()
            self._del_threads[usrp_num] = t
            return 'ok'
        else:
            return 'not_in_use'

    def release_sim_unit(self,args):
        """
        Deletes the SIM VM
        """
        self._log.info("Delete VM SIM unit: "+args)
        try:
            vm_id,domain_str = args.split(',')
        except:
            msg = 'Please specify a vm_id, and domain_str.'
            return msg

        host = ""
        try:
            hostname=resource_ctrl.list_vms_deployed(self._conn_list, domain_str+vm_id)
            for rack_id, ipaddress in virt.HOST_IPS.iteritems():
                if rack_id == hostname:
                    host = rack_id

            deny = False
        except:
            deny = True

        if deny:
            msg = 'denied'
        else:
            self._log.debug("print host: "+host)
            t = threading.Thread(target=self._vm_sim_destroyer, args=(domain_str+vm_id,host,))
            t.name = 'destroy_sim_vm_'+str(vm_id)+t.name.lower()
            t.start()
            self._del_threads[vm_id] = t
            msg = 'complete'
        return msg

    def restart_sim_unit(self,args):
        """
        restart the SIM VM
        """
        self._log.info("restart VM SIM unit: "+args)
        try:
            vm_id,domain_str = args.split(',')
        except:
            msg = 'Please specify a vm_id, and domain_str.'
            return msg

        host = ""
        try:
            hostname=resource_ctrl.list_vms_deployed(self._conn_list, domain_str+vm_id)
            for rack_id, ipaddress in virt.HOST_IPS.iteritems():
                if rack_id == hostname:
                    host = rack_id

            deny = False
        except:
            deny = True

        if deny:
            msg = 'denied'
        else:
            self._log.debug("print host: "+host)
            t = threading.Thread(target=self._restart_sim_unit, args=(domain_str+vm_id,host,))
            t.name = 'restart_sim_unit_'+str(vm_id)+t.name.lower()
            t.start()
            self._del_threads[vm_id] = t
            msg = 'complete'
        return msg

    def start_sim_unit(self,args):
        """
        start the SIM VM
        """
        self._log.info("start VM SIM unit: "+args)
        try:
            vm_id,domain_str = args.split(',')
        except:
            msg = 'Please specify a vm_id, and domain_str.'
            return msg

        host = ""
        try:
            if vm_id == '11' or vm_id == '44':
                host, usrp_if =resource_ctrl.lookup_x310_if(vm_id)
            elif vm_id == '12' or vm_id == '43':
                host, usrp_if =resource_ctrl.lookup_n210beam_if(vm_id)
            else:
                host, usrp_if =resource_ctrl.lookup_host_if(vm_id)

            deny = False
        except:
            deny = True

        if deny:
            msg = 'denied'
        else:
            self._log.info("print host: "+host)
            t = threading.Thread(target=self._start_sim_unit, args=(domain_str+vm_id,host,))
            t.name = 'start_sim_unit_'+str(vm_id)+t.name.lower()
            t.start()
            self._del_threads[vm_id] = t
            msg = 'complete'
        return msg

    def stop_sim_unit(self,args):
        """
        stop the SIM VM
        """
        self._log.info("stop VM SIM unit: "+args)
        try:
            vm_id,domain_str = args.split(',')
        except:
            msg = 'Please specify a vm_id, and domain_str.'
            return msg

        host = ""
        try:
            hostname=resource_ctrl.list_vms_deployed(self._conn_list, domain_str+vm_id)
            for rack_id, ipaddress in virt.HOST_IPS.iteritems():
                if rack_id == hostname:
                    host = rack_id

            deny = False
        except:
            deny = True

        if deny:
            msg = 'denied'
        else:
            self._log.debug("print host: "+host)
            t = threading.Thread(target=self._stop_sim_unit, args=(domain_str+vm_id,host,))
            t.name = 'stop_sim_unit_'+str(vm_id)+t.name.lower()
            t.start()
            self._del_threads[vm_id] = t
            msg = 'complete'
        return msg

    def sim_unit_status(self, args):
        """ 
        Get the satus of a particular simulation vm. Possible status are:
            free: No VM is defined 
            vm IP: If the machine is booted and ready, the IP is replied.
            wait: The VM is being created. Wait for completion.

        Returns:
            msg (string): String that indicates the status of the USRP.
        """

        host=""

        try:
            vm_id,domain_str = args.split(',')
            hostname=resource_ctrl.list_vms_deployed(self._conn_list, domain_str+vm_id)

            if hostname == "null":
                return 'sim_unit_status:wait'
            else:
                for rack_id, ipaddress in virt.HOST_IPS.iteritems():
                    if rack_id == hostname:
                        host = rack_id
                        print("host : " + host + " rack_id : " +rack_id)

            #Check if the VM is being made
            assert(self._make_threads[vm_id].is_alive())
        except (AssertionError, KeyError):
            try:
                vm_ip, usrp = self._conn_list[host].get_vm_if_info(domain_str+vm_id)
                if vm_ip == "0.0.0.0":
                    return 'sim_unit_status:wait'
                else:
                    return 'sim_unit_status:'+str(vm_ip)
            except(AssertionError, KeyError):
                return 'sim_unit_status:wait'
        else:
            #The VM is being made, wait.
            return 'sim_unit_status:wait'

    def unit_status(self, usrp_num):
        """ 
        Get the satus of a particular USRP. Possible status are:
            free: No VM is defined for that particular USRP
            vm IP: If the machine is booted and ready, the IP is replied.
            wait: The VM is being created. Wait for completion.

        Returns:
            msg (string): String that indicates the status of the USRP.
        """

        try:
            #Check if the VM is being made
            assert(self._make_threads[usrp_num].is_alive())
        except (AssertionError, KeyError):
            #The VM is not being made, get its info.
            use, boot, vm_ip = resource_ctrl.check_usrp_status(self._conn_list,usrp_num)
            if not use:
                return 'unit_status:free'
            elif boot:
                return 'unit_status:'+str(vm_ip)
            else:
                return 'unit_status:0.0.0.0'
        else:
            #The VM is being made, wait.
            return 'unit_status:wait'

    def shutdown(self):
        for key in self._break_flags.keys():
            self._break_flags[key].set()

        for key in self._queues.keys():
            try:
                self._queues[key].put_nowait(-1)
            except Queue.Full:
                continue

    #def update(self):
    #    self._usrps.update()

    def start_unit(self, usrp_num):
        self._start_vm(usrp_num)
        return 'ok'

    def shutdown_unit(self, usrp_num):
        self._stop_vm(usrp_num)
        return 'ok'

    def restart_unit(self, usrp_num):
        self._restart_vm(usrp_num)
        return 'ok'

    def make_clean(self,non_active_usrps):
        non_active_usrps = non_active_usrps.split(',')
        self._make_clean(non_active_usrps)
        return 'ok'
