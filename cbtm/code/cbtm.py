#!/usr/bin/env python
'''
    CBTM"s main module.

    This module defines the main CBTM class. To start the CBTM server, just run this module.
'''

import template_ctrl
import resource_ctrl
import image_ctrl
import virt
import os
import subprocess
import sys
import time
import argparse

import logging
import cbtm_logging
import threading
#from cbtm_logging import get_cbtm_logger
from twisted_server import ConnectionFactory
from response_handler import Handler

from image_ctrl import POOL_PATH

outlock = threading.Lock()

image_name = 'basic_eu_'

class Cbtm(object):
    ''' 
    Main class to define the CBTM.

    The run function of this class will start the CBTM server.

    Attributes:
        log (cbtm_logging): Logging object that is used to create the logs
        usrps (usrp_list): List of the USRPs connected to a VM and their IPs
        handler (response_handler): Function that handles CBTP messages
        server (ConnectionFactory): TCP server that handles requests from the
            clients (most likely the Aggregate Manager)
        port (int): the port which the CBTM server listens to (default 5006)
    '''
    def __init__(self, args):
        #Start the logger
        cbtm_logging.init_logger(level = args.log_level, verbose = args.verbose)
        self.log = logging.getLogger("cbtm_logger")
        self.log.info("Initialized Logger. Log Level: "+str(args.log_level).upper())

        self.log.info('Opening connections to the hypervisor')
        self.conn_list = virt.open_connections()

        self.log.info('Looking up USRPs')
        usrps = resource_ctrl.check_grid_resources(self.conn_list)
        self.log.debug("USRP Info:\n"+usrps.usrp_print())

        self.log.debug('Starting Handler')
        self.handler = Handler(self.conn_list, self.make_usrp_vm,self.make_simulation_vm, self.destroy_vm, self.destroy_sim_vm,
                               self.start_vm, self.shutdown_vm, self.restart_vm, self.make_clean, self.start_vm_unit, self.restart_vm_unit, self.shutdown_vm_unit)

        self.log.debug('Starting Connection Factory')
        self.port = 5006
        self.server = ConnectionFactory(self.handler, self.log)

    def run(self):
        '''Start the CBTM server.'''
        self.log.debug('Starting CBTM Server')
        self.server.run_server(self.port)

    def shutdown(self):
        '''Shutdown the CBTM server'''
        self.log.debug('Shuting down CBTM')
        self.handler.shutdown()

    def create_user(self,user,ssh_key,ip):
        '''
        Create a new user in a VM.

        Args:
            vm_num (int): USRP Id
            host_name (string): Rack where the VM is located
            user (string): new user name
            ssh_key (string): ssh key for the user
        '''
        self.log.info('Creating user for usage in the VM')

        HOST='@'+ip

        COMMAND = 'sudo adduser --disabled-password --gecos "" ${USER}'+ \
                  ' && sudo addgroup ${USER} sudo'+ \
                  ' && sudo mkdir /home/${USER}/.ssh'+ \
                  ' && sudo echo "${SSH_KEY}" >> authorized_keys'+ \
                  ' && sudo mv authorized_keys /home/${USER}/.ssh'+ \
                  ' && sudo chown ${USER}:${USER} /home/${USER}/.ssh -R'

        COMMAND = COMMAND \
                .replace('${USER}',user) \
                .replace('${SSH_KEY}',ssh_key)

        #Sometimes the connection is refused, probably because the ssh server has not started yet.
        #This loops tries to create the user up to maximum_attempts times, with a sleep_time interval (in seconds) between tries.
        attempts = 0
        maximum_attempts = 30
        sleep_time = 5
        user_created = False
        std_error_msg = "Permanently"

        while (user_created == False) and (attempts < maximum_attempts):
            self.log.info("break out of loop "+str(user_created))
            if user_created:
	        break

            with outlock:
                ssh = subprocess.Popen(['ssh','-o','UserKnownHostsFile=/dev/null','-o','StrictHostKeyChecking no','-t', '%s' % HOST, COMMAND],
                                   shell=False,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
            with outlock:
                stdout = ssh.stdout.read()
                stderr = ssh.stderr.read()
            self.log.debug('Stdout :'+str(stdout))
            self.log.debug('Stderr :'+str(stderr))

            if std_error_msg not in stderr:
                with outlock:
                    self.log.error('Connection Refused when creating user. Attempt :'+str(attempts))
                    self.log.debug('Std err:\n'+stderr)
                    self.log.debug('Std out:\n'+stdout)
                attempts+=1
                time.sleep(sleep_time)
            else:
                with outlock:
                    user_created = True
                    self.log.debug('Successfully created user.')
                    self.log.debug('Std err:\n'+stderr)
                    self.log.debug('Std out:\n'+stdout)
                user_created = True
                break

    def vm_launch_wait(self, vm_num, img_name, user, ssh_key, host, vm_xml, template_ctrl, container_name):
        template_ctrl.define_and_boot_vm(self.conn_list, host, vm_xml)
        self.log.debug('VM Defined and Booting.')
        #The VM takes some time to boot, so wait a few seconds before asking for the IP
        out_ip = '0.0.0.0'
        boot_time = 10 #Give the VM some time to boot.
        check_interval = 0.5
        time.sleep(boot_time)
        time0 = time.time()
        self.log.debug("Starting to get IP")
        while out_ip == '0.0.0.0':
            out_ip = '127.0.0.1'
            if img_name == 'basic_eu_':
                out_ip, usrp = self.conn_list[host].get_vm_if_info(img_name + str(vm_num))
            elif img_name =='simulation_eu_':
                out_ip, usrp = self.conn_list[host].get_vm_if_info(img_name + str(vm_num))
                usrp = 0
            else:
                out_ip, usrp = self.conn_list[host].get_vm_if_info(img_name + str(vm_num))
                usrp = 0

            if time.time() - time0 > 30:
                self.log.info("time is: " + str(time.time() - time0))
                out_ip = '0.0.0.0' # break
                time0 = time.time()
            time.sleep(check_interval)
        
        if out_ip != '0.0.0.0':
            self.create_user(user, ssh_key, out_ip)
        
        if len(container_name) > 1:
            self.launch_conatiner(user, out_ip, container_name)
        
        return out_ip, usrp
    
    def launch_conatiner(self, user, ip, container_name):
        self.log.info("=============ENTERING CONTAINER LAUNCH ==============")
        fullpath = '@' + ip
        for i, elem in enumerate(container_name):
            self.log.info("i is: " + elem)
            if "hello-world" in elem:
                command = 'sudo docker run hello-world'
                result = self.run_command(fullpath, command)
                self.log.info("Result is : " + result)
            elif "lxc-udp" in elem:
                command = 'sudo lxc-create -t ubuntu -n cn-01'
                result = self.run_command(fullpath, command)
                self.log.info("Result is : " + result)
    
    def run_command(self, fullpath, COMMAND):
        result = 'error'
        try:
            ssh = subprocess.Popen(['ssh','-o','UserKnownHostsFile=/dev/null','-o','StrictHostKeyChecking no','-t', "%s" % fullpath, COMMAND],
            shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

            result = ssh.stdout.read()
            err = ssh.stderr.read()
            if err:
                self.log.debug('ERROR: '+ str(err))
            # print "Inside run_command"
            # print str(result)
        except Exception as e:
            self.log.debug(str(e))
            return 'error'

        return str(result)
    
    def make_usrp_vm(self, vm_num,img_type,host,iface,user,ssh_key,break_flag):
        ''' 
        Create a new VM to be used with a USRP.

        Args:
            vm_num (int): USRP Id
            img_type (string): Type of image to be used (clean, gnuradio of iris)
            user (string): User for the VM
            ssh_key (string): SSH key for the user
        '''
        container_name = []
        try:
            usrp_if_mac_addr = resource_ctrl.lookup_usrp_if_mac(vm_num)

            #self.log.debug('Creating VM image. (This may take a while)')

            self.log.debug('Creating VM image. (This may take a while).')
            self.log.debug('USRP '+ str(vm_num))
            if img_type =='plain':
                image_ctrl.copy_image('master-plain', 'basic_eu_'+str(vm_num), self.conn_list, host)
            elif img_type =='gnuradio':
                image_ctrl.copy_image('master-gnuradio', 'basic_eu_'+str(vm_num), self.conn_list, host)
            elif img_type =='1704':
                image_ctrl.copy_image('1704-ubuntu', 'basic_eu_'+str(vm_num), self.conn_list, host)
            elif img_type =='oai-enb':
                image_ctrl.copy_image('oai-enb', 'basic_eu_'+str(vm_num), self.conn_list, host)
            elif img_type =='iris':
                image_ctrl.copy_image('master-iris', 'basic_eu_'+str(vm_num), self.conn_list, host)
            elif img_type =='plain-14-04':
                image_ctrl.copy_image('master-plain-14-04-cpu', 'basic_eu_'+str(vm_num), self.conn_list, host)
            elif img_type =='fosphor':
                image_ctrl.copy_image('master-fosphor', 'basic_eu_'+str(vm_num), self.conn_list, host)
            elif img_type =='srslte':
                image_ctrl.copy_image('master-srslte', 'basic_eu_'+str(vm_num), self.conn_list, host)
            elif img_type =='wishful':
                image_ctrl.copy_image('16.04-wishful', 'basic_eu_'+str(vm_num), self.conn_list, host)
            elif img_type =='iris-radar':
                image_ctrl.copy_image('iris-radar', 'basic_eu_'+str(vm_num), self.conn_list, host)
            elif img_type =='aggmgr_cbtm_coor':
                image_ctrl.copy_image('aggmgr_cbtm_coor', 'basic_eu_'+str(vm_num), self.conn_list, host)
            elif 'docker-criu' in img_type:
                image_ctrl.copy_image('docker-criu', 'basic_eu_'+str(vm_num), self.conn_list, host)
                container_name = img_type.split('_')
            elif "docker-img" in img_type:
                image_ctrl.copy_image('docker-img', 'basic_eu_'+str(vm_num), self.conn_list, host)
                container_name = img_type.split('_')
            else:
                self.log.warning('Unrecognized image type:'+str(img_type)+' Getting plain image instead')
                image_ctrl.copy_image('master-plain', 'basic_eu_'+str(vm_num), self.conn_list, host)

            img_name = 'basic_eu_'+str(vm_num)+'.qcow2'

            vm_xml = template_ctrl.create_usrp_xml(vm_num,iface,POOL_PATH+img_name,usrp_if_mac_addr)

            out_ip, vmname = self.vm_launch_wait(vm_num, "basic_eu_", user, ssh_key, host, vm_xml, template_ctrl, container_name)
            return (out_ip,vmname)

        except RuntimeError as e:
            self.log.debug(str(e))
            return

    def make_simulation_vm(self, rack_id, vm_id, img_title, cores, memory,img_type,user,ssh_key,break_flag):
        ''' 
        Create a new VM for simulation

        Args:
	    rack_id: machine IP
            vm_id: random id of VM machine
            img_title: title of image to be created e.g., simulation_eu_, x310s_eu_, etc
            cores (int): number of cores in vm
            memory (int): memory 
            img_type (string): Type of image to be used (clean)
            user (string): User for the VM
            ssh_key (string): SSH key for the user
        '''
        container_name = []
        try:
            (host) = resource_ctrl.lookup_vm_sim_mapping(rack_id)

            self.log.debug('Creating VM image. (This may take a while).')
            self.log.debug(img_title+ str(vm_id))
            if img_type =='plain':
                image_ctrl.copy_image('simulation-php', img_title+str(vm_id), self.conn_list, host)
            elif img_type =='aggmgr_cbtm_coor':
                image_ctrl.copy_image('aggmgr_cbtm_coor', img_title+str(vm_id), self.conn_list, host)
            elif img_type =='oai-cn':
                image_ctrl.copy_image('master-openair-cn', img_title+str(vm_id), self.conn_list, host)
            elif img_type =='1704':
                image_ctrl.copy_image('1704-ubuntu', img_title+str(vm_id), self.conn_list, host)
            elif img_type =='ovs':
                image_ctrl.copy_image('master-ovs', img_title+str(vm_id), self.conn_list, host)
            elif img_type =='ndnrepo':
                image_ctrl.copy_image('master_ndn_repo', img_title+str(vm_id), self.conn_list, host)
            elif img_type =='plain-14-04':
                image_ctrl.copy_image('master-plain-14-04-cpu', img_title+str(vm_id), self.conn_list, host)
            elif 'docker-criu' in img_type:
                image_ctrl.copy_image('docker-criu', img_title+str(vm_id), self.conn_list, host)
                container_name = img_type.split('_')
            elif 'docker-img' in img_type:
                image_ctrl.copy_image('docker-img', img_title+str(vm_id), self.conn_list, host)
                container_name = img_type.split('_')
            elif img_type =='wishful':
                image_ctrl.copy_image('16.04-wishful', img_title+str(vm_id), self.conn_list, host)
            else:
                self.log.warning('Unrecognized image type:'+str(img_type)+' Getting plain image instead')
                image_ctrl.copy_image('simulation-php', img_title+str(vm_id), self.conn_list, host)

            img_name = img_title+str(vm_id)+'.qcow2'

            vm_xml = template_ctrl.create_simulation_xml(vm_id,img_title,POOL_PATH+img_name,cores,memory)

            out_ip, vmname = self.vm_launch_wait(vm_id, img_title, user, ssh_key, host, vm_xml, template_ctrl, container_name)
            return (out_ip,vm_id)

        except RuntimeError as e:
            self.log.debug(str(e))
            return
        
    def destroy_vm(self, vm_num):
        ''' 
        Delete a VM

        Args:
            vm_num (int): USRP Id
        '''
        (host, iface) = resource_ctrl.lookup_host_if(vm_num)
        vm_name = resource_ctrl.get_vm(self.conn_list,vm_num)

        if vm_name == -1:
            self.log.debug('No VM found for USRP %s'%vm_num)
        else:
            self.log.info('Deleting VM %s'%vm_name)
            template_ctrl.destroy_vm(self.conn_list,host,vm_name)

        image_ctrl.del_image(vm_name, self.conn_list, host)

    def destroy_sim_vm(self, vm_name,host):
        ''' 
        Delete a VM

        Args:
            vm_name (str): vm name
            host (str): ip of host 
        '''
        if vm_name == -1:
            self.log.debug('No VM found for VM %s'%vm_num)
        else:
            self.log.info('Deleting SIM VM %s'%vm_name)
            template_ctrl.destroy_vm(self.conn_list,host,vm_name)

        image_ctrl.del_image(vm_name, self.conn_list, host)
 
    def start_vm(self, vm_num):
        ''' 
        Args:
            vm_num (int): USRP Id
        '''
        (host, iface) = resource_ctrl.lookup_host_if(vm_num)
        vm_name = resource_ctrl.get_vm(self.conn_list,vm_num)

        if vm_name == -1:
            self.log.debug('No VM found for USRP %s'%vm_num)
        else:
            self.log.debug('Starting VM %s'%vm_name)
            template_ctrl.start_vm(self.conn_list,host,vm_name)

    def start_vm_unit(self, vm_name,host):
        ''' 
        Args:
            vm_num (int): USRP Id
        '''
        if vm_name == -1:
            self.log.debug('No VM found for id basic_eu_ %s'%vm_num)
        else:
            self.log.info('Starting VM basic_eu_ %s'%vm_name)
            self.log.info('Starting VM host %s'%host)
            template_ctrl.start_vm(self.conn_list,host,vm_name)

    def shutdown_vm_unit(self, vm_name,host):
        ''' 
        Args:
            vm_num (int): USRP Id
        '''

        if vm_name == -1:
            self.log.debug('No VM found for USRP %s'%vm_num)
        else:
            self.log.debug('Shutting Down VM %s'%vm_name)
            template_ctrl.shutdown_vm(self.conn_list,host,vm_name)

    def restart_vm_unit(self, vm_num):
        ''' 
        Args:
            vm_num (int): USRP Id
        '''
        if vm_name == -1:
            self.log.debug('No VM found for USRP %s'%vm_num)
        else:
            self.log.debug('Restarting VM %s'%vm_name)
            template_ctrl.restart_vm(self.conn_list,host,vm_name)

    def shutdown_vm(self, vm_num):
        ''' 
        Args:
            vm_num (int): USRP Id
        '''
        (host, iface) = resource_ctrl.lookup_host_if(vm_num)
        vm_name = resource_ctrl.get_vm(self.conn_list,vm_num)

        if vm_name == -1:
            self.log.debug('No VM found for USRP %s'%vm_num)
        else:
            self.log.debug('Shutting Down VM %s'%vm_name)
            template_ctrl.shutdown_vm(self.conn_list,host,vm_name)

    def restart_vm(self, vm_num):
        ''' 
        Args:
            vm_num (int): USRP Id
        '''
        (host, iface) = resource_ctrl.lookup_host_if(vm_num)
        vm_name = resource_ctrl.get_vm(self.conn_list,vm_num)

        if vm_name == -1:
            self.log.debug('No VM found for USRP %s'%vm_num)
        else:
            self.log.debug('Restarting VM %s'%vm_name)
            template_ctrl.restart_vm(self.conn_list,host,vm_name)

    def make_clean(self, non_active_usrps):
        ''' 
        Args:
            non_active_usrps (list): List of strings with the USRP Ids that we need to make sure are deleted.
        '''
        host = ""
        self.log.info(non_active_usrps)
        for vm_num in non_active_usrps:
            if vm_num == "":
                return "ok"
            if vm_num == '11' or vm_num =='44':
                self.log.debug("Deleting vmi no. " + vm_num)
                host, usrp_if =resource_ctrl.lookup_x310_if(vm_num)
            elif vm_num =='12' or vm_num == '43':
                self.log.debug("Deleting vmi no. " + vm_num)
                host, usrp_if =resource_ctrl.lookup_n210beam_if(vm_num)
            else:
                self.log.debug("Deleting vmi no. " + vm_num)
                host, usrp_if =resource_ctrl.lookup_host_if(vm_num)

            vm_name = -1 
            if vm_num =='12' or vm_num == '43' or vm_num == '11' or vm_num =='44':
                vm_name = resource_ctrl.list_vms_deployed(self.conn_list,"basic_eu_"+vm_num)
                if vm_name != "null":
                    vm_name = "basic_eu_"+vm_num
                else:
                    vm_name = -1
            else:
                vm_name = resource_ctrl.get_vm(self.conn_list,vm_num)

            if vm_name != -1:
                self.log.info("Non-expected active VM! Deleting: "+vm_name)
                template_ctrl.destroy_vm(self.conn_list,host,vm_name)
            
            vol_name = -1
            if vm_num =='12' or vm_num == '43' or vm_num == '11' or vm_num =='44':
                vol_name = resource_ctrl.list_vms_deployed(self.conn_list,"basic_eu_"+vm_num)
                if vol_name != "null":
                    vol_name = "basic_eu_"+vm_num
                else:
                    vol_name = -1
            else:
                vol_name = resource_ctrl.get_vol(self.conn_list,vm_num)

            if vol_name != -1:
                self.log.debug("Non-expected active Volume! Deleting: "+vol_name)
                image_ctrl.del_image(vol_name[:-6],self.conn_list,host)

        return "ok"

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='CBTM Tesbed Manager')
    parser.add_argument('--log-level', action="store", type=str,
                    choices=["critical", "error", "warning", "info", "debug", "notset"],
                    default="info", help='Select the log level of the program.')
    parser.add_argument('--verbose', default=False, action = 'store_true',
                    help='Select whether to output logs to the console.')

    args = parser.parse_args()
    coord = Cbtm(args)
    try:
        coord.run()
    except Exception as e:
        raise e
    finally:
        coord.shutdown()

