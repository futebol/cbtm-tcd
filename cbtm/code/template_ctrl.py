#!/usr/bin/env python
'''
    Package 'template_ctrl'
    Manages VM templates for SDR use

    This module provides the ability to create and instantiate VM
    templates for Iris use. Each template enables the passthrough of a
    network interface. Passthrough is currently accomplished in a manner
    That requires the interface to be configured independently on the host
    machine.
'''
from string import Template
import datetime
import virt
import os

DIR_NAME = '../resources/'
'''
    Specifies the directory that contains the base file for template creation
'''

BASE_FILE = 'basic_eu.xml'
SIM_BASE_FILE = 'sim_eu.xml'
'''
    specifies the name of the base file for template creation
'''

def read_xml(xmlPath):
    '''
    Read the bare XML template
    '''
    try:
        template = open(xmlPath,'r')
    except:
        print ('Error: Cannot open the file '+xmlPath)
        exit(1)

    xml = template.read()
    template.close()

    return xml;

def create_usrp_xml(usrp_num,iface,image,mac_addr):
    '''
    Creates a XML template based on the Args assigned.

    Args:
        usrp_num (int): USRP Id
        iface (string): Interface that is connected to USRP
        image (string): Full path to VM image
        mac_addr (string): Macc Address of the interface connected to USRP

    Returns:
        full_template (string): A full template with the defined specifications
    '''

    bare_template = read_xml(DIR_NAME+BASE_FILE)
    full_template = bare_template \
        .replace('${DOM_NAME}','basic_eu_'+str(usrp_num)) \
        .replace('${AMOUNT_CPU}',str(4)) \
        .replace('${AMOUNT_MEMORY}',str(4)) \
        .replace('${IMG_PATH}',image) \
        .replace('${HOST_IF}',iface) \
        .replace('${MAC_ADDRESS}',mac_addr)

    return full_template

def create_simulation_xml(vm_num,domain_name,image, cpu_num, mem_num):
    '''
    Creates a XML template based on the Args assigned.

    Args:
        vm_num (int): vm_id
        image (string): Full path to VM image
        domain_name (string): domain name
        cpu_num (string): Number of CPUs
        mem_num (string): Fmount of memory 4 = 4GB, ..., 16=16GB

    Returns:
        full_template (string): A full template with the defined specifications
    '''

    bare_template = read_xml(DIR_NAME+SIM_BASE_FILE)
    full_template = bare_template \
        .replace('${DOM_NAME}',domain_name+str(vm_num)) \
        .replace('${AMOUNT_CPU}',str(cpu_num)) \
        .replace('${AMOUNT_MEMORY}',str(mem_num)) \
        .replace('${IMG_PATH}',image)

    return full_template

def define_and_boot_vm(conn_list, host_name, template):
    '''
    Define (create) and boot a Virtual Machine.

    Args:
        conn_list (ConnInfo): Libvirt connections to the Racks
        host_name (string): Host where the VM will be located
        template (string): XML template created using create_xml()
    '''
    try:
        conn = conn_list[host_name].conn
    except KeyError:
        print ('Could not find connection for the intended rack:', rack)
        raise

    #dom = conn.defineXML(template)
    #dom.create()
    try:
        dom = conn.defineXML(template)
    except:
        print ('Could not define VM')
        raise

    try:
        dom.create()
    except:
        print ('Could not boot VM')
        raise

def destroy_vm(conn_list, host_name, domain_name):
    '''
    Undefine (destroy) a Virtual Machine.

    Args:
        conn_list (ConnInfo): Libvirt connections to the Racks
        host_name (string): Host where the VM will be located
        domain_name (string): Name of the VM
    '''
    try:
        conn = conn_list[host_name].conn
    except KeyError:
        print ('Could not find connection for the intended rack:', host_name)
        raise

    dom = conn.lookupByName(domain_name)
    if dom.isActive():
        dom.destroy()
    dom.undefine()

def start_vm(conn_list, host_name, domain_name):
    '''
    Start a Virtual Machine.

    Args:
        conn_list (ConnInfo): Libvirt connections to the Racks
        host_name (string): Host where the VM will be located
        domain_name (string): Name of the VM
    '''
    try:
        conn = conn_list[host_name].conn
    except KeyError:
        print ('Could not find connection for the intended rack:', rack)
        raise

    dom = conn.lookupByName(domain_name)
    if not dom.isActive():
        dom.create()

def shutdown_vm(conn_list, host_name, domain_name):
    '''
    Shutdown a Virtual Machine.

    Args:
        conn_list (ConnInfo): Libvirt connections to the Racks
        host_name (string): Host where the VM will be located
        domain_name (string): Name of the VM
    '''
    try:
        conn = conn_list[host_name].conn
    except KeyError:
        print ('Could not find connection for the intended rack:', rack)
        raise

    dom = conn.lookupByName(domain_name)
    if dom.isActive():
        dom.shutdownFlags(2)

def restart_vm(conn_list, host_name, domain_name):
    '''
    Restart a Virtual Machine.

    Args:
        conn_list (ConnInfo): Libvirt connections to the Racks
        host_name (string): Host where the VM will be located
        domain_name (string): Name of the VM
    '''
    try:
        conn = conn_list[host_name].conn
    except KeyError:
        print ('Could not find connection for the intended rack:', rack)
        raise

    dom = conn.lookupByName(domain_name)
    if dom.isActive():
        dom.reboot(2)


if __name__ == '__main__':
    pass
