#!/bin/bash

echo "About to kill these processes:"
pgrep -u root -f cbtm.py --list-full

read -p "Do you want to proceed?" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo pkill -u scamallfe -f cbtm.py
fi

