#!/usr/bin/env python
'''
CBTM's image control module.

This module provides basic functionality to manage (copy and delete) VM images.
'''
import threading
import template_ctrl
import virt

POOL_NAME = 'cbtm-pool'
POOL_PATH = '/home/scamallra/cbtm-pool/'

def copy_image(target, new_name, conn_list, host_name):
    ''' 
    Copy VM's image.

    Args:
        target (string): Image to be copied
        new_name (string): Name for the new image
        conn_list (ConnInfo): Libvirt connections to the Racks
        host_name (string): Host where the VM will be located
    '''
    storage_pool_target = POOL_NAME #storage pool where are the templates.
    storage_pool_new = POOL_NAME
    template_name = 'volume_template_clone.xml'

    try:
        conn = conn_list[host_name].conn
    except KeyError:
        print ('Could not find connection for the intended rack:', rack)
        raise

    s_pool_targ = conn.storagePoolLookupByName(storage_pool_target)
    s_pool_new = conn.storagePoolLookupByName(storage_pool_new)

    pathTemplate = template_ctrl.DIR_NAME+template_name

    vol_XML = template_ctrl.read_xml(pathTemplate)
    vol_XML = vol_XML \
        .replace('${VOL_NAME}',new_name) \
        .replace('${POOL_PATH}',POOL_PATH) #here comes the path for the images in the racks.

    try:
        orig_vol = s_pool_targ.storageVolLookupByName(target+'.qcow2')
    except:
        print 'Error: Cannot locate volume to be cloned.'
        exit(1)

    try:
        new_vol = s_pool_new.createXMLFrom(vol_XML,orig_vol,0)
    except:
        print 'Error copying the image.'
        exit(1)

def del_image(name, conn_list, host_name):
    ''' 
    Delete VM's image.

    Args:
        name (string): Name of the image
        conn_list (ConnInfo): Libvirt connections to the Racks
        host_name (string): Host where the VM will be located
    '''
    storage_pool = POOL_NAME
    try:
        conn = conn_list[host_name].conn
    except KeyError:
        print ('Could not find connection for the intended rack:', rack)
        raise

    try:
        s_pool = conn.storagePoolLookupByName(storage_pool)
    except:
        print 'Error: Cannot locate Storage Pool.'

    if not s_pool.isActive():
        s_pool.create()

    try:
        vol = s_pool.storageVolLookupByName(name+'.qcow2')
    except:
        print 'Error: The volume doesn\'t exist.'

    try:
        if vol is not None:
            vol.delete(0)
    except:
        print ('Error: The volume doesn\'t exist:',name)

if __name__ == '__main__':
    import time

    name = str(time.time())

    conn_list = virt.open_connections()
    host = 'rackA13'
