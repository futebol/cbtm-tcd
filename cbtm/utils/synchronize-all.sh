#!/bin/bash
ansible-playbook synchronize-master-iris.yml
ansible-playbook synchronize-master-gnuradio.yml
ansible-playbook synchronize-master-plain-14-04.yml
ansible-playbook synchronize-master-plain.yml
ansible-playbook synchronize-master-srslte.yml
ansible-playbook synchronize-master-fosphor.yml
ansible-playbook synchronize-master-wishful.yml
ansible-playbook synchronize-iris-radar.yml
ansible-playbook synchronize-master-aggmgr_cbtm_coor.yml
ansible-playbook synchronize-docker-criu.yml
ansible-playbook synchronize-master-openvswitch.yml
ansible-playbook synchronize-simulation-php.yml
ansible-playbook synchronize-master-oai-cn.yml
